-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-06-2015 a las 00:58:36
-- Versión del servidor: 5.6.15-log
-- Versión de PHP: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cautriut`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aportes`
--

CREATE TABLE IF NOT EXISTS `aportes` (
  `idaporte` int(11) NOT NULL AUTO_INCREMENT,
  `idsocio` int(11) NOT NULL,
  `quincenauno1` float NOT NULL,
  `quincenados1` float NOT NULL,
  `incresalmes1` float NOT NULL,
  `nivelalto1` float NOT NULL,
  `divanoanterior` float NOT NULL,
  `quincenauno2` float NOT NULL,
  `quincenados2` float NOT NULL,
  `incresalmes2` float NOT NULL,
  `nivelalto2` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idaporte`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Volcado de datos para la tabla `aportes`
--

INSERT INTO `aportes` (`idaporte`, `idsocio`, `quincenauno1`, `quincenados1`, `incresalmes1`, `nivelalto1`, `divanoanterior`, `quincenauno2`, `quincenados2`, `incresalmes2`, `nivelalto2`, `fecha`) VALUES
(1, 1, 5.3, 5.3, 5.3, 5.3, 5.3, 5.3, 5.3, 5.3, 5.3, '2015-05-14'),
(4, 3, 4.7, 4.7, 4.7, 4.7, 4.7, 4.7, 4.7, 4.7, 4.7, '2015-05-01'),
(5, 3, 5.4, 5.4, 5.4, 5.4, 5.4, 5.4, 5.4, 5.4, 5.4, '2015-02-01'),
(6, 3, 9.8, 9.8, 9.8, 9.8, 9.8, 9.8, 9.8, 9.8, 9.8, '2015-01-01'),
(7, 3, 8, 8, 8, 8, 8, 8, 8, 8, 8, '2014-12-01'),
(42, 3, 10, 10, 10, 10, 10, 10, 10, 10, 10, '2015-04-01'),
(43, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, '2015-06-01'),
(44, 3, 5, 5, 5, 5, 5, 5, 5, 5, 5, '2015-07-01'),
(45, 3, 10, 10, 10, 10, 10, 10, 10, 10, 10, '2015-09-01'),
(46, 3, 10, 10, 10, 10, 10, 10, 10, 10, 10, '2015-10-01'),
(47, 3, 5, 5, 5, 5, 5, 5, 5, 5, 5, '2015-11-01'),
(48, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, '2015-12-01'),
(52, 3, 30, 30, 30, 30, 30, 30, 30, 30, 30, '2016-03-01'),
(53, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00'),
(54, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `idcargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idcargo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`idcargo`, `nombre`) VALUES
(1, 'Contratado'),
(2, 'Administrativo'),
(3, 'Trabajador de la CA'),
(4, 'Asistente de la CA'),
(5, 'Obrero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE IF NOT EXISTS `departamento` (
  `iddepartamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `telefono` int(11) NOT NULL,
  PRIMARY KEY (`iddepartamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`iddepartamento`, `nombre`, `telefono`) VALUES
(1, 'Extension y reproducción', 0),
(2, 'Planificación', 0),
(3, 'Curriculum', 0),
(4, 'Postgrado', 0),
(5, 'Daintes', 0),
(6, 'Rectoría', 0),
(7, 'Orientación', 0),
(8, 'Division de RRHH', 0),
(9, 'Division de EIT', 0),
(10, 'Unidad de deportes', 0),
(11, 'Coord. PNF mantenimiento', 0),
(12, 'Coord. PNF agroalimentaria', 0),
(13, 'Coord. PNF Mecánica', 0),
(14, 'Mercadeo', 0),
(15, 'Ciencias agropecuarias', 0),
(16, 'Tecnología naval', 0),
(17, 'Tecnología de alimentos', 0),
(18, 'Informática', 0),
(19, 'Turismo', 0),
(20, 'Tecnología administrativa', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disponibilidad`
--

CREATE TABLE IF NOT EXISTS `disponibilidad` (
  `iddisponibilidad` int(11) NOT NULL AUTO_INCREMENT,
  `efectivo` float NOT NULL,
  `haberes` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`iddisponibilidad`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `disponibilidad`
--

INSERT INTO `disponibilidad` (`iddisponibilidad`, `efectivo`, `haberes`, `fecha`) VALUES
(1, 26023000, 78447800, '2015-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `endeudamientopatronal`
--

CREATE TABLE IF NOT EXISTS `endeudamientopatronal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `porcobrar` float NOT NULL,
  `patrimonios` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `endeudamientopatronal`
--

INSERT INTO `endeudamientopatronal` (`id`, `porcobrar`, `patrimonios`) VALUES
(1, 11, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `endeudamientoprestamos`
--

CREATE TABLE IF NOT EXISTS `endeudamientoprestamos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `saldo` float NOT NULL,
  `haberes` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `endeudamientoprestamos`
--

INSERT INTO `endeudamientoprestamos` (`id`, `saldo`, `haberes`) VALUES
(1, 1100, 1223);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadosocio`
--

CREATE TABLE IF NOT EXISTS `estadosocio` (
  `idestadosocio` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`idestadosocio`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `estadosocio`
--

INSERT INTO `estadosocio` (`idestadosocio`, `estado`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadosolicitud`
--

CREATE TABLE IF NOT EXISTS `estadosolicitud` (
  `idestadosolicitud` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(50) NOT NULL,
  PRIMARY KEY (`idestadosolicitud`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `estadosolicitud`
--

INSERT INTO `estadosolicitud` (`idestadosolicitud`, `estado`) VALUES
(1, 'Aprobado'),
(2, 'No Aprobado'),
(3, 'En espera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialdeoperaciones`
--

CREATE TABLE IF NOT EXISTS `historialdeoperaciones` (
  `idhistorial` int(11) NOT NULL AUTO_INCREMENT,
  `usuarioquerealizaaccion` varchar(50) NOT NULL,
  `accion` varchar(50) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

--
-- Volcado de datos para la tabla `historialdeoperaciones`
--

INSERT INTO `historialdeoperaciones` (`idhistorial`, `usuarioquerealizaaccion`, `accion`, `usuario`, `fecha`) VALUES
(1, 'Javier Delgado', 'registro un Socio', '', '2015-05-01'),
(2, 'admin3', 'Registro', '', '2015-05-30'),
(3, 'admin3', 'Modifico un socio', '', '2015-05-30'),
(4, 'admin3', 'Modifico un socio', '', '2015-05-30'),
(5, 'admin2', 'Cambio el estado del Prestamo', '', '2015-05-30'),
(6, 'admin', 'Elimino un usuario', '', '2015-05-30'),
(7, 'admin3', 'Registro un Aporte', '', '2015-05-30'),
(8, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(9, 'admin3', 'Registro un Aporte', '', '2015-06-05'),
(10, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(11, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(12, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(13, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(14, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-05'),
(15, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(16, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(17, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(18, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(19, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(20, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(21, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(22, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(23, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(24, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(25, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(26, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(27, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(28, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(29, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(30, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(31, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(32, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(33, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(34, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(35, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(36, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(37, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(38, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(39, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(40, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(41, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(42, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(43, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(44, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(45, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(46, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(47, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(48, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(49, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(50, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(51, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(52, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(53, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(54, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(55, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(56, 'admin3', 'Registro un Aporte', '', '2015-06-06'),
(57, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-06'),
(58, 'admin3', 'Solicitud de Retiro', '', '2015-06-06'),
(59, 'admin3', 'Solicitud de Retiro', '', '2015-06-06'),
(60, 'admin3', 'Solicitud de Retiro', '', '2015-06-06'),
(61, 'admin3', 'Solicitud de Retiro', '', '2015-06-06'),
(62, 'admin3', 'Solicitud de Retiro', '', '2015-06-06'),
(63, 'admin2', 'Cambio el estado del Retiro', '', '2015-06-07'),
(64, 'admin2', 'Cambio el estado del Retiro', '', '2015-06-07'),
(65, 'admin3', 'Registro un Aporte', '', '2015-06-07'),
(66, 'admin3', 'Registro un Aporte', '', '2015-06-07'),
(67, 'admin3', 'Registro un Aporte', '', '2015-06-07'),
(68, 'admin3', 'Registro un Aporte', '', '2015-06-07'),
(69, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(70, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(71, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(72, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(73, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(74, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(75, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(76, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-07'),
(77, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(78, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(79, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(80, 'admin3', 'Registro un Aporte', '', '2015-06-08'),
(81, 'admin3', 'Registro un Aporte', '', '2015-06-08'),
(82, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(83, 'admin2', 'Cambio el estado del Retiro', '', '2015-06-08'),
(84, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(85, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(86, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(87, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(88, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(89, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(90, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(91, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(92, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(93, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(94, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(95, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(96, 'admin3', 'Registro un Aporte', '', '2015-06-08'),
(97, 'admin3', 'Registro un Aporte', '', '2015-06-08'),
(98, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(99, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(100, 'admin3', 'Solicitud de Retiro', '', '2015-06-08'),
(101, 'admin2', 'Cambio el estado del Retiro', '', '2015-06-08'),
(102, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(103, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(104, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-08'),
(105, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-08'),
(106, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-09'),
(107, 'admin3', 'Registro un socio', '', '2015-06-09'),
(108, 'admin3', 'Registro un socio', '', '2015-06-09'),
(109, 'admin3', 'Solicitud de prestamo para un socio', '', '2015-06-09'),
(110, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-09'),
(111, 'usuario', 'Solicitud de prestamo para un socio', '', '2015-06-09'),
(112, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-09'),
(113, 'usuario', 'Solicitud de prestamo para un socio', '', '2015-06-09'),
(114, 'usuario', 'Solicitud de prestamo para un socio', '', '2015-06-09'),
(115, 'admin2', 'Cambio el estado del Prestamo', '', '2015-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `liquidez`
--

CREATE TABLE IF NOT EXISTS `liquidez` (
  `idliquidez` int(11) NOT NULL AUTO_INCREMENT,
  `efectivo` float NOT NULL,
  `pasivo` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idliquidez`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `liquidez`
--

INSERT INTO `liquidez` (`idliquidez`, `efectivo`, `pasivo`, `fecha`) VALUES
(1, 10, 10, '2015-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plandeinversion`
--

CREATE TABLE IF NOT EXISTS `plandeinversion` (
  `idplandeinversion` int(11) NOT NULL AUTO_INCREMENT,
  `objetivo` varchar(100) NOT NULL,
  `concepto` varchar(100) NOT NULL,
  `inversion` decimal(10,0) NOT NULL,
  `ejecucion` varchar(100) NOT NULL,
  PRIMARY KEY (`idplandeinversion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `plandeinversion`
--

INSERT INTO `plandeinversion` (`idplandeinversion`, `objetivo`, `concepto`, `inversion`, `ejecucion`) VALUES
(1, 'Aprobar préstamos personales, a corto, mediano y largo plazo.', 'Otorgar préstamos personales, a corto, mediano y largo plazo.', '3000000', 'Enero 2015- Diciembre 2015'),
(2, 'Aprobar préstamos comerciales', 'Otorgar préstamos comerciales a los asociados', '2000000', 'Enero 2015- Diciembre 2015'),
(3, 'Aprobar préstamos hipotecarios', 'Otorgar préstamos hipotecarios', '1250000', 'Enero 2015- Diciembre 2015'),
(4, 'Otorgar préstamos de vehiculo', 'Otorgar préstamos de vehículos', '1250000', 'Enero 2015- Diciembre 2015');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plazos`
--

CREATE TABLE IF NOT EXISTS `plazos` (
  `idplazos` int(11) NOT NULL AUTO_INCREMENT,
  `plazo` int(11) NOT NULL,
  PRIMARY KEY (`idplazos`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `plazos`
--

INSERT INTO `plazos` (`idplazos`, `plazo`) VALUES
(1, 6),
(2, 12),
(3, 24),
(4, 36),
(5, 120),
(6, 240);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamo`
--

CREATE TABLE IF NOT EXISTS `prestamo` (
  `idprestamo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(50) NOT NULL,
  `tasa` float NOT NULL,
  `plazo` int(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`idprestamo`),
  UNIQUE KEY `codigo` (`codigo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `prestamo`
--

INSERT INTO `prestamo` (`idprestamo`, `tipo`, `tasa`, `plazo`, `descripcion`, `codigo`) VALUES
(1, '1', 10, 24, 'Prestamos Personales', '01'),
(2, '2', 10, 24, 'Prestamos Comerciales', '02'),
(3, '3', 8, 120, 'Prestamos para la adquisicion de Vehiculos', '03'),
(5, '4', 12, 36, 'Prestamos Especiales o con Fianza', '04'),
(6, '5', 12, 12, 'Prestamos Especiales sin Fiador', '05'),
(7, '6', 7, 240, 'Prestamos con Garantia Hipotecaria (Para adquisicion de vivienda', '06'),
(8, '7', 7, 240, 'Prestamos con Garantia Hipotecaria (Para remodelacion de vivienda', '07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socio`
--

CREATE TABLE IF NOT EXISTS `socio` (
  `idsocio` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(8) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `correo` varchar(75) NOT NULL,
  `lugardenacimiento` varchar(50) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `celular` varchar(12) NOT NULL,
  `departamento` int(2) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cantidaddefamiliares` int(2) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `fechadenacimiento` date NOT NULL,
  `estadocivil` varchar(50) NOT NULL,
  `telfonohab` varchar(12) NOT NULL,
  `gremiocargo` int(2) NOT NULL,
  `condicion` varchar(50) NOT NULL,
  `fechadeingreso` date NOT NULL,
  `fechadeafiliacion` date NOT NULL,
  `familiares` text NOT NULL,
  `salario` decimal(10,0) NOT NULL,
  PRIMARY KEY (`idsocio`),
  UNIQUE KEY `correo` (`correo`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `socio`
--

INSERT INTO `socio` (`idsocio`, `cedula`, `nombres`, `correo`, `lugardenacimiento`, `direccion`, `celular`, `departamento`, `estado`, `cantidaddefamiliares`, `apellidos`, `fechadenacimiento`, `estadocivil`, `telfonohab`, `gremiocargo`, `condicion`, `fechadeingreso`, `fechadeafiliacion`, `familiares`, `salario`) VALUES
(1, '19940339', 'cheche', 'cheche338@gmail.com', 'El Tigre', 'Valle Verde', '04248309075', 2, '2', 1, 'Delgado', '2014-03-04', 'Soltero', '02832351250', 4, 'Contratado', '2015-03-04', '2015-01-01', 'a:5:{i:0;s:14:"Javier delgado";i:1;s:8:"32165498";i:2;s:10:"2015-03-04";i:3;s:7:"hermao ";i:4;s:2:"15";}', '0'),
(3, '19940338', 'cheche', 'cheche38@gmail.com', 'El Tigre', 'Valle Verde', '04248309075', 20, '1', 1, 'Delgado', '2014-03-04', 'Soltero', '02832351250', 5, 'Contratado', '2015-03-04', '2015-02-04', 'a:5:{i:0;s:14:"Javier delgado";i:1;s:7:"3216549";i:2;s:10:"2015-03-04";i:3;s:7:"hermao ";i:4;s:2:"15";}', '0'),
(9, '19940340', 'cheche', 'aaaavbcvb@gmail.com', 'El Tigre', 'Valle Verde', '04248309075', 3, '1', 1, 'Delgado', '2014-03-04', 'Soltero', '02832351250', 4, 'Contratado', '2015-03-04', '2015-03-03', 'a:5:{i:0;s:14:"Javier delgado";i:1;s:7:"3216549";i:2;s:10:"2015-03-04";i:3;s:7:"hermao ";i:4;s:2:"15";}', '0'),
(10, '39940338', 'cheche', 'cheche3bbnbv38@gmail.com', 'El Tigre', 'Valle Verde', '04248309075', 13, '1', 1, 'Delgado', '2014-03-04', 'Viudo', '02832351250', 5, 'Ordinario', '2015-03-04', '2015-03-04', 'a:5:{i:0;s:14:"Javier delgado";i:1;s:8:"32165498";i:2;s:10:"2015-03-04";i:3;s:7:"hermao ";i:4;s:2:"15";}', '10750'),
(11, '22940338', 'Javier Antonio', 'cheche338@javier.com', 'El Tigre', 'Calle el carmen casa no 0-7', '04121880663', 5, '', 1, 'Delgado Alfonzo', '1990-07-10', 'Soltero', '02832351250', 5, 'Contratado', '2015-06-10', '2015-06-10', 'a:5:{i:0;s:15:"Claudio Delgado";i:1;s:7:"4657367";i:2;s:8:"El Tigre";i:3;s:4:"Papa";i:4;s:2:"57";}', '70000'),
(12, '55940338', 'Javier Delgado', 'cheche383@cheche.com', 'El Tigre', 'Valle ', '5465465', 4, '', 1, 'asdasd', '2015-06-04', 'Casado', '321345645', 4, 'Contratado', '2015-06-09', '2015-06-09', 'a:5:{i:0;s:6:"sdfdsf";i:1;s:7:"2456456";i:2;s:6:"dfgdfg";i:3;s:6:"sdfsdf";i:4;s:2:"12";}', '74000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudprestamo`
--

CREATE TABLE IF NOT EXISTS `solicitudprestamo` (
  `idsolicitudprestamo` int(11) NOT NULL AUTO_INCREMENT,
  `idsocio` int(11) NOT NULL,
  `idtipoprestamo` int(11) NOT NULL,
  `cuotas` int(11) NOT NULL,
  `cantidadprestamo` float NOT NULL,
  `cheque` double NOT NULL,
  `interes` float NOT NULL,
  `cuotaquincenal` float NOT NULL,
  `totalhaberes` float NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `fechadeaprobacion` date NOT NULL,
  PRIMARY KEY (`idsolicitudprestamo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `solicitudprestamo`
--

INSERT INTO `solicitudprestamo` (`idsolicitudprestamo`, `idsocio`, `idtipoprestamo`, `cuotas`, `cantidadprestamo`, `cheque`, `interes`, `cuotaquincenal`, `totalhaberes`, `estado`, `fecha`, `fechadeaprobacion`) VALUES
(14, 3, 0, 0, 0, 700000, 11, 0, 0, 1, '2015-06-01', '2015-06-09'),
(15, 11, 2, 0, 0, 500000, 10, 0, 0, 1, '2015-06-01', '2015-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudretiro`
--

CREATE TABLE IF NOT EXISTS `solicitudretiro` (
  `idretiro` int(11) NOT NULL AUTO_INCREMENT,
  `idsocio` int(11) NOT NULL,
  `tipoderetiro` int(11) NOT NULL,
  `monto` float NOT NULL,
  `justificacion` text NOT NULL,
  `totalhaberes` float NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idretiro`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `solicitudretiro`
--

INSERT INTO `solicitudretiro` (`idretiro`, `idsocio`, `tipoderetiro`, `monto`, `justificacion`, `totalhaberes`, `estado`, `fecha`) VALUES
(1, 9, 1, 213, 'prueba', 0, 3, '2015-06-04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solvencia`
--

CREATE TABLE IF NOT EXISTS `solvencia` (
  `idsolvencia` int(11) NOT NULL AUTO_INCREMENT,
  `activo` float NOT NULL,
  `pasivo` float NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`idsolvencia`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `solvencia`
--

INSERT INTO `solvencia` (`idsolvencia`, `activo`, `pasivo`, `fecha`) VALUES
(1, 50, 200, '2015-06-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoderetiro`
--

CREATE TABLE IF NOT EXISTS `tipoderetiro` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipoderetiro`
--

INSERT INTO `tipoderetiro` (`idtipo`, `nombre`) VALUES
(1, 'Total'),
(2, 'Parcial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario3`
--

CREATE TABLE IF NOT EXISTS `usuario3` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tipo` int(1) NOT NULL,
  `cedula` varchar(12) NOT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `usuario3`
--

INSERT INTO `usuario3` (`idusuario`, `nombre`, `apellido`, `usuario`, `password`, `tipo`, `cedula`) VALUES
(1, 'Javier', 'Delgado', 'admin', 'admin', 1, '1'),
(2, 'Luis', 'Dian', 'admin2', 'admin', 2, '2'),
(3, 'cheche', 'delgado', 'admin3', 'admin', 3, '19940338'),
(13, 'cheche', 'Delgado', 'usuario', 'usuario', 4, '19940339');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
