function eventos(){
	eventosMenuPrincipal();
	comprobarsesion();

}

function cerrarsesion(){
	  $('#cerrarsesion').on('click',  function(){

	  	console.log("click en cerrar sesion");
	  	$.ajax
			({
			type: "POST",
		   	url: "controlador/controladorSesion.php",
		   	data: {id:3},
			async: false,	
				
			success:
		    function () 
			{	
				
				window.open('index.php' , '_self');
		     },
		        error:
		        function () {console.log( msg +"  No se pudo realizar la conexion");}
			});
    });
}

function eventosAdministrador(){
	$("#wait").css("display", "block");
    $("#contenedor").load('partials/administrador/configurarsistema.html', function(){
		 	$("#wait").css("display", "none");
		 });
	$('#configurarsistema').addClass("active");

    
	$('#configurarsistema').on('click',  function(e){
		console.log("configurarsistema");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		 $("#contenedor").load('partials/administrador/configurarsistema.html', function(){
		 	$("#wait").css("display", "none");
			$('#configurarsistema').addClass("active");
			$('#gestionarindicador').removeClass("active");
			
		});

	});
	cerrarsesion();
}
function eventosPresidente(){
	cerrarsesion();	
   	 $("#wait").css("display", "block");

   	 	$("#contenedor").load('partials/administrador/gestionarindicador.html', function(){
			$("#wait").css("display", "none");			
		});

   	 $('#gestionarindicador').on('click',  function(e){
		console.log("gestionarsocio");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		 $("#contenedor").load('partials/administrador/gestionarindicador.html', function(){
		 	$("#wait").css("display", "none");

			$('#gestionarindicador').addClass("active");
			$('#configurarsistema').removeClass("active");
			$('#darpermiso1').removeClass("active");
			$('#darpermiso2').removeClass("active");
	
			
		});

	});
	$('#verificarreporte').on('click',  function(e){
		console.log("verificarreporte");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		/*$("#contenedor").load('partials/presidente/verificarreporte.html', function(){
			$("#wait").css("display", "none");
			$('#verificarreporte').addClass("active");
			$('#darpermiso1').removeClass("active");
			$('#darpermiso2').removeClass("active");			

		});*/
		window.open('verificarreporte.html' , "_blank");
	});
	$('#darpermiso1').on('click',  function(e){
		console.log("darpermiso1");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/presidente/prestamos.html', function(){
			$("#wait").css("display", "none");
			$('#darpermiso1').addClass("active");
			$('#verificarreporte').removeClass("active");
			$('#darpermiso2').removeClass("active");	
			$('#gestionarindicador').removeClass("active");	

		});
	});


	$('#darpermiso2').on('click',  function(e){
		console.log("darpermiso1");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/presidente/retiros.html', function(){
			$("#wait").css("display", "none");
			$('#darpermiso2').addClass("active");
			$('#verificarreporte').removeClass("active");	
			$('#darpermiso1').removeClass("active");		
			$('#gestionarindicador').removeClass("active");	
				
		});
	});

}
function eventosSecretaria(){
	cerrarsesion();	
	    $("#contenedor").load('partials/secretaria/registrosocio.php');
	$('#gestionarsocio').on('click',  function(e){
		console.log("gestionarsocio");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/secretaria/registrosocio.php', function(){
			$("#wait").css("display", "none");

			$('#gestionarsocio').addClass("active");
		//	$('#gestionarsocio').removeClass("active");
			$('#gestionarsolicitud').removeClass("active");
			$('#consultar').removeClass("active");
			$('#gestionaraportemanual').removeClass("active");
			
		});

	});
	$('#gestionarsolicitud').on('click',  function(e){
		console.log("gestionarsolicitud");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/secretaria/gestionarsolicitud.php', function(){
			$("#wait").css("display", "none");
			$('#gestionarsolicitud').addClass("active");
			$('#gestionarsocio').removeClass("active");
			//$('#gestionarsolicitud').removeClass("active");
			$('#consultar').removeClass("active");
			$('#gestionaraportemanual').removeClass("active");			
		});

	});
	$('#consultar').on('click',  function(e){
		console.log("consultar");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/secretaria/consultar.php', function(){
			$("#wait").css("display", "none");
			$('#consultar').addClass("active");
			$('#gestionarsocio').removeClass("active");
			$('#gestionarsolicitud').removeClass("active");
			//$('#consultar').removeClass("active");
			$('#gestionaraportemanual').removeClass("active");			
		});
	});
	$('#gestionaraportemanual').on('click',  function(e){
		console.log("gestionaraportemanual");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/secretaria/gestionaraportemanual.php', function(){
			$("#wait").css("display", "none");
			$('#gestionaraportemanual').addClass("active");
			$('#gestionarsocio').removeClass("active");
			$('#gestionarsolicitud').removeClass("active");
			$('#consultar').removeClass("active");
			//$('#gestionaraportemanual').removeClass("active");
			
		});
	});

}

function eventosUsuarioSocio(){
	cerrarsesion();	
       	 $("#wait").css("display", "block");

	$("#contenedor").load('partials/socio/solicitud.php', function(){
			$("#wait").css("display", "none");
			$('#gestionarsolicitud').addClass("active");
			//$('#gestionarsolicitud').removeClass("active");
			$('#consultar').removeClass("active");
		});

		$('#gestionarsolicitud').on('click',  function(e){
		console.log("gestionarsolicitud");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/socio/solicitud.php', function(){
			$("#wait").css("display", "none");
			$('#gestionarsolicitud').addClass("active");
			//$('#gestionarsolicitud').removeClass("active");
			$('#consultar').removeClass("active");
		});

	});
	$('#consultar').on('click',  function(e){
		console.log("consultar");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").load('partials/secretaria/consultar.php', function(){
			$("#wait").css("display", "none");
			$('#consultar').addClass("active");
			$('#gestionarsolicitud').removeClass("active");
			//$('#consultar').removeClass("active");
		});
	});

}

function comprobarsesion(){
	 $.ajax
        ({
          type: "POST",
          url: "controlador/controladorSesion.php",
          data: {id:2},
          async: false,  
          dataType: "json",  
          success:
          function (msg) 
        { 
        		console.log("bandera de sesion: "+msg[0].tipo);
              if(msg[0].salida=="true"&&msg[0].tipo==1){
              		eventosAdministrador();
              }
              if(msg[0].salida=="true"&&msg[0].tipo==2){
              		eventosPresidente();
              }
              if(msg[0].salida=="true"&&msg[0].tipo==3){
              		eventosSecretaria();
              }
              if(msg[0].salida=="true"&&msg[0].tipo==4){
              		eventosUsuarioSocio();
              }
              else
              {
              	console.log("Sesion no abierta");
              }
        },
          error:
          function (msg) {console.log( msg +" No se pudo realizar la conexion");}
        });
}

function eventosMenuPrincipal(){
	$('#ingresaralsistema').on('click',  function(e){
		console.log("click en ingresaralsistema");
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").hide().load('partials/principal/login.html', function(){	
			$("#wait").css("display", "none");

		}).fadeIn(1500);

	});
	$('#misionyvida').on('click',  function(e){	
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").hide().load('partials/principal/misionyvida.html', function(){		
			$("#wait").css("display", "none");

		}).fadeIn(1500);
	});
	$('#organigrama').on('click',  function(e){
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").hide().load('partials/principal/organigrama.html', function(){		
			$("#wait").css("display", "none");

		}).fadeIn(1500);
	});
	$('#plandeinversion').on('click',  function(e){
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").hide().load('partials/principal/plandeinversion.html', function(){	
			$("#wait").css("display", "none");

		}).fadeIn(1500);
	});
	$('#ayuda').on('click',  function(e){
		e.preventDefault();
       	 $("#wait").css("display", "block");
		$("#contenedor").hide().load('partials/principal/ayuda.html', function(){
			$("#wait").css("display", "none");

		}).fadeIn(1500);
	});

}
function ActivarLogin(){
	console.log("entro en panel activar login");
			$("#menuizquierdo").hide().load('partials/login.html', function(){
				$('#iniciarsesion').on('click',  function(){
						if(temail.value!=""&&tpass.value!=""){
							$.ajax
							({
							type: "POST",
						   	url: "controlador/controladorSesion.php",
						   	data: {id:1, temail:temail.value, tpass:tpass.value},
							async: false,			
							success:
						    function (msg) 
							{	console.log(msg);
								
								if(msg=="false"){		
									alertas.innerHTML="<div class='alert alert-danger'>Datos Invalidos</div>";
								}
								else{comprobarlogeo();}
								

								
						     },
						        error:
						        function (msg) {console.log(msg +"No se pudo realizar la conexion Comuniquese con el administrador por favor");}
							});
							
							}
							else alertas.innerHTML="<div class='alert alert-danger'>Llene con sus datos</div>";
				
			});


			}).fadeIn(1500);
	}