<!DOCTYPE html>
<html>
<head>
	<title>Cautriut</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
    <link href="css/bootstrapValidator.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="css/formValidation.css">
    <link rel="stylesheet" type="text/css" href="css/datePicker.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
    <style type="text/css">
    	.alert {
			  padding: 12px;
			  margin-bottom: 12px;
			}
		.page-header {
		  padding-bottom: 0px;
		  margin: 0px 0 2px;
		}
    </style>
</head>
<body>
<br>
<div class="container well" style="padding-top: 1%;">
	<div class="page-header">
		<div class="row">
			<div class="col-md-3"><img src="img/catriu1.png" style="width:240px; height:110px; "></div>
			<div class="col-md-9">
				
				<center>
					<h3><b>SISTEMA DE INFORMACIÓN BAJO AMBIENTE WEB DE APOYO A LA TOMA DE DECISIONES PARA EL OTORGAMIENTO DE CRÉDITOS DE LA CAJA DE AHORROS CATRIUT
					</b></h3>	
				</center>		
			</div>
		</div>
	</div>
	
</div>
<div class="container well" style="padding-top: 1%;" id="menu">
					<?php
                        session_start(); 
                        if(isset($_SESSION['tipo'])&&($_SESSION['tipo']=="1"||$_SESSION['tipo']==1)){    ?>

		                    <ul class="nav nav-pills">
								
								<li id="configurarsistema"><a href="#">Configurar Sistema</a></li>
								 <ul class="nav nav-pills navbar-right">
								 		<li id="nombretipo" ><p><b>Administrador: </b> <?php echo $_SESSION['nombre'] ?></p></li>
										<li id="cerrarsesion"><a href="#">Cerrar Sesion</a></li>
								 </ul>						
							</ul>

						<?php }if(isset($_SESSION['tipo'])&&($_SESSION['tipo']=="2"||$_SESSION['tipo']==2)){ 	 ?>

						    <ul class="nav nav-pills">
						    	<li id="gestionarindicador" class="active"><a href="#">Gestionar Indicador</a></li>
								<li id="verificarreporte" ><a href="#">Verificar Reporte</a></li>
								<li id="darpermiso1"><a href="#">Permiso Prestamos</a></li>
								<li id="darpermiso2"><a href="#">Permiso Retiros</a></li>
								 <ul class="nav nav-pills navbar-right">
								 		<li id="nombretipo" ><p><b>Presidente/Cons.Adm: </b> <?php echo $_SESSION['nombre'] ?></p></li>
										<li id="cerrarsesion"><a href="#">Cerrar Sesion</a></li>
								 </ul>								
							</ul>

						<?php }if(isset($_SESSION['tipo'])&&($_SESSION['tipo']=="3"||$_SESSION['tipo']==3)){ 	 ?>
						<ul class="nav nav-pills">
								<li id="gestionarsocio" class="active"><a href="#">Gestionar Socio</a></li>
								<li id="gestionarsolicitud"><a href="#">Gestionar Solicitud</a></li>
								<li id="consultar"><a href="#">Consultar</a></li>
								<li id="gestionaraportemanual"><a href="#">Gestionar Aporte Manual</a></li>
								 <ul class="nav nav-pills navbar-right">
								 		<li id="nombretipo" ><p><b>Secretaria: </b> <?php echo $_SESSION['nombre'] ?></p></li>
										<li id="cerrarsesion"><a href="#">Cerrar Sesion</a></li>
								 </ul>								
							</ul>

						<?php }if(isset($_SESSION['tipo'])&&($_SESSION['tipo']=="4"||$_SESSION['tipo']==4)){ 	 ?>
							<ul class="nav nav-pills">								
								<li id="gestionarsolicitud"><a href="#">Gestionar Solicitud</a></li>
								<li id="consultar"><a href="#">Consultar</a></li>
								<ul class="nav nav-pills navbar-right">
								 		<li id="nombretipo" ><p><b>Secretaria: </b> <?php echo $_SESSION['nombre'] ?></p></li>
										<li id="cerrarsesion"><a href="#">Cerrar Sesion</a></li>
								 </ul>																
							</ul>

						<?php } if(!isset($_SESSION['tipo'])){		?>

						<ul class="nav nav-pills">
							<li id="ingresaralsistema"><a href="#">Ingresar Al Sistema</a></li>
							<li id="misionyvida"><a href="#">Misión y Visión</a></li>
							<li id="organigrama"><a href="#">Organigrama</a></li>
							<li id="plandeinversion"><a href="#">Plan de Inversion</a></li>
							<li id="ayuda" style="display:none;"><a href="#">Ayuda</a></li>
						</ul>

						<?php } ?>


</div>
	<div class="container" >

		<div class="panel panel-default">
		  <!-- <div class="panel-heading">Panel heading</div> -->
		  <!-- <div class="panel-body" id="contenedor" style="height:400px;"> -->

		  <div class="panel-body">
		  	 <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='img/demo_wait.gif' width="64" height="64" /><br>Cargando</div>
				<div class="panel-body" id="contenedor">
						<h3>
							<p  align="justify">Bienvenidos al SISTEMA DE INFORMACIÓN BAJO AMBIENTE WEB DE APOYO A LA TOMA DE DECISIONES PARA EL OTORGAMIENTO DE CRÉDITOS DE LA CAJA DE AHORRO de los trabajadores del Instituto Universitario de Tecnología “Jacinto Navarro Vallenilla”, conocida por sus siglas “CATRIUT”, ubicada dentro de las instalaciones de la Universidad Politécnica Territorial de Paria “Luis Mariano Rivera”(U.P.T.P), Carúpano, Estado Sucre.

							<br><br><br>
							En este sistema usted podrá realizar operaciones como consultar su estado de cuenta , realizar peticiones de prestamos, operaciones administrativas de cuenta de los usuarios, entre otros; según el tipo de acceso al mismo.
							</p>
						</h3>
		  		</div>
		  </div>
		</div>
	</div>

	<DIV class="container well">
 		<footer> 
 			<h4>
 				<center>
 					<b>Todos 	los derechos Reservados @2015 CATRIUT</b>
 					
 				</center>
 			
 			</h4>
		</footer>
		<img src="img/catriu2.png" id="deco">
	</DIV>

	 <script src="js/jquery-2.1.1.min.js"></script>
	  <script src="js/dist/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-notify.js"></script>

    <script src="js/eventos2.js"></script>
    <script src="js/date.js"></script>
    <script src="js/jquery.datePicker.js"></script>


    <script type="text/javascript">
        eventos();
        
    </script>
</body>
</html>