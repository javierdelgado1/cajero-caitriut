<?php

include("../mpdf.php");

$mpdf=new mPDF('c','A4','','',32,25,47,47,10,10); 

$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins



$html = '
<head><style type="text/css">html { height: 100%; width: 100%; }
body { box-sizing: border-box; cursor: text; font-family: "Helvetica", "Arial", sans-serif; font-size: 13px; height: 100%; line-height: 1.42; margin: 0px; overflow-x: hidden; overflow-y: auto; padding: 12px 15px; }
.editor-container { height: 100%; outline: none; position: relative; tab-size: 4; white-space: pre-wrap; }
.editor-container p { margin: 0; padding: 0; }
.editor-container a { text-decoration: underline; }
.editor-container b { font-weight: bold; }
.editor-container i { font-style: italic; }
.editor-container s { text-decoration: line-through; }
.editor-container u { text-decoration: underline; }
.editor-container img { max-width: 100%; }
.editor-container blockquote { margin: 0 0 0 2em; padding: 0; }
.editor-container ol { margin: 0 0 0 2em; padding: 0; list-style-type: decimal; }
.editor-container ul { margin: 0 0 0 2em; padding: 0; list-style-type: disc; }
.editor-container ol > li > ol { list-style-type: lower-alpha; }
.editor-container ol > li > ol > li > ol { list-style-type: lower-roman; }
.editor-container ol > li > ol > li > ol > li > ol { list-style-type: decimal; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol { list-style-type: lower-alpha; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol > li > ol { list-style-type: lower-roman; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol { list-style-type: decimal; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol { list-style-type: lower-alpha; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol { list-style-type: lower-roman; }
.editor-container ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol > li > ol { list-style-type: decimal; }</style><style type="text/css">.snow .image-tooltip-container a { border: 1px solid #06c; }
.snow .image-tooltip-container a.insert { background-color: #06c; color: #fff; }
.snow .cursor-name { border-radius: 4px; font-size: 11px; font-family: Arial; margin-left: -50%; padding: 4px 10px; }
.snow .cursor-triangle { border-left: 4px solid transparent; border-right: 4px solid transparent; height: 0px; margin-left: -3px; width: 0px; }
.snow .cursor.left .cursor-name { margin-left: -8px; }
.snow .cursor.right .cursor-flag { right: auto; }
.snow .cursor.right .cursor-name { margin-left: -100%; margin-right: -8px; }
.snow .cursor-triangle.bottom { border-top: 4px solid transparent; display: block; margin-bottom: -1px; }
.snow .cursor-triangle.top { border-bottom: 4px solid transparent; display: none; margin-top: -1px; }
.snow .cursor.top .cursor-triangle.bottom { display: none; }
.snow .cursor.top .cursor-triangle.top { display: block; }
.snow a { color: #06c; }
.snow .tooltip { border: 1px solid #ccc; box-shadow: 0px 0px 5px #ddd; color: #222; }
.snow .tooltip a { color: #06c; }
.snow .tooltip .input { border: 1px solid #ccc; margin: 0px; padding: 5px; }
.snow .image-tooltip-container .preview { border-color: #ccc; color: #ccc; }
.snow .link-tooltip-container a, .snow .link-tooltip-container span { display: inline-block; line-height: 25px; }</style><style type="text/css">.link-tooltip-container { padding: 5px 10px; }
.link-tooltip-container input.input { width: 170px; }
.link-tooltip-container input.input, .link-tooltip-container a.done, .link-tooltip-container.editing a.url, .link-tooltip-container.editing a.change { display: none; }
.link-tooltip-container.editing input.input, .link-tooltip-container.editing a.done { display: inline-block; }
.tooltip { background-color: #fff; border: 1px solid #000; top: 0px; white-space: nowrap; z-index: 2000; }
.tooltip a { cursor: pointer; text-decoration: none; }</style><style type="text/css">.image-tooltip-container { margin: 25px; padding: 10px; width: 300px; }
.image-tooltip-container:after { clear: both; content: ""; display: table; }
.image-tooltip-container .preview { margin: 10px 0px; position: relative; border: 1px dashed #000; height: 200px; }
.image-tooltip-container .preview span { display: inline-block; position: absolute; text-align: center; top: 40%; width: 100%; }
.image-tooltip-container img { bottom: 0; left: 0; margin: auto; max-height: 100%; max-width: 100%; position: absolute; right: 0; top: 0; }
.image-tooltip-container .input { box-sizing: border-box; width: 100%; }
.image-tooltip-container a { border: 1px solid black; box-sizing: border-box; display: inline-block; float: left; padding: 5px; text-align: center; width: 50%; }
.tooltip { background-color: #fff; border: 1px solid #000; top: 0px; white-space: nowrap; z-index: 2000; }
.tooltip a { cursor: pointer; text-decoration: none; }</style><style type="text/css">.cursor-container { position: absolute; left: 0; top: 0; z-index: 1000; }
.cursor { margin-left: -1px; position: absolute; }
.cursor-flag { bottom: 100%; position: absolute; white-space: nowrap; }
.cursor-name { display: inline-block; color: white; padding: 2px 8px; }
.cursor-caret { height: 100%; position: absolute; width: 2px; }
.cursor.hidden .cursor-flag { display: none; }
.cursor.top > .cursor-flag { bottom: auto; top: 100%; }
.cursor.right > .cursor-flag { right: -2px; }</style><style type="text/css">.paste-container { left: -10000px; position: absolute; top: 50%; }</style></head><body class="snow"><div class="cursor-container"></div><div id="quill-1" class="editor-container authorship" contenteditable="true"><p class="line" id="line-53" style="text-align: right;">Caracas, 25 de Abril de 2015</p><p class="line" id="line-55" style="text-align: right;"><b>Oficio Nº: H00150</b></p><p class="line" id="line-57"><br></p><p class="ql-editor line" id="line-59" contenteditable="true"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Vicealmirante</b></p><p class="ql-paste-manager line" contenteditable="true" id="line-61"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Clemente Antonio Díaz</b></p><p class="ql-paste-manager line" contenteditable="true" id="line-63"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Comandante Naval de Operaciones</b></p><p class="ql-paste-manager line" contenteditable="true" id="line-65"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Armada Nacional Bolivariana</b></p><p class="ql-paste-manager line" contenteditable="true" id="line-67"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Su Despacho.-</b></p><p class="line" id="line-69"><br></p><p class="line" id="line-71" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">Distinguido Vicealmirante: </span></p><p class="line" id="line-73" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-75" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">Tengo el agrado de dirigirme a usted en la oportunidad de expresarle un gran saludo, en nombre de mi represenada la "Empresa Estatal de Comercio Exterior" Belspetsneshtechnika (BSVT), de la Republica de Balrus, aprovechando la ocasion </span><span style="font-family: Helvetica, sans-serif; font-size: 10pt; text-align: justify;">para desearle exito en su gestion.</span></p><p class="line" id="line-78" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-80" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-82" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">En esta Ocasion, se le Remite Oferta Comercion N 09/5391-1, emitida por mi representada, correspondiente a la formacion y mantenimiento de </span><span style="font-family: Helvetica, sans-serif; font-size: 10pt; text-align: justify;">militares en la facultad tecnico-militar.</span></p><p class="line" id="line-85" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-87" style="text-align: justify;"><br></p><p class="line" id="line-89" style="text-align: justify;"><br></p><p class="line" id="line-91" style="text-align: justify;"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">Agradeciendo de antemano y quedando a su disposicion.</span></p><p class="line" id="line-93"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-95"><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-97" style="text-align: center;"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Atentamente&nbsp;</b><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-100" style="text-align: center;"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Soledad Jaimes</b><span style="font-family: Helvetica, sans-serif; font-size: 10pt;">&nbsp;</span></p><p class="line" id="line-103" style="text-align: center;"><b style="font-family: Helvetica, sans-serif; font-size: 10pt;">Representante de BSVT en Venezuela</b></p></div><div class="tooltip link-tooltip-container" style="position: absolute; left: -10000px;"><span class="title">Visit URL:&nbsp;</span><a href="#" class="url" target="_blank"></a><input class="input" type="text"><span>&nbsp;-&nbsp;</span><a href="javascript:;" class="change">Change</a><a href="javascript:;" class="done">Done</a></div><div class="tooltip image-tooltip-container" style="position: absolute; left: -10000px;"><input class="input" type="textbox"><div class="preview"><span>Preview</span></div><a href="javascript:;" class="cancel">Cancel</a><a href="javascript:;" class="insert">Insert</a></div><div class="paste-container" contenteditable="true"></div></body>
';

$mpdf->WriteHTML($html);

$mpdf->Output();
exit;

?>