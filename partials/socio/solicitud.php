<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#SolicitarPrestamo" aria-controls="SolicitarPrestamo" role="tab" data-toggle="tab">Solicitar Prestamo</a></li>
    <li role="presentation" style="display:none;"><a href="#solicitarretiro" aria-controls="solicitarretiro" role="tab" data-toggle="tab">Solicitar Retiro</a></li>
    <li role="presentation" style="display:none;"><a href="#detalledelprestamo" aria-controls="detalledelprestamo" role="tab" data-toggle="tab">Detalle del Prestamo</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="SolicitarPrestamo">
    	<br>
    				<center>
    					
    				<div id="alertas"> <div class="alert alert-info"><h4><b>Solicitud de Prestamo</b></h4></div></div>
    				</center>

				<hr> 
				<br>
		<section id="resultado">
			
		</section>
		<section id="alerta">
			
		</section>
		<section id="formulario" style="display:none;">
		
			<div class="row">
				<div class="col-md-5">
					 	   <div class="form-group">
					          <label class="col-sm-7 control-label">Nombres:</label>
					          <div class="col-sm-5">
					              <input  id="tnombre" type="text" class="form-control input-sm" name="nombre" disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Total Haberes Caja Bs:</label>
					          <div class="col-sm-5">
					              <input  id="ttotalhaberes" type="text" class="form-control input-sm" name="totalhaberes" disabled="true" />  
					          </div>
					      </div>
				</div>
				<div class="col-md-7">
						  <div class="form-group">
					          <label class="col-sm-7 control-label">Apellidos:</label>
					          <div class="col-sm-5">
					              <input  id="tapellido" type="text" class="form-control input-sm" name="apellido"  disabled="true"/>  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Fecha de Afiliacion:</label>
					          <div class="col-sm-5">
					              <input  id="tfecha" type="text" class="form-control input-sm" name="fecha" disabled="true"/>  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Haberes disponibles(80%)</label>
					          <div class="col-sm-5">
					              <input  id="thaberesdisponibles" type="text" class="form-control input-sm" name="haberesdisponibles" disabled="true" />  
					          </div>
					      </div>
				</div>
			</div>
			<hr>
				<center><section id="alertasPrestamo"></section></center>
		<form id="formsolicitudprestamo2" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 

			<div class="row">
				<div class="col-md-5">
						<div class="form-group">
					          <label class="col-sm-7 control-label">Fecha de Solicitud:</label>
					          <div class="col-sm-5">
					              <input  id="tfechasolicitud" type="text" class="form-control input-sm" name="fechasolicitud" value=<?php echo date("d-m-Y");  ?> disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Tipo de Préstamo:</label>
					          <div class="col-sm-5">
					               <select  class="form-control input-sm"   name="tipoprestamo"  id="ttipoprestamo" >
										<option value="0"disabled selected> --Seleccione una opcion -- </option>
					               		<option value="1">Préstamos Personales</option>
					               		<option value="2">Préstamos Comerciales</option>
					               		<option value="3">Préstamos para la adquisicion de Vehiculos</option>	
					               		<option value="4">Préstamos Especiales o con Fianza</option>            		
					               		<option value="5">Préstamos Especiales sin Fiador</option>
					               		<option value="6">Préstamos con Garantía Hipotecaria (Para adquisicion de vivienda)</option>
					               		<option value="7">Préstamos con Garantía Hipotecaria (Para remodelacion de vivienda)</option> 
					               </select>
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Préstamo Solicitado:</label>
					          <div class="col-sm-5">
					              <input  id="tprestamosolicitado" type="text" class="form-control input-sm" name="prestamosolicitado" />  
					          </div>
					      </div>
				</div>
				<div class="col-md-7">
						<div class="form-group">
					          <label class="col-sm-7 control-label">Código del préstamo:</label>
					          <div class="col-sm-5">
					              <input  id="tcodigodelprestamo" type="text" class="form-control input-sm" name="codigodelprestamo" disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Tasa de intéres:</label>
					          <div class="col-sm-5">
					              <input  id="ttazadeinteres" type="text" class="form-control input-sm" name="tazadeinteres" disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label"># de cuotas (plazo)</label>
					          <div class="col-sm-5">
					              <input  style="display:block;" id="tNumerodecuentas" type="text" class="form-control input-sm" name="Numerodecuentas" disabled="true"/>  
				          		  <select  class="form-control input-sm"   name="cuotas"  id="tcuotas" style="display:none;">
										
					               		<option value="6">6 Meses</option>
					               		<option value="12">12 Meses</option>
					               		<option value="24" selected>24 Meses</option>					               		
					               </select>
					          </div>
					      </div>




					      
				</div>
			</div>
			<hr>
				  <div class="form-group">
			          <label class="col-sm-9 control-label"></label>
			          <div class="col-sm-3">
			               <button type="submit" class="btn btn-sm btn-default" id="calcular" >Calcular tabla de amortización</button>		  
			          </div>
			      </div>
			<hr>
			</form>
			<center>
		   			<div class="alert alert-warning"><h4><b>Préstamos anteriores y vigentes</b></h4></div>			   						
			</center>
			<hr>
			<div id="resultado3"> </div>

			<div class="row">
				<div class="col-md-5">
						<div class="form-group">
					          <label class="col-sm-7 control-label">Total préstamo cheque</label>
					          <div class="col-sm-5">
					              <input  id="ttotalprestamocheque" type="text" class="form-control input-sm" name="totalprestamocheque"  disabled="true"/>  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Monto Cuota quincenal</label>
					          <div class="col-sm-5">
					              <input  id="tmontocuotaquincenal" type="text" class="form-control input-sm" name="montocuotaquincenal" disabled="true" />  
					          </div>
					      </div>
				</div>
				<div class="col-md-7">
						  <div class="form-group">
					          <label class="col-sm-7 control-label">Total interés</label>
					          <div class="col-sm-5">
					              <input  id="ttotalinteres" type="text" class="form-control input-sm" name="totalinteres" disabled="true" />  
					          </div>
					      </div>
				</div>
			</div>
			<hr>
			<center>
		         <div class="row">
					<div class="col-md-4">
		                  <button type="submit" class="btn btn-sm btn-default" id="procesar"><span class="glyphicon glyphicon-cog"></span>Procesar</button>				
					</div>
					<div class="col-md-4">
		                  <button type="button" class="btn btn-sm btn-primary" id="limpiar"><span class="glyphicon glyphicon-trash"></span>Limpiar</button>					
					</div>
					<div class="col-md-4">
		                  <button type="button" class="btn btn-sm btn-danger" id="cancelar"><span class="glyphicon glyphicon-remove"></span>Cancelar</button>				
					</div>
				</div>        	
	        </center>
	</section>

    </div> 
   <!--  ...................................................... Solicitar Retiro    ..................................................... -->
    <div role="tabpanel" class="tab-pane" id="solicitarretiro">
    			<center>
			      <br>
			      	 <div class="alert alert-info"><h4><b>Información del retiro</b></h4></div>		
					<hr>
					<h5><b>Ingrese la cédula o el código a buscar</b></h5>
					<hr>				
			      </center>  
  			       <div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-sm-4 control-label">Cédula de identidad:</label>
					          <div class="col-sm-4">
					              <input  id="cedulabusqueda2" type="text" class="form-control input-sm" name="cedulabusqueda2" />  
					          </div>
					          <div class="col-sm-4">
					             <button type="button" class="btn btn-sm btn-info" id="buscar2"><span class="glyphicon glyphicon-search"></span>Buscar</button>	 
					          </div>
				          </div>
						</div>
						
					</div>
					<div id="resultado2"> </div>
				
				<hr> 
					
<section id="formulario2" style="display:none;">
				<form id="formsolicitudprestamo3" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 
					<div class="row">
						<div class="col-md-5">
							 	   <div class="form-group">
							          <label class="col-sm-7 control-label">Nombres:</label>
							          <div class="col-sm-5">
							              <input  id="tnombre2" type="text" class="form-control input-sm" name="nombre" / disabled="true" />  
							          </div>
							      </div>
							      <div class="form-group">
							          <label class="col-sm-7 control-label">Total Haberes Caja Bs:</label>
							          <div class="col-sm-5">
							              <input  id="ttotalhaberes2" type="text" class="form-control input-sm" name="totalhaberes2" disabled="true" />  
							          </div>
							      </div>
						</div>
						<div class="col-md-7">
								  <div class="form-group">
							          <label class="col-sm-7 control-label">Apellidos:</label>
							          <div class="col-sm-5">
							              <input  id="tapellido2" type="text" class="form-control input-sm" name="apellido2" disabled="true"/>  
							          </div>
							      </div>
							      <div class="form-group">
							          <label class="col-sm-7 control-label">Fecha de Afiliacion:</label>
							          <div class="col-sm-5">
							              <input  id="tfecha2" type="text" class="form-control input-sm" name="fecha2" disabled="true" />  
							          </div>
							      </div>
							      <div class="form-group">
							          <label class="col-sm-7 control-label">Haberes disponibles(80%)</label>
							          <div class="col-sm-5">
							              <input  id="thaberesdisponibles2" type="text" class="form-control input-sm" name="haberesdisponibles2" disabled="true" />  
							          </div>
							      </div>
						</div>
					</div>
				<hr>
				<div class="row">
					<div class="col-md-5">
				 		<div class="form-group">
					          <label class="col-sm-7 control-label">Fecha de Sol. de Retiro</label>
					          <div class="col-sm-5">
					              <input  id="tfechasolicitudretiro" type="text" class="form-control input-sm" name="fechasolicitudretiro" value=<?php echo date("d-m-Y");  ?> disabled="true" />  
					          </div>
					      </div>
					       <div class="form-group">
						          <label class="col-sm-7 control-label">Tipo de Retiro</label>
						          <div class="col-sm-5">
						              <input  id="ttipoderetiro" type="text" class="form-control input-sm" name="tipoderetiro" />  
						          </div>
					      </div>
					       <div class="form-group">
					          <label class="col-sm-7 control-label">Justificación del Retiro</label>
					          <div class="col-sm-5">
					              <input  id="tjustificaciondelretiro" type="text" class="form-control input-sm" name="justificaciondelretiro" />  
					          </div>
					       </div>
					</div>
					<div class="col-md-7">
						 <div class="form-group">
						          <label class="col-sm-7 control-label">Código del Retiro</label>
						          <div class="col-sm-5">
						              <input  id="tcodigodelretiro2" type="text" class="form-control input-sm" name="codigodelretiro2" />  
						          </div>
					      </div>
					       <div class="form-group">
					          <label class="col-sm-7 control-label">Monto a retirar</label>
					          <div class="col-sm-5">
					              <input  id="tmontoaretirar" type="text" class="form-control input-sm" name="montoaretirar" />  
					          </div>
					      </div>
					</div>
				</div>
				<hr>
			<center>
		         <div class="row">
					<div class="col-md-4">
		                  <button type="submit" class="btn btn-sm btn-default" id="procesar2"><span class="glyphicon glyphicon-cog"></span>Procesar</button>				
					</div>
					<div class="col-md-4">
		                  <button type="button" class="btn btn-sm btn-primary" id="limpiar"><span class="glyphicon glyphicon-trash"></span>Limpiar</button>					
					</div>
					<div class="col-md-4">
		                  <button type="button" class="btn btn-sm btn-danger" id="cancelar"><span class="glyphicon glyphicon-remove"></span>Cancelar</button>				
					</div>
				</div>        	
	        </center>


			</form>
		</section>
    </div>

     <!--  ...................................................... Detalle del Prestamo   ..................................................... -->
    <div role="tabpanel" class="tab-pane" id="detalledelprestamo">
    		<br>
    		<center>
		   			<div class="alert alert-info"><h4><b>Detalle del Prestamo</b></h4></div>	
		   						
			</center>

			<hr>
			<br>
			<table class="table">
				<thead>
					<tr>
						<th>
							Fecha
						</th>
						<th>
							Nº de cuota
						</th>
						<th>
							Pago de Cuota
						</th>
						<th>
							Capital Amortizado
						</th>
						<th>
							Intereses
						</th>
						<th>
							Saldo
						</th>
						<th>
							Condición
						</th>
						<th>
							Fecha Amortización
						</th>
					</tr>
				</thead>
			</table>

    </div>

  </div>

</div>

<div class="modal fade" id="modalmodificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Confirmación</b></h4>
      </div>
      <div class="modal-body">
        <center>¿Esta Seguro que desea modificar los datos?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-sm btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="tabladeamortizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tabla de Amortización</h4>
      </div>
      <div class="modal-body">
        <table class="table">
        	<thead>
        		<tr>
        			<th>Mes</th>
        			<th>Nº de Cuota</th>
        			<th>Pago Cuota</th>
        			<th>Capital Amortizado</th>
        			<th>Intereses</th>
        			<th>Saldo</th>
        		</tr>
        	</thead>
        	<tbody id="cuerpoTabla">
        		
        	</tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cerrar</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>



<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>
 <script type="text/javascript">
 $(document).ready(function() {
 	Buscar();
 	Buscar2();
 	var calculado=false;
 	var tmontocuotaquincenal=0;
 	var ttotalinteres=0;
/* 	$('#formsolicitudprestamo2').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {                
                fechasolicitud:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },                            
                      }
                },
                tipoprestamo:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },                            
                      }
                },
                prestamosolicitado:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              },

                      }
                }
            }              
          }).on('success.field.fv', function(e, data) {              
            	if (!data.fv.isValid()) {    
                     data.fv.disableSubmitButtons(true);        
                  }
            });	*/

 	$('#ttipoprestamo').change(function () {

 			/*console.log($('#ttipoprestamo').val());*/
 		$.ajax
	      ({
	      type: "POST",
	      url: "controlador/controladorPrestamo.php",
	      data: {id:5, tipo:ttipoprestamo.value},
	      dataType: "json",
	      async: false,           
	      success:
	      function (msg) 
	      {   
				tcodigodelprestamo.value=msg[0].codigo;
				ttazadeinteres.value=msg[0].tasa+" %";
				tNumerodecuentas.value=msg[0].plazo+ " meses";
				alertasPrestamo.innerHTML='';
	        },
	          error: function(jqXHR, textStatus, errorThrown) {
				  console.log("ERROR EN: " +textStatus, errorThrown);
				}
	        });

    });
 	$.ajax
      ({
      type: "POST",
      url: "controlador/controladorSocio.php",
      data: {id:7},
      dataType: "json",
      async: false,           
      success:
      function (msg) 
      {   
			/*console.log(" Tiempo aportando a la caja de ahorro en meses " + msg[0].m); */
			console.log("total: "+msg[0].total);
			console.log("cincuentaporciento: "+msg[0].cincuentaporciento+"  TOTAL DE HABERES SIN EDITAR: "+msg[0].TOTAL+"  Deuda: "+msg[0].deuda+"  Total para pedir prestamo: "+msg[0].totalparapedirprestamo+" Total Haberes: "+msg[0].totalhaberes + " cheque: "+ msg[0].cheque+" haberes reales:"+msg[0].total+ "tuplas retornadas: "+msg[0].m2)  ; 
      		if(msg[0].m>=12){
      			if(msg[0].total>msg[0].totalparapedirprestamo)
      				console.log(msg[0].total+": "+msg[0].totalparapedirprestamo);
      			

  			if(msg[0].totalparapedirprestamo==msg[0].total||msg[0].total>msg[0].totalparapedirprestamo){

				resultado.innerHTML='';
      			$('#formulario').fadeIn();
      			tnombre.value=msg[0].nombres;
      			tapellido.value=msg[0].apellidos;
      			ttotalhaberes.value=msg[0].total;
      			thaberesdisponibles.value=Math.round((msg[0].total*0.8)*100)/100;
      			tfecha.value=msg[0].fechadeafiliacion
  			}
  			else if(msg[0].totalparapedirprestamo>msg[0].total){
  				resultado.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡No puede pedir prestamo ya que no ha cancelado el 50% del prestamo anterior</div>';
  				$('#formulario').fadeOut();
  			}
      									} 	
			else{
      			$('#formulario').fadeOut();

				resultado.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡No se ha encontrado o no tiene 1 año como minimo en la aportando en la caja de ahorros!</div>';

			}

        },
      error: function(jqXHR, textStatus, errorThrown) {
		  console.log(textStatus, errorThrown);
		}
    });
 	$('#ttipoprestamo').change(function () {
 		calculado=false
 			/*console.log($('#ttipoprestamo').val());*/
 			if($('#ttipoprestamo').val()==1||$('#ttipoprestamo').val()==2){
 					console.log($('#ttipoprestamo').val());
 					$('#tcuotas').fadeIn();
 					$('#tNumerodecuentas').fadeOut();
 			}
 			else{
 					$('#tcuotas').fadeOut();
 					$('#tNumerodecuentas').fadeIn();
 			}
	 		$.ajax
		      ({
		      type: "POST",
		      url: "controlador/controladorPrestamo.php",
		      data: {id:5, tipo:ttipoprestamo.value},
		      dataType: "json",
		      async: false,           
		      success:
		      function (msg) 
		      {   
					tcodigodelprestamo.value=msg[0].codigo;
					ttazadeinteres.value=msg[0].tasa+" %";
					tNumerodecuentas.value=msg[0].plazo+ " meses";
					alertasPrestamo.innerHTML='';
					ttotalprestamocheque.value="";
					$('#tmontocuotaquincenal').val("");
					$('#ttotalinteres').val("");
		        },
		          error: function(jqXHR, textStatus, errorThrown) {
					  console.log("ERROR EN: " +textStatus, errorThrown);
					}
		        });

    });

     $('#calcular').on('click',  function(){
    	/*console.log(validarFormularioPrestamo()+ " : "+ !$('#ttipoprestamo').val());*/
    		if(validarFormularioPrestamo()){
    			alertasPrestamo.innerHTML='';
    			/*console.log("entro");*/
    			$('#tabladeamortizacion').modal('show');
    			calcularAmortizacion();

    			$.ajax
			      ({
			      type: "POST",
			      url: "controlador/controladorSocio.php",
			      data: {id:8},
			      dataType: "json",
			      async: false,           
			      success:
			      function (msg) 
			      {   
						/* console.log("Salida:" +msg);	*/
						 console.log("m: "+msg[0].m+"  Deuda: "+msg[0].deuda+"   total para pedir prestamo: "+msg[0].totalparapedirprestamo+" Total Haberes: "+msg[0].totalhaberes +" Total2: " +msg[0].TOTAL + " cheque: "+ msg[0].cheque)   
						
						 if(msg[0].deuda==0){
						 	ttotalprestamocheque.value=tprestamosolicitado.value;
						 	
						 }		
						 else if(msg[0].totalparapedirprestamo<msg[0].total){
						 	ttotalprestamocheque.value=tprestamosolicitado.value;
						 	return;
						 }	      		
						 else if((tprestamosolicitado.value-msg[0].deuda)>0){
						 	ttotalprestamocheque.value=tprestamosolicitado.value-msg[0].deuda;

						 }
						/* else if((tprestamosolicitado.value-msg[0].deuda)<0){
						 	resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡No Puede realizar prestamo ya que la deuda supera al monto solicitado!</div>';

						 }*/
			        },
			          error: function(jqXHR, textStatus, errorThrown) {
						  console.log("ERROR EN: " +textStatus, errorThrown);
						}
			        });
    		}  		
    	
    });
    $('#limpiar').on('click',  function(){
    			limpiar();
    });

    
    function verificarsitienedeuda(){
    	var tienedeuda=false;
    	$.ajax
			      ({
			      type: "POST",
			      url: "controlador/controladorRetiros.php",
			      data: {id:5 },
			      dataType: "json",
			      async: false,           
			      success:
			      function (msg) 
			      {   
			      	 var deuda=Math.round(msg[0].deuda*100)/100;
			      	 var haberes=Math.round(msg[0].haberes*100)/100;
					 console.log("deuda: "+ deuda+ " total haberes: "+ haberes );
					 if(msg[0].m>0){
					 	if(deuda>=haberes){
							tienedeuda=true;
						}	
						else{
							tienedeuda=false;
						}
					 }else{
					 	tienedeuda=false;
					 }
						
					 
			        },
			          error: function(jqXHR, textStatus, errorThrown) {
						  console.log("ERROR EN: " +textStatus, errorThrown);
						}
			        });
		console.log("tiene deuda? "+ tienedeuda);
		return tienedeuda;
    }
    function calculartiempodelsocio(){
     	var tiempo=new array();
     	tiempo[0]=0;
     	tiempo[1]=0;
     	$.ajax
	      ({
	      type: "POST",
	      url: "controlador/controladorSocio.php",
	      data: {id:9 },
	      dataType: "json",
	      async: false,           
	      success:
	      function (msg) 
	      {   
	         console.log("Tiempo aportando a la caja de ahorros: "+msg[0].meses+ " hizo retiro total: "+ msg[0].hizoretiro);
			 tiempo[0]=msg[0].meses;
			 tiempo[1]=msg[0].hizoretiro;
	        },
	          error: function(jqXHR, textStatus, errorThrown) {
				  console.log("ERROR EN: " +textStatus, errorThrown);
				}
	        });

	      console.log("Tiempo en la caja de ahorro "+tiempo);
	     return tiempo;
     }
 	function alerta(mensaje, titulo, tipoalert){
 		var notify = $.notify({
			icon: "glyphicon glyphicon-warning-sign",
			title: titulo,
			message: mensaje,
			url: "",
			target: "_blank"
		},{
			type: tipoalert,
			allow_dismiss: true,
			newest_on_top: false,
			placement: {
				from: "bottom",
				align: "left"
			},
			offset: {
				x: 20,
				y: 20
			},
			spacing: 10,
			z_index: 1031,
			delay: 5000,
			mouse_over: (true)
		});
 	}
 $('#procesar').on('click',  function(){
    		console.log("esta calculado?: " +calculado);
    		if($('#ttipoprestamo').val()==1||$('#ttipoprestamo').val()==1){
    			if(calculado){
    				$('#tcuotas').fadeOut();
 					$('#tNumerodecuentas').fadeIn();
 					console.log("valor del tipo de prestamo seleccionado: "+$('#ttipoprestamo').val());
					if($('#ttipoprestamo').val()==3||$('#ttipoprestamo').val()==6||$('#ttipoprestamo').val()==7)
 						{
 							if(calculartiempodelsocio()[1]==1){

								if(calculartiempodelsocio()[0]>=72){
		  							$.ajax
								      ({
								      type: "POST",
								      url: "controlador/controladorSolicitudPrestamo.php",
								      data: {id:1, totalhaberes:(ttotalhaberes.value-ttotalprestamocheque.value), tipo:ttipoprestamo.value, cuotas:$('#tcuotas').val(), total:ttotalprestamocheque.value, totalinteres:$('#ttotalinteres').val(), montocuotaquincenal:$('#tmontocuotaquincenal').val()},
								      dataType: "json",
								      async: false,           
								      success:
								      function (msg) 
								      {   
										 console.log(msg);	
										 if(msg=="true"){
										 	alerta("Se ha generado la solicitud de prestamo correctamente", "Alerta", "success");
										 	limpiar();
							      			$('#formulario').fadeOut();


										 }
								        },
								          error: function(jqXHR, textStatus, errorThrown) {
											  console.log("ERROR EN: " +textStatus, errorThrown);
											}
								        });
		 						}
	 							else{
								resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>!El Socio no tiene 5 años aun aportando a la caja de ahorros para solicitar este prestamo!</div>';
	 								
	 							}
 							}else{
 								if(calculartiempodelsocio()[0]>=72){
		  							$.ajax
								      ({
								      type: "POST",
								      url: "controlador/controladorSolicitudPrestamo.php",
								      data: {id:1, totalhaberes:(ttotalhaberes.value-ttotalprestamocheque.value), tipo:ttipoprestamo.value, cuotas:$('#tcuotas').val(), total:ttotalprestamocheque.value, totalinteres:$('#ttotalinteres').val(), montocuotaquincenal:$('#tmontocuotaquincenal').val()},
								      dataType: "json",
								      async: false,           
								      success:
								      function (msg) 
								      {   
										 console.log(msg);	
										 if(msg=="true"){
										 	alerta("Se ha generado la solicitud de prestamo correctamente", "Alerta", "success");
										 	limpiar();
							      			$('#formulario').fadeOut();


										 }
								        },
								          error: function(jqXHR, textStatus, errorThrown) {
											  console.log("ERROR EN: " +textStatus, errorThrown);
											}
								        });
		 						}
	 							else{
								resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>!El Socio no tiene 5 años aun aportando a la caja de ahorros para solicitar este prestamo!</div>';
	 								
	 							}
 							}

				      	}else{
				      		$.ajax
						      ({
						      type: "POST",
						      url: "controlador/controladorSolicitudPrestamo.php",
						      data: {id:1, totalhaberes:(ttotalhaberes.value-ttotalprestamocheque.value), tipo:ttipoprestamo.value, cuotas:$('#tcuotas').val(), total:ttotalprestamocheque.value, totalinteres:$('#ttotalinteres').val(), montocuotaquincenal:$('#tmontocuotaquincenal').val()},
						      dataType: "json",
						      async: false,           
						      success:
						      function (msg) 
						      {   
								 console.log(msg);	
								 if(msg=="true"){
								 	alerta("Se ha generado la solicitud de prestamo correctamente", "Alerta", "success");
								 	limpiar();
					      			$('#formulario').fadeOut();


								 }
						        },
						          error: function(jqXHR, textStatus, errorThrown) {
									  console.log("ERROR EN: " +textStatus, errorThrown);
									}
						        });
				      	}
				}
    			else{
					resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Se debe calcular la tabla de amortizacion antes de procesar la solicitud!</div>';

    			}
    		}
    		else{
    			if(calculado){

    					if($('#ttipoprestamo').val()==3||$('#ttipoprestamo').val()==6||$('#ttipoprestamo').val()==7)
 						{

						if(calculartiempodelsocio()>=60){

  							$.ajax
						      ({
						      type: "POST",
						      url: "controlador/controladorSolicitudPrestamo.php",
						      data: {id:1, totalhaberes:(ttotalhaberes.value-ttotalprestamocheque.value), tipo:ttipoprestamo.value, cuotas:$('#tcuotas').val(), total:ttotalprestamocheque.value, totalinteres:$('#ttotalinteres').val(), montocuotaquincenal:$('#tmontocuotaquincenal').val()},
						      dataType: "json",
						      async: false,           
						      success:
						      function (msg) 
						      {   
								 console.log(msg);	
								 if(msg=="true"){
								 	alerta("Se ha generado la solicitud de prestamo correctamente", "Alerta", "success");
								 	limpiar();
					      			$('#formulario').fadeOut();


								 }
						        },
						          error: function(jqXHR, textStatus, errorThrown) {
									  console.log("ERROR EN: " +textStatus, errorThrown);
									}
						        });
 							}else{
							resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>!El Socio no tiene 5 años aun aportando a la caja de ahorros para solicitar este prestamo!</div>';
 								
 							}

				      	}else{
				      		$.ajax
						      ({
						      type: "POST",
						      url: "controlador/controladorSolicitudPrestamo.php",
						      data: {id:1, totalhaberes:(ttotalhaberes.value-ttotalprestamocheque.value), tipo:ttipoprestamo.value, cuotas:$('#tcuotas').val(), total:ttotalprestamocheque.value, totalinteres:$('#ttotalinteres').val(), montocuotaquincenal:$('#tmontocuotaquincenal').val()},
						      dataType: "json",
						      async: false,           
						      success:
						      function (msg) 
						      {   
								 console.log(msg);	
								 if(msg=="true"){
								 	alerta("Se ha generado la solicitud de prestamo correctamente", "Alerta", "success");
								 	limpiar();
					      			$('#formulario').fadeOut();


								 }
						        },
						          error: function(jqXHR, textStatus, errorThrown) {
									  console.log("ERROR EN: " +textStatus, errorThrown);
									}
						        });
				      	}
  							
				}
    			else{
					resultado3.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Se debe calcular la tabla de amortizacion antes de procesar la solicitud!</div>';

    			}
    		}
    });


 function limpiar(){
		tcodigodelprestamo.value="";
		ttazadeinteres.value="";
		tNumerodecuentas.value="";
		alertasPrestamo.innerHTML='';
		tprestamosolicitado.value='';
		$('#ttipoprestamo').val(0);
		ttotalprestamocheque.value="";
		$('#tmontocuotaquincenal').val("");
		$('#ttotalinteres').val("");
		calculado=false;
		$('.form-group').removeClass('has-success');
		$('.form-group').removeClass('has-error');
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('small.help-block').hide();
		$('.form-group').find('i.form-control input-sm-feedback').hide();
		$('.form-group').find('i.form-control-feedback').hide();
    }
    $('#cancelar').on('click',  function(){
    	limpiar();
    	$('#formulario').fadeOut();

    });
 	function Buscar(){	
 	console.log("entro");						 			
			$.ajax
		      ({
		      type: "POST",
		      url: "controlador/controladorSocio.php",
		      data: {id:7},
		      dataType: "json",
		      async: false,           
		      success:
		      function (msg) 
		      {   
					console.log(msg);    
		      		if(msg[0].m>0){
		      			$('#formulario').fadeIn();
		      			tnombre.value=msg[0].nombres;
		      			tapellido.value=msg[0].apellidos;
		      			ttotalhaberes.value=msg[0].total;
		      			thaberesdisponibles.value=Math.round((msg[0].total*0.8)*100)/100;
		      			tfecha.value=msg[0].fechadeafiliacion
		      									} 	
					else{
		      			$('#formulario').fadeOut();

						resultado.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡No se ha encontrado o no posee ningun aporte a la caja de ahorro!</div>';

					}

		        },
	          error: function(jqXHR, textStatus, errorThrown) {
				  console.log(textStatus, errorThrown);
				}
	        });		
	}

	function Buscar2(){
		 	$('#buscar2').on('click',  function(){
		 		console.log("click en buscar");
	 			if(cedulabusqueda.value==""){
							resultado.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Escriba una cedula o el codigo del socio!</div>';

					}
					else{
							 			
								$.ajax
							      ({
							      type: "POST",
							      url: "controlador/controladorSocio.php",
							      data: {id:6, cedula:cedulabusqueda.value},
							      dataType: "json",
							      async: false,           
							      success:
							      function (msg) 
							      {   
										console.log(msg);    
							      		if(msg[0].m>0){
											resultado2.innerHTML='';
							      			$('#formulario2').fadeIn();
							      			tnombre2.value=msg[0].nombres;
							      			tapellido2.value=msg[0].apellidos;
							      			ttotalhaberes2.value=msg[0].total;
							      			thaberesdisponibles2.value=Math.round((msg[0].total*0.8)*100)/100;
							      			tfecha2.value=msg[0].fechadeafiliacion
							      									} 	
										else{
							      			$('#formulario2').fadeOut();

											resultado2.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡No se ha encontrado!</div>';

										}

							        },
						          error: function(jqXHR, textStatus, errorThrown) {
									  console.log(textStatus, errorThrown);
									}
						        });
 					}
		 		
			});
	}
		 function calcularAmortizacion(){
		 		$('#cuerpoTabla').html("");
        		calculado=true;
        		ttotalinteres=0;
        		ttotalprestamocheque.value=tprestamosolicitado.value;
/* ------------ Calculo de pago -----------------------------------------------------------------*/
               	var pago=0;
               	var i=(parseFloat(ttazadeinteres.value)/100)/12;
               	var n=parseInt(tNumerodecuentas.value);
               	var c=parseFloat(tprestamosolicitado.value);
               	var n2=n*-1;               
                pago= c*(  			(i) / ( 1-   (1/Math.pow(n,   (1+i))) )          );
                var suma=1+i;
                var multiplicar=suma
                for (var x = 0; x <n-1; x++) {
                	multiplicar=multiplicar*suma;
                	/*console.log("Resultado de la multiplicacion: "+multiplicar);*/

    	         }
    	         var potencia=1/multiplicar;
    	         var numerador=i;
    	         var denominador=1-potencia;
    	         var division=numerador/denominador;
    	         var mult2=c*division;
    	         pago=Math.round(mult2*100)/100;
    	         var inte=i;         
    	         $('#tmontocuotaquincenal').val(Math.round((pago/2)*100)/100);
/* ------------ Calculo de pago -----------------------------------------------------------------*/
                for(var i=0; i<=n; i++){ 
                		if(i==0){
								var row3=$('<tr ></tr>');
								var fila0=$('<td></td>').text(i);
								var fila1 = $('<td></td>').text(" ");
								var fila3 = $('<td></td>').text(" ");
								var fila4 = $('<td></td>').text(" ");
								var fila5 = $('<td></td>').text(" ");
								var fila6 = $('<td></td>').text(c);
								row3.append(fila0);
								row3.append(fila1);
								row3.append(fila3);
								row3.append(fila4);
								row3.append(fila5);
								row3.append(fila6);
								$('#cuerpoTabla').append(row3);
                		}
                		else{
                			/******* calculando interes *****/	
                			var interes=c*inte;
                			var amortizacion=pago-interes;
                			c=c-amortizacion;
                			ttotalinteres=ttotalinteres+interes;
                			/******* calculando interes *****/
                			    var row3=$('<tr ></tr>');
		                        var fila0=$('<td></td>').text(i);
		                        var fila1 = $('<td></td>').text(i);
		                        var fila3 = $('<td></td>').text(pago);
		                        var fila4 = $('<td></td>').text(Math.round(amortizacion*100)/100);
		                        var fila5 = $('<td></td>').text(Math.round(interes*100)/100);
		                        var fila6="";
		                        if(i==n)
		                          fila6 = $('<td></td>').text(0);  
		                        else
		                          fila6 = $('<td></td>').text(Math.round(c*100)/100);                  
								row3.append(fila0);
								row3.append(fila1);
								row3.append(fila3);
								row3.append(fila4);
								row3.append(fila5);
								row3.append(fila6);
								$('#cuerpoTabla').append(row3);
                		}                      
                } 
                $('#ttotalinteres').val(Math.round(ttotalinteres*100)/100);   
                console.log("ttotalinteres: "+$('#ttotalinteres').val()+  "  "+ttotalinteres);            		
    	} 
	 function validarFormularioPrestamo(){
	 	var b=true;
	 		if(!$('#ttipoprestamo').val()==true){
				alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Debe seleccionar un tipo de prestamo!</div>';
       			return false;
	 		}
	 		else if(tprestamosolicitado.value==""){
					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Debe escribir una cantidad en prestamo solicitado!</div>';
       				return false;	            		

	 		}
            else if($('#ttipoprestamo').val()=="1"){
	            	if(parseFloat(tprestamosolicitado.value)>parseFloat(thaberesdisponibles.value))
	            	{
	    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo '+thaberesdisponibles.value+' bs!</div>';
	               			return false;	            		
	            	}
            }
            else if(parseFloat(tprestamosolicitado.value)<=0){
            	alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser mayor de 0 bs!</div>';
	               			return false;
            }
            else if($('#ttipoprestamo').val()=="2"){
	            	if(parseFloat(tprestamosolicitado.value)>parseFloat(thaberesdisponibles.value)){
	    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo '+thaberesdisponibles.value+' bs!</div>';
	               			return false;	            		
	            	}

            }
            else  if($('#ttipoprestamo').val()=="3"){
	            	if(parseFloat(tprestamosolicitado.value)>parseFloat(150000)){
	    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo 150000bs!</div>';
	               			return false;	            		
	            	}
            }
             else if($('#ttipoprestamo').val()=="4"){
            	if(parseFloat(tprestamosolicitado.value)>parseFloat(thaberesdisponibles.value)){
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo '+thaberesdisponibles.value+' bs!</div>';
               			return false;            		
            	}
            }
              else if($('#ttipoprestamo').val()=="5"){
            	if(parseFloat(tprestamosolicitado.value)>parseFloat(thaberesdisponibles.value)){
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo '+thaberesdisponibles.value+' bs!</div>';
               			return false;
                   }
            }
             else if($('#ttipoprestamo').val()=="6"){
            	if(parseFloat(tprestamosolicitado.value)>parseFloat(100000)){
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo 100000 bs!</div>';
               			return false;            		
            	}
            }
              else if($('#ttipoprestamo').val()=="7"){
            	if(parseFloat(tprestamosolicitado.value)>parseFloat(150000)){
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El Prestamo Solicitado Debe ser maximo 150000 bs!</div>';
               			return false;            		
            	}
            }
            return true;
	 }
	});
</script>