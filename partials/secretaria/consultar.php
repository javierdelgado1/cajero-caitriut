 <center>
  <br>
  	<div id="alertas"> <div class="alert alert-info"><h4><b>Tabla de Amortización  de préstamos</b></h4></div></div>
		<hr>
  </center>   
  		<section id="imprimirseccion">
		<form id="formsolicitudprestamo2" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 
		<center><section id="alertasPrestamo"></section></center>
			<div class="row">
				<div class="col-md-5">
						<div class="form-group">
					          <label class="col-sm-7 control-label">Fecha de Solicitud:</label>
					          <div class="col-sm-5">
					              <input  id="tfechasolicitud" type="text" class="form-control input-sm" name="fechasolicitud" value=<?php echo date("d-m-Y");  ?> disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Tipo de Préstamo:</label>
					          <div class="col-sm-5">
					               <select  class="form-control input-sm"   name="tipoprestamo"  id="ttipoprestamo" >
										<option value="0"disabled selected> --Seleccione una opcion -- </option>
					               		<option value="1">Préstamos Personales</option>
					               		<option value="2">Préstamos Comerciales</option>
					               		<option value="3">Préstamos para la adquisicion de Vehiculos</option>	
					               		<option value="4">Préstamos Especiales o con Fianza</option>            		
					               		<option value="5">Préstamos Especiales sin Fiador</option>
					               		<option value="6">Préstamos con Garantía Hipotecaria (Para adquisicion de vivienda)</option>
					               		<option value="7">Préstamos con Garantía Hipotecaria (Para remodelacion de vivienda)</option> 
					               </select>
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Préstamo Solicitado:</label>
					          <div class="col-sm-5">
					              <input  id="tprestamosolicitado" type="text" class="form-control input-sm" name="prestamosolicitado"  />  
					          </div>
					      </div>
				</div>
				<div class="col-md-7">
						<div class="form-group">
					          <label class="col-sm-7 control-label">Código del préstamo:</label>
					          <div class="col-sm-5">
					              <input  id="tcodigodelprestamo" type="text" class="form-control input-sm" name="codigodelprestamo" disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label">Tasa de intéres:</label>
					          <div class="col-sm-5">
					              <input  id="ttazadeinteres" type="text" class="form-control input-sm" name="tazadeinteres" disabled="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-7 control-label"># de cuotas (plazo)</label>
					          <div class="col-sm-5">
					              <input  id="tNumerodecuentas" type="text" class="form-control input-sm" name="Numerodecuentas" disabled="true"/>  
					          </div>
					      </div>
				</div>
			</div>
				<hr>
					  <div class="form-group">
				          <label class="col-sm-9 control-label"></label>
				          <div class="col-sm-3">
				               <button type="submit" class="btn btn-sm btn-default" id="calcular" >Calcular tabla de amortización</button>		  
				          </div>
				      </div>
				<hr>
			</form>
<hr>

<br><br>

<!-- <table class="table">
	<thead>
		<tr>
			<th>
				Fecha
			</th>
			<th>
				N de cuota
			</th>
			<th>
				Pago. Cuota
			</th>
			<th>
				Capital Amortizado
			</th>
			<th>
				Intereses
			</th>
			<th>
				Saldo
			</th>
		</tr>
	</thead>
</table> -->

<div id="resultados" style="display:none;">	
		<table class="table">
		        	<thead>
		        		<tr>
		        			<th>Mes</th>
		        			<th>Nº de Cuota</th>
		        			<th>Pago Cuota</th>
		        			<th>Capital Amortizado</th>
		        			<th>Intereses</th>
		        			<th>Saldo</th>
		        		</tr>
		        	</thead>
		        	<tbody id="cuerpoTabla">
		        		
		        	</tbody>
		        </table>
		<br><br>
		<hr>
		  <div class="row">
				<div class="col-md-4">
					<div class="form-group">
					          <label class="col-sm-7 control-label">Monto Cuota Quincenal:</label>
					          <div class="col-sm-5">
					              <input  id="tmontocuotaquincenal" type="text" class="form-control  input-sm" name="nombre" >  
					          </div>
					      </div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					          <label class="col-sm-7 control-label">Total interés</label>
					          <div class="col-sm-5">
					              <input  id="ttotalinteres" type="text" class="form-control input-sm" name="nombre" >  
					          </div>
					      </div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
				               <button type="submit" class="btn btn-sm btn-default" id="imprimir"  style="display:none;"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>Imprimir</button>		  
					         	
					      </div>
				</div>
		</div>
		</section>
		<hr>
<!-- 		<center>
		     <div class="row">
				<div class="col-md-4">
		              <button type="submit" class="btn btn-sm btn-default" id="afiliar"><span class="glyphicon glyphicon-cog"></span>Procesar</button>				
				</div>
				<div class="col-md-4">
		              <button type="button" class="btn btn-sm btn-primary" id="limpiar"><span class="glyphicon glyphicon-trash"></span>Limpiar</button>					
				</div>
				<div class="col-md-4">
		              <button type="button" class="btn btn-sm btn-danger" id="cancelar"><span class="glyphicon glyphicon-remove"></span>Cancelar</button>				
				</div>
			</div>        	
		</center> -->
</div>

<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>
<script type="text/javascript">
	 $(document).ready(function() {
			var tmontocuotaquincenal=0;
			var ttotalinteres=0;
	 		$('#ttipoprestamo').change(function () {

			 			/*console.log($('#ttipoprestamo').val());*/
			 		$.ajax
				      ({
				      type: "POST",
				      url: "controlador/controladorPrestamo.php",
				      data: {id:5, tipo:ttipoprestamo.value},
				      dataType: "json",
				      async: false,           
				      success:
				      function (msg) 
				      {   
							tcodigodelprestamo.value=msg[0].codigo;
							ttazadeinteres.value=msg[0].tasa+" %";
							tNumerodecuentas.value=msg[0].plazo+ " meses";
							
				        },
				          error: function(jqXHR, textStatus, errorThrown) {
							  console.log("ERROR EN: " +textStatus, errorThrown);
							}
				        });

			    });
	 		$('#imprimir').on('click',  function(){
	 				$('#imprimir').fadeOut();
	 				$.ajax
				      ({
				      type: "POST",
				      url: "controlador/controladorReportes.php",
				      data: {id:1, datahtml:$('#imprimirseccion').html()},
				      /*dataType: "json",*/
				      async: false,           
				      success:
				      function (msg) 
				      {   
				      		console.log(msg);
							window.open('partials/reportes/tabladeamortizacion.php');
							
				        },
				          error: function(jqXHR, textStatus, errorThrown) {
							  console.log("ERROR EN: " +textStatus, errorThrown);
							}
				        });
	 		});

	 	    $('#calcular').on('click',  function(){
			    	console.log(!$('#ttipoprestamo').val());
			    		if(!$('#ttipoprestamo').val()==false){
			    			alertasPrestamo.innerHTML='';
			    			if(tprestamosolicitado.value!=""){
			    				alertasPrestamo.innerHTML='';

				    			$('#tabladeamortizacion').modal('show');
				    			calcularAmortizacion();
				    			$('#resultados').fadeIn();
			    				
			    			}
			    			else{
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Debe escribir el monto del prestamo a solicitar!</div>';

			    			}

			    		}
			    	else{
    					alertasPrestamo.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Debe seleccionar el tipo de prestamo!</div>';

			    	}
			    		 		
			    	
			    });
	 	    $('#limpiar').on('click',  function(){
    			limpiar();
		    			
		    });
		    $('#cancelar').on('click',  function(){
		    	limpiar();
		    	$('#resultados').fadeOut();
		    });
 function calcularAmortizacion(){
		 		$('#cuerpoTabla').html("");
        		calculado=true;
        		ttotalinteres=0;
/* ------------ Calculo de pago -----------------------------------------------------------------*/
               	var pago=0;
               	var i=(parseFloat(ttazadeinteres.value)/100)/12;
               	var n=parseInt(tNumerodecuentas.value);
               	var c=parseFloat(tprestamosolicitado.value);
               	var n2=n*-1;               
                pago= c*(  			(i) / ( 1-   (1/Math.pow(n,   (1+i))) )          );
                var suma=1+i;
                var multiplicar=suma
                for (var x = 0; x <n-1; x++) {
                	multiplicar=multiplicar*suma;
                	/*console.log("Resultado de la multiplicacion: "+multiplicar);*/

    	         }
    	         var potencia=1/multiplicar;
    	         var numerador=i;
    	         var denominador=1-potencia;
    	         var division=numerador/denominador;
    	         var mult2=c*division;
    	         pago=Math.round(mult2*100)/100;
    	         var inte=i;         
    	         $('#tmontocuotaquincenal').val(Math.round((pago/2)*100)/100);
/* ------------ Calculo de pago -----------------------------------------------------------------*/
                for(var i=0; i<=n; i++){ 
                		if(i==0){
								var row3=$('<tr ></tr>');
								var fila0=$('<td></td>').text(i);
								var fila1 = $('<td></td>').text(" ");
								var fila3 = $('<td></td>').text(" ");
								var fila4 = $('<td></td>').text(" ");
								var fila5 = $('<td></td>').text(" ");
								var fila6 = $('<td></td>').text(c);
								row3.append(fila0);
								row3.append(fila1);
								row3.append(fila3);
								row3.append(fila4);
								row3.append(fila5);
								row3.append(fila6);
								$('#cuerpoTabla').append(row3);
                		}
                		else{
                			/******* calculando interes *****/	
                			var interes=c*inte;
                			var amortizacion=pago-interes;
                			c=c-amortizacion;
                			ttotalinteres=ttotalinteres+interes;
                			/******* calculando interes *****/
                			    var row3=$('<tr ></tr>');
		                        var fila0=$('<td></td>').text(i);
		                        var fila1 = $('<td></td>').text(i);
		                        var fila3 = $('<td></td>').text(pago);
		                        var fila4 = $('<td></td>').text(Math.round(amortizacion*100)/100);
		                        var fila5 = $('<td></td>').text(Math.round(interes*100)/100);
		                        var fila6="";
		                        if(i==n)
		                          fila6 = $('<td></td>').text(0);  
		                        else
		                          fila6 = $('<td></td>').text(Math.round(c*100)/100);                  
								row3.append(fila0);
								row3.append(fila1);
								row3.append(fila3);
								row3.append(fila4);
								row3.append(fila5);
								row3.append(fila6);
								$('#cuerpoTabla').append(row3);
                		}                      
                } 
                $('#ttotalinteres').val(Math.round(ttotalinteres*100)/100);   
                console.log("ttotalinteres: "+$('#ttotalinteres').val()+  "  "+ttotalinteres);            		
    	} 

    	 function limpiar(){
    			tcodigodelprestamo.value="";
				ttazadeinteres.value="";
				tNumerodecuentas.value="";			
				tprestamosolicitado.value='';
				$('#ttipoprestamo').val(0);
				ttotalinteres.value="";
				tmontocuotaquincenal.value="";
    			cuerpoTabla.innerHTML="";

    }


	});
 </script>