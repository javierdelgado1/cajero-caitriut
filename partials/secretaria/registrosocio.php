
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#agregar" aria-controls="agregar" role="tab" data-toggle="tab">Agregar Socio</a></li>
    <li role="presentation"><a href="#buscarsocio" aria-controls="buscarsocio" role="tab" data-toggle="tab">Buscar Socio</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="agregar">
    <br>
			<form id="formregistrosocio" method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
			      <center>
			      	<div id="alertas2"> <div class="alert alert-info"><h4><b>Identificación del socio</b></h4></div></div>
			      </center>   

			       <div class="row">
						<div class="col-md-6">
					       <div class="form-group">
					          <label class="col-sm-5 control-label">Cedula de Identidad:</label>
					          <div class="col-sm-7">
					                <input id="tc" type="text" class="form-control input-sm"  name="ci"  >
					          </div>
					      	</div> 
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Nombres:</label>
					          <div class="col-sm-7">
					              <input  id="tnombre" type="text" class="form-control input-sm" name="nombre"  >  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Correo:</label>
					          <div class="col-sm-7">
					              <input  id="temail" type="text" class="form-control input-sm" name="email"  >  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Lugar de Nacimiento:</label>
					          <div class="col-sm-7">
					              <input  id="tldn" type="text" class="form-control input-sm" name="ldn"  >  
					          </div>
					      </div>    
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Dirección:</label>
					          <div class="col-sm-7">
					           		<textarea id="td"rows="3" cols="50" class="form-control input-sm" name="direccion"></textarea>
					             <!--  <input   type="text"  >   -->
					          </div>
					      </div> 
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Teléfono Celular:</label>
					          <div class="col-sm-7">
					              <input  id="tcelular" type="text" class="form-control input-sm" name="celular"  >  
					          </div>
					      </div>          
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Departamento:</label>
					          <div class="col-sm-7">
			<!-- 		              <input   type="text" class="form-control input-sm"  >  -->
					               <select  class="form-control input-sm"  name="departamento"  id="tdepartamento"  >
										
					               </select> 
					          </div>
					      </div> 
					     <!--  <div class="form-group">
					          <label class="col-sm-5 control-label">Estatus:</label>
					          <div class="col-sm-7">
					              <select  class="form-control input-sm"  name="estado"  id="testado"  >
					               		<option value="0" disabled selected> --Seleccione una opcion -- </option>
					               		<option value="Estado1">Estado 1</option>
					               		<option value="Estado2">Estado 2</option>
					               		<option value="Estado3">Estado 3</option>
					               		<option value="Estado4">Estado 4</option>
					               		<option value="Estado5">Estado 5</option>
					               </select> 
					          </div>
					      </div> -->   
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Cantidad  de Familiares:</label>
					          <div class="col-sm-7">
					              <select class="form-control input-sm"  name="cantidadfamiliares"  id="tcfamiliar"  >
					               		<option value="-1" disabled selected> --Seleccione una opcion -- </option>
					               		<option value="0">0</option>
					               		<option value="1">1</option>
					               		<option value="2">2</option>
					               		<option value="3">3</option>
					               		<option value="4">4</option>
					               		<option value="5">5</option>
					               </select>  
					          </div>
					      </div>				
						 <div class="form-group">
									<label class="col-sm-5 control-label">Salario </label>
						          <div class="col-sm-7">
										<input  id="tsalario" type="text" class="form-control input-sm" name="salario" />  
						          </div>
					          </div>
						</div>
						<div class="col-md-6">
							 <div class="form-group">
									<label class="col-sm-5 control-label">Codigo Socio:</label>
						          <div class="col-sm-7">
						              <input  id="tcodigosocio" type="text" class="form-control input-sm" name="codigosocio" value="1" disabled>  
						          </div>
					          </div>
			      			  <div class="form-group">
									<label class="col-sm-5 control-label">Apellidos:</label>
						          <div class="col-sm-7">
						              <input  id="tapellido" type="text" class="form-control input-sm" name="apellido"  >  
						          </div>
					          </div>
			      			  <div class="form-group">
									<label class="col-sm-5 control-label">Fecha de Nacimiento:</label>
						          <div class="col-sm-7">
						              <input  id="tfechadenacimiento" type="date" class="form-control input-sm date-pick" name="fechadenacimiento" >  
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Estado Civil:</label>
						          <div class="col-sm-7">
						              <select  class="form-control input-sm"  name="estadocivil"  id="testadocivil"  >
						              	<option value="0" disabled selected> --Seleccione una opcion -- </option>
					               		<option value="Soltero">Soltero</option>
					               		<option value="Casado">Casado</option>		
					               		<option value="Viudo">Viudo</option>		               		
					               		<option value="Divorciado">Divorciado</option>		               		

					               	  </select>   
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Teléfono Hab.:</label>
						          <div class="col-sm-7">
						              <input  id="ttelefonohab" type="text" class="form-control input-sm" name="telefonohabitacion" >  
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Gremio/Cargo:</label>
						              <!-- <input  id="tgremiocargo" type="text" class="form-control input-sm" name="gremiocargo" value="Na"> -->
					               		<div class="col-sm-7">
								              <select  class="form-control input-sm" name="gremiocargo" id="tgremiocargo"  >
								              		
								            </select>     
					          </div>
					          </div>

					          <div class="form-group">
									<label class="col-sm-5 control-label">Condición:</label>
						          <div class="col-sm-7">
						              <select  class="form-control input-sm"  name="condicion"  id="tcondicion"  >
						              		<option value="0" disabled selected> --Seleccione una opcion -- </option>
						               		<option value="Contratado">Contratado</option>
						               		<option value="Ordinario">Ordinario</option>
						               		<option value="Fijo">Fijo</option>
						               		<option value="Jubilado">Jubilado</option>	
						               		<option value="Incapacitado">Incapacitado</option>		               		



					               	  </select>   
						          </div>
					          </div>
					           <div class="form-group">
									<label class="col-sm-5 control-label">Fecha de Ingreso:</label>
						          <div class="col-sm-7">
						              <input  id="tfechadeingreso" type="date" class="form-control input-sm date-pick" name="fechadeingreso">  
						          </div>
					          </div>
					           <div class="form-group">
									<label class="col-sm-5 control-label">Fecha de afiliación:</label>
						          <div class="col-sm-7">
			<input  id="tfechadeafiliacion" type="date" class="form-control input-sm" name="fechadeafiliacion" value=<?php echo date('Y-m-d'); ?> disabled>  
						          </div>
					          </div>
					          

						</div>
					</div>
					<section id="listafamilia" style="display:none;">

						<br>
						<hr>
						<section id="alerta2"></section>
						<center>
							<table class="table">
								<thead>
									<tr>
										<th>Nro</th>
										<th>Nombres y Apellidos</th>
										<th>C.I:</th>
										<th>Fecha de Nacimiento</th>
										<th>Parentesco</th>
										<th>Edad</th>
									</tr>
								</thead>
								<tbody>
									<tr id="familia1" style="display:none;">
										<td>1</td>
										<td>
						              		<input  id="nombreapellido1" type="text" class="form-control input-sm"   />	
						              	</td>
						              	<td>
						              		<input  id="cedula1" type="text" class="form-control input-sm"   />				              		
						              	</td>
						              	<td>
						              		<input  id="fecha1" type="date" class="form-control input-sm date-pick"   />				              		
						              	</td>
						              	<td>
						              		<input  id="parentesco1" type="text" class="form-control input-sm"   />			              		
						              	</td>
						              	<td>
						              		<input  id="edad1" type="text" class="form-control input-sm" />					              		
						              	</td>
									</tr>
									<tr id="familia2" style="display:none;">
										<td>2</td>
										<td>
						              		<input  id="nombreapellido2" type="text" class="form-control input-sm"   />	
						              	</td>
						              	<td>
						              		<input  id="cedula2" type="text" class="form-control input-sm"   />				              		
						              	</td>
						              	<td>
						              		<input  id="fecha2" type="date" class="form-control input-sm date-pick"   />				              		
						              	</td>
						              	<td>
						              		<input  id="parentesco2" type="text" class="form-control input-sm"  />			              		
						              	</td>
						              	<td>
						              		<input  id="edad2" type="text" class="form-control input-sm"  />					              		
						              	</td>
									</tr>
									<tr id="familia3" style="display:none;">
										<td>3</td>
										<td>
						              		<input  id="nombreapellido3" type="text" class="form-control input-sm"  />	
						              	</td>
						              	<td>
						              		<input  id="cedula3" type="text" class="form-control input-sm"  />				              		
						              	</td>
						              	<td>
						              		<input  id="fecha3" type="date" class="form-control input-sm date-pick"   />				              		
						              	</td>
						              	<td>
						              		<input  id="parentesco3" type="text" class="form-control input-sm"  />			              		
						              	</td>
						              	<td>
						              		<input  id="edad3" type="text" class="form-control input-sm"  />					              		
						              	</td>
									</tr>
									<tr id="familia4" style="display:none;">
										<td>4</td>
										<td>
						              		<input  id="nombreapellido4" type="text" class="form-control input-sm"  />	
						              	</td>
						              	<td>
						              		<input  id="cedula4" type="text" class="form-control input-sm"  />				              		
						              	</td>
						              	<td>
						              		<input  id="fecha4" type="date" class="form-control input-sm date-pick"   />				              		
						              	</td>
						              	<td>
						              		<input  id="parentesco4" type="text" class="form-control input-sm"   />			              		
						              	</td>
						              	<td>
						              		<input  id="edad4" type="text" class="form-control input-sm"   />					              		
						              	</td>
									</tr>
									<tr id="familia5" style="display:none;">
										<td>5</td>
										<td>
						              		<input  id="nombreapellido5" type="text" class="form-control input-sm"   />	
						              	</td>
						              	<td>
						              		<input  id="cedula5" type="text" class="form-control input-sm"   />				              		
						              	</td>
						              	<td>
						              		<input  id="fecha5" type="date" class="form-control input-sm date-pick"   />				              		
						              	</td>
						              	<td>
						              		<input  id="parentesco5" type="text" class="form-control input-sm"   />			              		
						              	</td>
						              	<td>
						              		<input  id="edad5" type="text" class="form-control input-sm"   />					              		
						              	</td>
									</tr>
								</tbody>
							</table>
						</center>
					</section>

			        <center>
				         <div class="row">
							<div class="col-md-4">
				                  <button type="submit" class="btn btn-sm btn-default" id="afiliar"><span class="glyphicon glyphicon-plus"></span>Afiliar</button>				
							</div>
							<div class="col-md-4">
							</div>
							<div class="col-md-4">
				                  <button type="button" class="btn btn-sm btn-danger" id="limpiar"><span class="glyphicon glyphicon-trash"></span>Limpiar</button>					

							</div>
						</div>        	
			        </center>
			     <!-- <div class="alert alert-warning">Para Acceder al sistema debe ingresar sus datos de usuario</div> -->
			 </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="buscarsocio">
    <br>
      <center>
      	<div id="alertas"> <div class="alert alert-info"><h4><b>Buscar Socio</b></h4></div></div>
      	<hr>
      	<h5><b>Ingrese la cédula a buscar</b></h5>
      	<hr>
      </center> 
      <center>
	       <div class="row">
				<div class="col-md-11">
					<div class="form-group">
						<label class="col-sm-4 control-label">Cédula de identidad:</label>
			          <div class="col-sm-4">
			              <input  id="cedulabusqueda" type="text" class="form-control input-sm" name="cedulabusqueda" >  
			          </div>
			          <div class="col-md-3">
			             <button type="button" class="btn btn-sm btn-info" id="buscar"><span class="glyphicon glyphicon-search"></span>Buscar</button>	 
			          </div>
		          </div>
				</div>
				 <div class="col-md-1">
				 	<div class="form-group" style="display:none;">
						<label class="col-sm-4 control-label">Código del socio:</label>
			          <div class="col-sm-4">
			              <input  id="codigobuscar" type="text" class="form-control input-sm" name="codigobuscar" >  
			          </div>
			          
		          </div>
		    
				</div>
			</div>
		</center>
<br><hr>
			<!-- comienzo de resultado -->
			<section id="resultado" style="display:none;">
				<form id="formresultadobusqueda" method="post" class="form-signin form-horizontal"  onsubmit="return false;">  
			      <center>
			      	<div id="alertas3"> <div class="alert alert-info"><h4><b>Socio Encontrado</b></h4></div></div>
			      </center>   

			       <div class="row">
						<div class="col-md-6">
					       <div class="form-group">
					          <label class="col-sm-5 control-label">Cedula de Identidad:</label>
					          <div class="col-sm-7">
					                <input id="btc" type="text" class="form-control input-sm"  name="bci" readonly="true" />
					          </div>
					      	</div> 
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Nombres:</label>
					          <div class="col-sm-7">
					              <input  id="btnombre" type="text" class="form-control input-sm" name="bnombre" readonly="true" />  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Correo:</label>
					          <div class="col-sm-7">
					              <input  id="btemail" type="text" class="form-control input-sm" name="bemail" readonly="true"/>  
					          </div>
					      </div>
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Lugar de Nacimiento:</label>
					          <div class="col-sm-7">
					              <input  id="btldn" type="text" class="form-control input-sm" name="bldn" readonly="true"/>  
					          </div>
					      </div>    
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Dirección:</label>
					          <div class="col-sm-7">
					           		<textarea id="btd"rows="3" cols="50" class="form-control input-sm" name="bdireccion" readonly="true"></textarea>
					             
					          </div>
					      </div> 
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Teléfono Celular:</label>
					          <div class="col-sm-7">
					              <input  id="btcelular" type="text" class="form-control input-sm" name="bcelular" readonly="true">  
					          </div>
					      </div>          
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Departamento:</label>
					          <div class="col-sm-7">
					               <select  class="form-control input-sm"  name="bdepartamento"  id="btdepartamento"  readonly="true" >
										
					               </select> 
					          </div>
					      </div> 
					     <!--  <div class="form-group">
					          <label class="col-sm-5 control-label">Estatus:</label>
					          <div class="col-sm-7">
					              <select  class="form-control input-sm"  name="bestado"  id="btestado"  readonly="true" >
					               		<option value="0" disabled selected> --Seleccione una opcion -- </option>
					               		<option value="Estado1">Estado 1</option>
					               		<option value="Estado2">Estado 2</option>
					               		<option value="Estado3">Estado 3</option>
					               		<option value="Estado4">Estado 4</option>
					               		<option value="Estado5">Estado 5</option>
					               </select> 
					          </div>
					      </div>  -->  
					      <div class="form-group">
					          <label class="col-sm-5 control-label">Cantidad  de Familiares:</label>
					          <div class="col-sm-7">
					              <select class="form-control input-sm"  name="bcantidadfamiliares"  id="btcfamiliar"  readonly="true">
					               		<option value="-1" disabled selected> --Seleccione una opcion -- </option>
					               		<option value="0">0</option>
					               		<option value="1">1</option>
					               		<option value="2">2</option>
					               		<option value="3">3</option>
					               		<option value="4">4</option>
					               		<option value="5">5</option>
					               </select>  
					          </div>
					      </div>	
					       <div class="form-group">
									<label class="col-sm-5 control-label">Salario </label>
						          <div class="col-sm-7">
										<input  id="btsalario" type="text" class="form-control input-sm" name="bsalario" readonly="true" />  
						          </div>
					          </div>
						</div>			
						
						<div class="col-md-6">
							 <div class="form-group">
									<label class="col-sm-5 control-label">Codigo Socio:</label>
						          <div class="col-sm-7">
						              <input  id="btcodigosocio" type="text" class="form-control input-sm" name="bcodigosocio"  disabled />  
						          </div>
					          </div>
			      			  <div class="form-group">
									<label class="col-sm-5 control-label">Apellidos:</label>
						          <div class="col-sm-7">
						              <input  id="btapellido" type="text" class="form-control input-sm" name="bapellido" readonly="true" />  
						          </div>
					          </div>
			      			  <div class="form-group">
									<label class="col-sm-5 control-label">Fecha de Nacimiento:</label>
						          <div class="col-sm-7">
						              <input  id="btfechadenacimiento" type="text" class="form-control input-sm date-pick"   name="bfechadenacimiento"/>  
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Estado Civil:</label>
						          <div class="col-sm-7">
						              <select  class="form-control input-sm"  name="bestadocivil"  id="btestadocivil" readonly="true" >
						              	<option value="Soltero">Soltero</option>
					               		<option value="Casado">Casado</option>		
					               		<option value="Viudo">Viudo</option>		               		
					               		<option value="Divorciado">Divorciado</option>		  	               		
					               	  </select>   
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Teléfono Hab.:</label>
						          <div class="col-sm-7">
						              <input  id="bttelefonohab" type="text" class="form-control input-sm" name="btelefonohabitacion" readonly="true"/>  
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Gremio/Cargo:</label>
						          <div class="col-sm-7">
						              <select  class="form-control input-sm" name="bgremiocargo" id="btgremiocargo"  readonly="true" >
								              		<!-- <option value="0" disabled selected> --Seleccione una opcion -- </option>
								               		<option value="cContratado">Contratado</option>
								               		<option value="Administrativo">Administrativo</option>
								               		<option value="Trabajador">Trabajador de la CA</option>
								               		<option value="Asistente">Asistente de la CA</option>	
								               		<option value="Obrero">Obrero</option> -->
								            </select>     
						          </div>
					          </div>
					          <div class="form-group">
									<label class="col-sm-5 control-label">Condición:</label>
						          <div class="col-sm-7">
						              <select  class="form-control input-sm"  name="bcondicion"  id="btcondicion"  readonly="true" >
              				               	<option value="0" disabled selected> --Seleccione una opcion -- </option>
						               		<option value="Contratado">Contratado</option>
						               		<option value="Ordinario">Ordinario</option>
						               		<option value="Fijo">Fijo</option>
						               		<option value="Jubilado">Jubilado</option>	
						               		<option value="Incapacitado">Incapacitado</option>		
             		
					               	  </select>   
						          </div>
					          </div>
					           <div class="form-group">
									<label class="col-sm-5 control-label">Fecha de Ingreso:</label>
						          <div class="col-sm-7">
						              <input  id="btfechadeingreso" type="date" class="form-control input-sm" name="bfechadeingreso"/>  
						          </div>
					          </div>
				

						</div>
					</div>
					<section id="listafamilia2" style="display:none;">

						<br>
						<hr>
						<section id="alerta2b"></section>
						<center>
							<table class="table">
								<thead>
									<tr>
										<th>Nro</th>
										<th>Nombres y Apellidos</th>
										<th>C.I:</th>
										<th>Fecha de Nacimiento</th>
										<th>Parentesco</th>
										<th>Edad</th>
									</tr>
								</thead>
								<tbody>
									<tr id="bfamilia1" style="display:none;">
										<td>1</td>
										<td>
						              		<input  id="bnombreapellido1" type="text" class="form-control input-sm" readonly="true"/>	
						              	</td>
						              	<td>
						              		<input  id="bcedula1" type="text" class="form-control input-sm"   readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bfecha1" type="date" class="form-control input-sm date-pick"   readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bparentesco1" type="text" class="form-control input-sm"  readonly="true" />			              		
						              	</td>
						              	<td>
						              		<input  id="bedad1" type="text" class="form-control input-sm"   readonly="true"/>					              		
						              	</td>
									</tr>
									<tr id="bfamilia2" style="display:none;">
										<td>2</td>
										<td>
						              		<input  id="bnombreapellido2" type="text" class="form-control input-sm"  readonly="true" />	
						              	</td>
						              	<td>
						              		<input  id="bcedula2" type="text" class="form-control input-sm"  readonly="true" />				              		
						              	</td>
						              	<td>
						              		<input  id="bfecha2" type="date" class="form-control input-sm date-pick"  readonly="true" />				              		
						              	</td>
						              	<td>
						              		<input  id="bparentesco2" type="text" class="form-control input-sm"   readonly="true"/>			              		
						              	</td>
						              	<td>
						              		<input  id="bedad2" type="text" class="form-control input-sm" readonly="true"/>					              		
						              	</td>
									</tr>
									<tr id="bfamilia3" style="display:none;">
										<td>3</td>
										<td>
						              		<input  id="bnombreapellido3" type="text" class="form-control input-sm" readonly="true"/>	
						              	</td>
						              	<td>
						              		<input  id="bcedula3" type="text" class="form-control input-sm" readonly="true" />				              		
						              	</td>
						              	<td>
						              		<input  id="bfecha3" type="text" class="form-control input-sm date-pick"  readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bparentesco3" type="text" class="form-control input-sm" readonly="true"/>			              		
						              	</td>
						              	<td>
						              		<input  id="bedad3" type="text" class="form-control input-sm" readonly="true"/>					              		
						              	</td>
									</tr>
									<tr id="bfamilia4" style="display:none;">
										<td>4</td>
										<td>
						              		<input  id="bnombreapellido4" type="text" class="form-control input-sm"  readonly="true"/>	
						              	</td>
						              	<td>
						              		<input  id="bcedula4" type="text" class="form-control input-sm"  readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bfecha4" type="text" class="form-control input-sm date-pick" readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bparentesco4" type="text" class="form-control input-sm"   readonly="true"/>			              		
						              	</td>
						              	<td>
						              		<input  id="bedad4" type="text" class="form-control input-sm" readonly="true" />					              		
						              	</td>
									</tr>
									<tr id="bfamilia5" style="display:none;">
										<td>5</td>
										<td>
						              		<input  id="bnombreapellido5" type="text" class="form-control input-sm" readonly="true" />	
						              	</td>
						              	<td>
						              		<input  id="bcedula5" type="text" class="form-control input-sm"  readonly="true"/>				              		
						              	</td>
						              	<td>
						              		<input  id="bfecha5" type="text" class="form-control input-sm date-pick"  readonly="true" />				              		
						              	</td>
						              	<td>
						              		<input  id="bparentesco5" type="text" class="form-control input-sm"  readonly="true" />			              		
						              	</td>
						              	<td>
						              		<input  id="bedad5" type="text" class="form-control input-sm"  readonly="true" />					              		
						              	</td>
									</tr>
								</tbody>
							</table>
						</center>
					</section>

			        <section id="controlresultados" style="display:none;">
	
					    	  <center>
						         <div class="row">
									<div class="col-md-4">
						                  <button type="button" class="btn btn-sm btn-default" id="modificar" ><span class="glyphicon glyphicon-pencil"></span>Modificar</button>				
									</div>
							<!-- 		<div class="col-md-3">
						                  <button type="button" class="btn btn-default" id="limpiar"><span class="glyphicon glyphicon-trash"></span>Limpiar</button>					
									</div> -->
									<div class="col-md-4">
						                  <button type="submit" class="btn btn-sm btn-primary" id="guardar" data-toggle="modal" data-target="#modalguardar"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>				
									</div>
									<div class="col-md-4">
						                  <button type="button" class="btn btn-sm btn-danger" id="cancelar" data-toggle="modal" data-target="#modaleliminar"><span class="glyphicon glyphicon-remove"></span>Eliminar/Desafiliar</button>				
									</div>
								</div>        	
						    </center>
					</section>
			 </form>
			</section>


			<!--Fin seccion de resultado -->

    </div>

  </div>

</div>
<br><hr>
<div class="modal fade" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Confirmación</b></h4>
      </div>
      <div class="modal-body">
        <center>¿Esta Seguro que desea eliminar/desafiliar?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="modalguardar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Confirmación</b></h4>
      </div>
      <div class="modal-body">
        <center>¿Esta Seguro que desea modificar los datos?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="confirmacion">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalmodificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><b>Confirmación</b></h4>
      </div>
      <div class="modal-body">
        <center>¿Esta Seguro que desea modificar los datos?</center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary">Aceptar</button>
      </div>
    </div>
  </div>
</div> -->





 <script type="text/javascript" src="js/bootstrapValidator.js"></script>
  <script src="js/bootstrapValidator.min.js"></script>
  <script type="text/javascript"> 
    $(document).ready(function() {
asignarIdSocio();
		/*$('.date-pick').datePicker(
		{
			startDate: '01/01/1970',
			endDate: (new Date()).asString()
		}
		);*/
    $('#formregistrosocio')
          .bootstrapValidator({
              feedbackIcons: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                ci: {
                    validators: {
                        notEmpty: {
                             message: 'Se requiere este campo'
                        },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        },
		         		stringLength: {
                       		 max: 8,
                        	message: 'Por favor introduce menos de %s caracteres'
                    	}                     
                    }
                },
                  nombre: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                          regexp: {
		                        regexp: /^[a-z\s]+$/i,
		                        message: 'Solo se permiten letras de a-z, A-Z'
		                    }
                      }
                  },
                 email: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        emailAddress: {
                            message: 'Es invalido el email'
                        }
                      }
                  },
                  ldn: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  direccion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  celular: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }
                      }
                  },
                  departamento: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  estado: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  cantidadfamiliares: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  codigosocio: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  apellido: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                          regexp: {
		                        regexp: /^[a-z\s]+$/i,
		                        message: 'Solo se permiten letras de a-z, A-Z'
		                    }
                      }
                  },
                  fechadenacimiento: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  estadocivil: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  gremiocargo: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  condicion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  fechadeingreso: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  telefonohabitacion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }

                      }
                  },
                   salario: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }

                      }
                  }

              }

          });



  $('#formresultadobusqueda')
          .bootstrapValidator({
              feedbackIcons: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                bci: {
                    validators: {
                        notEmpty: {
                             message: 'Se requiere este campo'
                        },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        },
		         		stringLength: {
                       		 max: 8,
                        	message: 'Por favor introduce menos de %s caracteres'
                    	}                     
                    }
                },
                  bnombre: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                          regexp: {
		                        regexp: /^[a-z\s]+$/i,
		                        message: 'Solo se permiten letras de a-z, A-Z'
		                    }
                      }
                  },
                  bemail: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        emailAddress: {
                            message: 'Es invalido el email'
                        }
                      }
                  },
                  bldn: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bdireccion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bcelular: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }
                      }
                  },
                  bdepartamento: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bestado: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bcantidadfamiliares: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bcodigosocio: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bapellido: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                          regexp: {
		                        regexp: /^[a-z\s]+$/i,
		                        message: 'Solo se permiten letras de a-z, A-Z'
		                    }
                      }
                  },
                  bfechadenacimiento: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bestadocivil: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bgremiocargo: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bcondicion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  bfechadeingreso: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          }
                      }
                  },
                  btelefonohabitacion: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }

                      }
                  },                  
                  bsalario: {
                      validators: {
                          notEmpty: {
                              message: 'Se requiere este campo'
                          },
                        digits: {
				             message: 'Por favor introduce sólo dígitos'
				        }

                      }
                  }

              }

          }) .on('err.field.fv', function(e, data) {
            data.fv.disableSubmitButtons(false);
        }).on('success.field.fv', function(e, data) {
            if (!data.fv.isValid()) {    // There is invalid field
       			 data.fv.disableSubmitButtons(true);                   
            }
        });

 function asignarIdSocio(){
 	 $.ajax
        ({
        type: "POST",
        url: "controlador/controladorSocio.php",
        data: {id:3},
        async: false,   
        dataType: "json",   
        success:
        function (msg) 
        {   
            tcodigosocio.value=msg;
         },
            error:
            function (msg) {console.log( msg +"No se pudo realizar la conexion");}
        });
 }

cargarDepartamentos();
cargarCargos();
 function cargarDepartamentos(){
  $.ajax
        ({
          type: "POST",
          url: "controlador/controladorDepartamento.php",
          data: {id:1},
          dataType: "json",  
          async: false,  
          success:
          function (msg) 
          { 
          		/*console.log(msg);*/
              $('#tdepartamento').empty();           
              $('#tdepartamento').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
              $('#btdepartamento').empty();           
              $('#btdepartamento').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                  
                  for (var i =0 ; i<msg[0].m; i++) {                 
                        $('#tdepartamento').append('<option  value="'+msg[i].iddepartamento+'" >'+msg[i].nombre+'</option>');
                        $('#btdepartamento').append('<option  value="'+msg[i].iddepartamento+'" >'+msg[i].nombre+'</option>');
                  		
                  }
          },
          error: function(jqXHR, textStatus, errorThrown) {
			  console.log(textStatus, errorThrown);
			}
        });
  }

   function cargarCargos(){
  $.ajax
        ({
          type: "POST",
          url: "controlador/controladorCargo.php",
          data: {id:1},
          dataType: "json",  
          async: false,  
          success:
          function (msg) 
          { 
          		/*console.log(msg);*/
              $('#tgremiocargo').empty();           
              $('#tgremiocargo').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
              $('#btgremiocargo').empty();           
              $('#btgremiocargo').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
                
                  for (var i =0 ; i<msg[0].m; i++) { 
                        $('#tgremiocargo').append('<option  value="'+msg[i].idcargo+'" >'+msg[i].nombre+'</option>');
                        $('#btgremiocargo').append('<option  value="'+msg[i].idcargo+'" >'+msg[i].nombre+'</option>');
                  }
          },
          error: function(jqXHR, textStatus, errorThrown) {
			  console.log(textStatus, errorThrown);
			}
        });
  }
 function afiliar(familiares){
	$.ajax
      ({
      type: "POST",
      url: "controlador/controladorSocio.php",
      data: {id:1, salario:tsalario.value, cedula:tc.value, nombre:tnombre.value,  email:temail.value, lugardenac:tldn.value, direccion:td.value, celular:tcelular.value,  departamento:$('#tdepartamento').val(),  cantidadfamiliares:$('#tcfamiliar').val(),   apellido:tapellido.value, fechadenac:tfechadenacimiento.value,  estadocivil:$('#testadocivil').val(), telefonohabitacion:ttelefonohab.value,  gremiocargo:$('#tgremiocargo').val(), condicion:$('#tcondicion').val(), fechadeingreso:tfechadeingreso.value, familiares:familiares},
      dataType: "json",
      async: false,           
      success:
      function (msg) 
      {   	console.log(msg);
		    if(msg=="repetido"){
				alertas2.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡El correo o la cedula ya existe por favor escriba otro!</div>';
			}
			else{
				limpiar();
				alertas2.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Se ha registrado correctamente!</div>';
			}
      },
          error:
          function (msg) {console.log( msg +"No se pudo realizar la conexion");}
      });
 }

	$('#confirmacion').on('click',  function(){
		console.log("confirmado");
	});
 $('#modificar').on('click',  function(){

    	$('#btnombre').removeAttr('readonly');
    	$('#btldn').removeAttr('readonly');
    	$('#btd').removeAttr('readonly');
    	$('#btcelular').removeAttr('readonly');
    	$('#btdepartamento').removeAttr('readonly');
    	/*$('#btestado').removeAttr('readonly');*/
    	$('#btcfamiliar').removeAttr('readonly');
    	$('#btapellido').removeAttr('readonly');
    	$('#btfechadenacimiento').removeAttr('readonly');
    	$('#btestadocivil').removeAttr('readonly');
    	$('#bttelefonohab').removeAttr('readonly');
    	$('#btgremiocargo').removeAttr('readonly');
    	$('#btcondicion').removeAttr('readonly');
    	$('#btfechadeingreso').removeAttr('readonly');
    	$('#bfamiliares').removeAttr('readonly');
    	$('#btsalario').removeAttr('readonly');
    	if($('#btcfamiliar').val()==1){
				$('#bnombreapellido1').removeAttr('readonly');
				$('#bcedula1').removeAttr('readonly');
				$('#bfecha1').removeAttr('readonly');
				$('#bparentesco1').removeAttr('readonly');
				$('#bedad1').removeAttr('readonly');
    	}
		if($('#btcfamiliar').val()==2){
				$('#bnombreapellido1').removeAttr('readonly');
				$('#bcedula1').removeAttr('readonly');
				$('#bfecha1').removeAttr('readonly');
				$('#bparentesco1').removeAttr('readonly');
				$('#bedad1').removeAttr('readonly');
				
				$('#bnombreapellido2').removeAttr('readonly');
				$('#bcedula2').removeAttr('readonly');
				$('#bfecha2').removeAttr('readonly');
				$('#bparentesco2').removeAttr('readonly');
				$('#bedad2').removeAttr('readonly');
    	}
		if($('#btcfamiliar').val()==3){
				$('#bnombreapellido1').removeAttr('readonly');
				$('#bcedula1').removeAttr('readonly');
				$('#bfecha1').removeAttr('readonly');
				$('#bparentesco1').removeAttr('readonly');
				$('#bedad1').removeAttr('readonly');

				$('#bnombreapellido2').removeAttr('readonly');
				$('#bcedula2').removeAttr('readonly');
				$('#bfecha2').removeAttr('readonly');
				$('#bparentesco2').removeAttr('readonly');
				$('#bedad2').removeAttr('readonly');

				$('#bnombreapellido3').removeAttr('readonly');
				$('#bcedula3').removeAttr('readonly');
				$('#bfecha3').removeAttr('readonly');
				$('#bparentesco3').removeAttr('readonly');
				$('#bedad3').removeAttr('readonly');

    	}

    	if($('#btcfamiliar').val()==4){
				$('#bnombreapellido1').removeAttr('readonly');
				$('#bcedula1').removeAttr('readonly');
				$('#bfecha1').removeAttr('readonly');
				$('#bparentesco1').removeAttr('readonly');
				$('#bedad1').removeAttr('readonly');

				$('#bnombreapellido2').removeAttr('readonly');
				$('#bcedula2').removeAttr('readonly');
				$('#bfecha2').removeAttr('readonly');
				$('#bparentesco2').removeAttr('readonly');
				$('#bedad2').removeAttr('readonly');

				$('#bnombreapellido3').removeAttr('readonly');
				$('#bcedula3').removeAttr('readonly');
				$('#bfecha3').removeAttr('readonly');
				$('#bparentesco3').removeAttr('readonly');
				$('#bedad3').removeAttr('readonly');

				$('#bnombreapellido4').removeAttr('readonly');
				$('#bcedula4').removeAttr('readonly');
				$('#bfecha4').removeAttr('readonly');
				$('#bparentesco4').removeAttr('readonly');
				$('#bedad4').removeAttr('readonly');

    	}
    	if($('#btcfamiliar').val()==5){
				$('#bnombreapellido1').removeAttr('readonly');
				$('#bcedula1').removeAttr('readonly');
				$('#bfecha1').removeAttr('readonly');
				$('#bparentesco1').removeAttr('readonly');
				$('#bedad1').removeAttr('readonly');

				$('#bnombreapellido2').removeAttr('readonly');
				$('#bcedula2').removeAttr('readonly');
				$('#bfecha2').removeAttr('readonly');
				$('#bparentesco2').removeAttr('readonly');
				$('#bedad2').removeAttr('readonly');

				$('#bnombreapellido3').removeAttr('readonly');
				$('#bcedula3').removeAttr('readonly');
				$('#bfecha3').removeAttr('readonly');
				$('#bparentesco3').removeAttr('readonly');
				$('#bedad3').removeAttr('readonly');

				$('#bnombreapellido4').removeAttr('readonly');
				$('#bcedula4').removeAttr('readonly');
				$('#bfecha4').removeAttr('readonly');
				$('#bparentesco4').removeAttr('readonly');
				$('#bedad4').removeAttr('readonly');

				$('#bnombreapellido5').removeAttr('readonly');
				$('#bcedula5').removeAttr('readonly');
				$('#bfecha5').removeAttr('readonly');
				$('#bparentesco5').removeAttr('readonly');
				$('#bedad5').removeAttr('readonly');

    	}

 });
	$('#buscar').on('click',  function(){
		if(cedulabusqueda.value==""&&codigobuscar.value==""){
				alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Escriba una cedula o el codigo del socio!</div>';

		}
		else{
				$.ajax
			      ({
			      type: "POST",
			       url: "controlador/controladorSocio.php",
			      data: {id:2, cedula:cedulabusqueda.value, socio:codigobuscar.value},
			      dataType: "json",
			      async: false,           
			      success:
			      function (msg) 
			      {   	console.log(msg[0].m);
					    if(msg[0].m>0){
						alertas.innerHTML='';
						console.log("gremiocargo: "+msg[0].gremiocargo);
					    	$('#resultado').show();
					    	$('#controlresultados').show();

					    	btsalario.value=msg[0].salario;
					    	btc.value=msg[0].cedula; 
					    	btnombre.value=msg[0].nombres;
					    	btemail.value=msg[0].correo;
					    	btldn.value=msg[0].lugardenacimiento;
					    	btd.value=msg[0].direccion;
					    	btcelular.value=msg[0].celular;
					    	$('#btdepartamento').val(msg[0].departamento);
					    	/*$('#btestado').val(msg[0].estado)*/
					    	$('#btcfamiliar').val(msg[0].cantidaddefamiliares);
					    	btapellido.value=msg[0].apellidos;
					    	btfechadenacimiento.value=msg[0].fechadenacimiento;
					    	$('#btestadocivil').val(msg[0].estadocivil);
					    	bttelefonohab.value=msg[0].telfonohab;
					    	$('#btgremiocargo').val(msg[0].gremiocargo); 
					    	$('#btcondicion').val(msg[0].condicion);
					    	btfechadeingreso.value=msg[0].fechadeingreso;
					    	bfamiliares=msg[0].cantidaddefamiliares;
					    	if(bfamiliares>0){
					    		$('#listafamilia2').show();

					    		if(bfamiliares==1){
					    			$('#bfamilia1').fadeIn();
									bnombreapellido1.value=msg[0]['familiares'][0];
									bcedula1.value=msg[0]['familiares'][1];
									bfecha1.value=msg[0]['familiares'][2];
									bparentesco1.value=msg[0]['familiares'][3];
									bedad1.value=msg[0]['familiares'][4];
					    			
					    		}
					    		if(bfamiliares==2){
					    			$('#bfamilia1').fadeIn();
					    			$('#bfamilia2').fadeIn();
					    			$('#bfamilia3').fadeIn();
					    			bnombreapellido1.value=msg[0]['familiares'][0];
									bcedula1.value=msg[0]['familiares'][1];
									bfecha1.value=msg[0]['familiares'][2];
									bparentesco1.value=msg[0]['familiares'][3];
									bedad1.value=msg[0]['familiares'][4];
					    			bnombreapellido2.value=msg[0]['familiares'][5];
									bcedula2.value=msg[0]['familiares'][6];
									bfecha2.value=msg[0]['familiares'][7];
									bparentesco2.value=msg[0]['familiares'][8];
									bedad2.value=msg[0]['familiares'][9];

					    		}
					    		if(bfamiliares==3){
					    			$('#bfamilia1').fadeIn();
					    			$('#bfamilia2').fadeIn();
					    			$('#bfamilia3').fadeIn();
					    			bnombreapellido1.value=msg[0]['familiares'][0];
									bcedula1.value=msg[0]['familiares'][1];
									bfecha1.value=msg[0]['familiares'][2];
									bparentesco1.value=msg[0]['familiares'][3];
									bedad1.value=msg[0]['familiares'][4];


									bnombreapellido2.value=msg[0]['familiares'][5];
									bcedula2.value=msg[0]['familiares'][6];
									bfecha2.value=msg[0]['familiares'][7];
									bparentesco2.value=msg[0]['familiares'][8];
									bedad2.value=msg[0]['familiares'][9];

									bnombreapellido3.value=msg[0]['familiares'][10];
									bcedula3.value=msg[0]['familiares'][11];
									bfecha3.value=msg[0]['familiares'][12];
									bparentesco3.value=msg[0]['familiares'][13];
									bedad3.value=msg[0]['familiares'][14];


					    		}
					    		if(bfamiliares==4){
					    			$('#bfamilia1').fadeIn();
					    			$('#bfamilia2').fadeIn();
					    			$('#bfamilia3').fadeIn();
					    			$('#bfamilia4').fadeIn();
					    			bnombreapellido1.value=msg[0]['familiares'][0];
									bcedula1.value=msg[0]['familiares'][1];
									bfecha1.value=msg[0]['familiares'][2];
									bparentesco1.value=msg[0]['familiares'][3];
									bedad1.value=msg[0]['familiares'][4];


									bnombreapellido2.value=msg[0]['familiares'][5];
									bcedula2.value=msg[0]['familiares'][6];
									bfecha2.value=msg[0]['familiares'][7];
									bparentesco2.value=msg[0]['familiares'][8];
									bedad2.value=msg[0]['familiares'][9];

									bnombreapellido3.value=msg[0]['familiares'][10];
									bcedula3.value=msg[0]['familiares'][11];
									bfecha3.value=msg[0]['familiares'][12];
									bparentesco3.value=msg[0]['familiares'][13];
									bedad3.value=msg[0]['familiares'][14];

									bnombreapellido4.value=msg[0]['familiares'][15];
									bcedula4.value=msg[0]['familiares'][16];
									bfecha4.value=msg[0]['familiares'][17];
									bparentesco4.value=msg[0]['familiares'][18];
									bedad4.value=msg[0]['familiares'][19];


					    		}
					    		if(bfamiliares==5){
					    			$('#bfamilia1').fadeIn();
					    			$('#bfamilia2').fadeIn();
					    			$('#bfamilia3').fadeIn();
					    			$('#bfamilia4').fadeIn();
					    			$('#bfamilia5').fadeIn();
									bnombreapellido1.value=msg[0]['familiares'][0];
									bcedula1.value=msg[0]['familiares'][1];
									bfecha1.value=msg[0]['familiares'][2];
									bparentesco1.value=msg[0]['familiares'][3];
									bedad1.value=msg[0]['familiares'][4];


									bnombreapellido2.value=msg[0]['familiares'][5];
									bcedula2.value=msg[0]['familiares'][6];
									bfecha2.value=msg[0]['familiares'][7];
									bparentesco2.value=msg[0]['familiares'][8];
									bedad2.value=msg[0]['familiares'][9];

									bnombreapellido3.value=msg[0]['familiares'][10];
									bcedula3.value=msg[0]['familiares'][11];
									bfecha3.value=msg[0]['familiares'][12];
									bparentesco3.value=msg[0]['familiares'][13];
									bedad3.value=msg[0]['familiares'][14];

									bnombreapellido4.value=msg[0]['familiares'][15];
									bcedula4.value=msg[0]['familiares'][16];
									bfecha4.value=msg[0]['familiares'][17];
									bparentesco4.value=msg[0]['familiares'][18];
									bedad4.value=msg[0]['familiares'][19];

									bnombreapellido5.value=msg[0]['familiares'][20];
									bcedula5.value=msg[0]['familiares'][21];
									bfecha5.value=msg[0]['familiares'][22];
									bparentesco5.value=msg[0]['familiares'][23];
									bedad5.value=msg[0]['familiares'][24];

								/*	bnombreapellido5.value=msg[0]['familiares'][25];
									bcedula5.value=msg[0]['familiares'][26];
									bfecha5.value=msg[0]['familiares'][27];
									bparentesco5.value=msg[0]['familiares'][28];
									bedad5.value=msg[0]['familiares'][29];*/
					    		}
					    	}

					    }else{
							alertas.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Socio no Encontrado!</div>';

					    }
			      },
			          error:
			          function (msg) {console.log( msg +"No se pudo realizar la conexion");}
			      });
		}
	});
function limpiar(){
	tc.value="";
	tnombre.value="";
	tldn.value="";
	td.value="";
	tcelular.value="";
	temail.value="";
	$('#tdepartamento').val(0);
	/*$('#testado').val(0);*/
	$('#tcfamiliar').val(-1);
	tcodigosocio.value="";
	tapellido.value="";
	tfechadenacimiento.value="";
	$('#testadocivil').val(0);
	ttelefonohab.value="";
	tgremiocargo.value="";
	tsalario.value="";
	$('#tcondicion').val(0);
	tfechadeingreso.value="";
	//tfechadeafiliacion.value="";
	$('.form-group').removeClass('has-success');
	$('.form-group').removeClass('has-error');
	$('.form-group').removeClass('has-error has-feedback');
	$('.form-group').find('small.help-block').hide();
	$('.form-group').find('i.form-control input-sm-feedback').hide();
	$('.form-group').find('i.form-control-feedback').hide();
	nombreapellido1.value="";
	cedula1.value="";
	fecha1.value="";
	parentesco1.value="";
	edad1.value="";

	nombreapellido2.value="";
	cedula2.value="";
	fecha2.value="";
	parentesco2.value="";
	edad2.value="";
	nombreapellido3.value="";
	cedula3.value="";
	fecha3.value="";
	parentesco3.value="";
	edad3.value="";
	nombreapellido4.value="";
	cedula4.value="";
	fecha4.value="";
	parentesco4.value="";
	edad4.value="";
	nombreapellido5.value="";
	cedula5.value="";
	fecha5.value="";
	parentesco5.value="";
	edad5.value="";	
}
$('#guardar').on('click',  function(){
	var familiares=btcfamiliar.value;
		if(familiares==0){
			var arreglo=new Array();
			console.log("Entro en opcion 0");
			modificar(arreglo);


		}
	if(validarDatosFamilia2(btcfamiliar.value)){
		alerta2.innerHTML="";
		if(familiares==1){
			var arreglo=new Array();
			arreglo[0]= familiares=btcfamiliar.value;}
		if(familiares==0){
			var arreglo=new Array();
			console.log("Entro en opcion 0");
			modificar(arreglo);


		}
	}
	if(validarDatosFamilia2(btcfamiliar.value)){
		alerta2.innerHTML="";
		if(familiares==1){
			var arreglo=new Array();
			arreglo[0]=bnombreapellido1.value;
			arreglo[1]=bcedula1.value;
			arreglo[2]=bfecha1.value;
			arreglo[3]=bparentesco1.value;
			arreglo[4]=bedad1.value;

			modificar(arreglo);
			console.log("Entro en opcion 1");
		}
		if(familiares==2){
			var arreglo=new Array();
			/*familia1*/
			arreglo[0]=bnombreapellido1.value;
			arreglo[1]=bcedula1.value;
			arreglo[2]=bfecha1.value;
			arreglo[3]=bparentesco1.value;
			arreglo[4]=bedad1.value;

			/*familia2*/
			arreglo[5]=bnombreapellido2.value;
			arreglo[6]=bcedula2.value;
			arreglo[7]=bfecha2.value;
			arreglo[8]=bparentesco2.value;
			arreglo[9]=bedad2.value;
			modificar(arreglo);
			console.log("Entro en opcion 2");

		}
		if(familiares==3){
			var arreglo=new Array();

			/*familia1*/
			arreglo[0]=bnombreapellido1.value;
			arreglo[1]=bcedula1.value;
			arreglo[2]=bfecha1.value;
			arreglo[3]=bparentesco1.value;
			arreglo[4]=bedad1.value;

			/*familia2*/
			arreglo[5]=bnombreapellido2.value;
			arreglo[6]=bcedula2.value;
			arreglo[7]=bfecha2.value;
			arreglo[8]=bparentesco2.value;
			arreglo[9]=bedad2.value;

			/*familia3*/
			arreglo[10]=bnombreapellido3.value;
			arreglo[11]=bcedula3.value;
			arreglo[12]=bfecha3.value;
			arreglo[13]=bparentesco3.value;
			arreglo[14]=bedad3.value;
			modificar(arreglo);
			console.log("Entro en opcion 3");


		}
		if(familiares==4){
				var arreglo=new Array();
			/*familia1*/
			arreglo[0]=bnombreapellido1.value;
			arreglo[1]=bcedula1.value;
			arreglo[2]=bfecha1.value;
			arreglo[3]=bparentesco1.value;
			arreglo[4]=bedad1.value;

			/*familia2*/
			arreglo[5]=bnombreapellido2.value;
			arreglo[6]=bcedula2.value;
			arreglo[7]=bfecha2.value;
			arreglo[8]=bparentesco2.value;
			arreglo[9]=bedad2.value;

			/*familia3*/
			arreglo[10]=bnombreapellido3.value;
			arreglo[11]=bcedula3.value;
			arreglo[12]=bfecha3.value;
			arreglo[13]=bparentesco3.value;
			arreglo[14]=bedad3.value;

			/*familia4*/
			arreglo[15]=bnombreapellido4.value;
			arreglo[16]=bcedula4.value;
			arreglo[17]=bfecha4.value;
			arreglo[18]=bparentesco4.value;
			arreglo[19]=bedad4.value;
			modificar(arreglo);
			console.log("Entro en opcion 4");

		}

		if(familiares==5){
			var arreglo=new Array();
			/*familia1*/
			arreglo[0]=bnombreapellido1.value;
			arreglo[1]=bcedula1.value;
			arreglo[2]=bfecha1.value;
			arreglo[3]=bparentesco1.value;
			arreglo[4]=bedad1.value;

			/*familia2*/
			arreglo[5]=bnombreapellido2.value;
			arreglo[6]=bcedula2.value;
			arreglo[7]=bfecha2.value;
			arreglo[8]=bparentesco2.value;
			arreglo[9]=bedad2.value;

			/*familia3*/
			arreglo[10]=bnombreapellido3.value;
			arreglo[11]=bcedula3.value;
			arreglo[12]=bfecha3.value;
			arreglo[13]=bparentesco3.value;
			arreglo[14]=bedad3.value;

			/*familia4*/
			arreglo[15]=bnombreapellido4.value;
			arreglo[16]=bcedula4.value;
			arreglo[17]=bfecha4.value;
			arreglo[18]=bparentesco4.value;
			arreglo[19]=bedad4.value;

			/*familia5*/
			arreglo[20]=bnombreapellido5.value;
			arreglo[21]=bcedula5.value;
			arreglo[22]=bfecha5.value;
			arreglo[23]=bparentesco5.value;
			arreglo[24]=bedad5.value;
			modificar(arreglo);
			console.log("Entro en opcion 5");
			
		}
	}
});

function modificar(familiares){
	console.log("entro en la opcion de modificar");
	$.ajax
      ({
      type: "POST",
      url: "controlador/controladorSocio.php",
      data: {id:4, salario:btsalario.value, cedula:btc.value, nombre:btnombre.value,  email:btemail.value, lugardenac:btldn.value, direccion:btd.value, celular:btcelular.value,  departamento:$('#btdepartamento').val(),  cantidadfamiliares:$('#btcfamiliar').val(),   apellido:btapellido.value, fechadenac:btfechadenacimiento.value,  estadocivil:$('#btestadocivil').val(), telefonohabitacion:bttelefonohab.value,  gremiocargo:$('#btgremiocargo').val(), condicion:$('#btcondicion').val(), fechadeingreso:btfechadeingreso.value, familiares:familiares},
      dataType: "json",
      async: false,           
      success:
      function (msg) 
      {   	console.log(msg);
		    if(msg=="true"){				
				alertas3.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Se ha Modificado correctamente!</div>';
			}
      },
          error:
          function (msg) {console.log( msg +"No se pudo realizar la conexion");}
      });
}
$('#afiliar').on('click',  function(){
	var familiares=tcfamiliar.value;
		if(familiares==0){
			var arreglo=new Array();
			console.log("Entro en opcion 0");
			afiliar(arreglo);


		}
	if(validarDatosFamilia(tcfamiliar.value)){
		alerta2.innerHTML="";
		if(familiares==1){
			var arreglo=new Array();
			arreglo[0]=nombreapellido1.value;
			arreglo[1]=cedula1.value;
			arreglo[2]=fecha1.value;
			arreglo[3]=parentesco1.value;
			arreglo[4]=edad1.value;

			afiliar(arreglo);
			console.log("Entro en opcion 1");
		}
		if(familiares==2){
			var arreglo=new Array();
			/*familia1*/
			arreglo[0]=nombreapellido1.value;
			arreglo[1]=cedula1.value;
			arreglo[2]=fecha1.value;
			arreglo[3]=parentesco1.value;
			arreglo[4]=edad1.value;

			/*familia2*/
			arreglo[5]=nombreapellido2.value;
			arreglo[6]=cedula2.value;
			arreglo[7]=fecha2.value;
			arreglo[8]=parentesco2.value;
			arreglo[9]=edad2.value;
			afiliar(arreglo);
			console.log("Entro en opcion 2");

		}
		if(familiares==3){
			var arreglo=new Array();

			/*familia1*/
			arreglo[0]=nombreapellido1.value;
			arreglo[1]=cedula1.value;
			arreglo[2]=fecha1.value;
			arreglo[3]=parentesco1.value;
			arreglo[4]=edad1.value;

			/*familia2*/
			arreglo[5]=nombreapellido2.value;
			arreglo[6]=cedula2.value;
			arreglo[7]=fecha2.value;
			arreglo[8]=parentesco2.value;
			arreglo[9]=edad2.value;

			/*familia3*/
			arreglo[10]=nombreapellido3.value;
			arreglo[11]=cedula3.value;
			arreglo[12]=fecha3.value;
			arreglo[13]=parentesco3.value;
			arreglo[14]=edad3.value;
			afiliar(arreglo);
			console.log("Entro en opcion 3");


		}
		if(familiares==4){
				var arreglo=new Array();
			/*familia1*/
			arreglo[0]=nombreapellido1.value;
			arreglo[1]=cedula1.value;
			arreglo[2]=fecha1.value;
			arreglo[3]=parentesco1.value;
			arreglo[4]=edad1.value;

			/*familia2*/
			arreglo[5]=nombreapellido2.value;
			arreglo[6]=cedula2.value;
			arreglo[7]=fecha2.value;
			arreglo[8]=parentesco2.value;
			arreglo[9]=edad2.value;

			/*familia3*/
			arreglo[10]=nombreapellido3.value;
			arreglo[11]=cedula3.value;
			arreglo[12]=fecha3.value;
			arreglo[13]=parentesco3.value;
			arreglo[14]=edad3.value;

			/*familia4*/
			arreglo[15]=nombreapellido4.value;
			arreglo[16]=cedula4.value;
			arreglo[17]=fecha4.value;
			arreglo[18]=parentesco4.value;
			arreglo[19]=edad4.value;
			afiliar(arreglo);
			console.log("Entro en opcion 4");

		}

		if(familiares==5){
			var arreglo=new Array();
			/*familia1*/
			arreglo[0]=nombreapellido1.value;
			arreglo[1]=cedula1.value;
			arreglo[2]=fecha1.value;
			arreglo[3]=parentesco1.value;
			arreglo[4]=edad1.value;

			/*familia2*/
			arreglo[5]=nombreapellido2.value;
			arreglo[6]=cedula2.value;
			arreglo[7]=fecha2.value;
			arreglo[8]=parentesco2.value;
			arreglo[9]=edad2.value;

			/*familia3*/
			arreglo[10]=nombreapellido3.value;
			arreglo[11]=cedula3.value;
			arreglo[12]=fecha3.value;
			arreglo[13]=parentesco3.value;
			arreglo[14]=edad3.value;

			/*familia4*/
			arreglo[15]=nombreapellido4.value;
			arreglo[16]=cedula4.value;
			arreglo[17]=fecha4.value;
			arreglo[18]=parentesco4.value;
			arreglo[19]=edad4.value;

			/*familia5*/
			arreglo[20]=nombreapellido5.value;
			arreglo[21]=cedula5.value;
			arreglo[22]=fecha5.value;
			arreglo[23]=parentesco5.value;
			arreglo[24]=edad5.value;
			afiliar(arreglo);
			console.log("Entro en opcion 5");
			
		}
	}
});


$('#limpiar').on('click',  function(){
	
	limpiar();

});
$('#tcfamiliar').change(function () {
    console.log(tcfamiliar.value);
    if(tcfamiliar.value==1||tcfamiliar.value>0){
		$('#listafamilia').fadeIn();
		$('#familia1').fadeIn();		
		$('#familia2').fadeOut();
		$('#familia3').fadeOut();
		$('#familia4').fadeOut();		
		$('#familia5').fadeOut();
    }
     if(tcfamiliar.value==2){
     	$('#familia1').fadeIn();
		$('#familia2').fadeIn();
		$('#familia3').fadeOut();
		$('#familia4').fadeOut();		
		$('#familia5').fadeOut();
    }
    if(tcfamiliar.value==3){
    	$('#familia1').fadeIn();
		$('#familia2').fadeIn();
		$('#familia3').fadeIn();
		$('#familia4').fadeOut();		
		$('#familia5').fadeOut();
    }
    if(tcfamiliar.value==4){
    	$('#familia1').fadeIn();
		$('#familia2').fadeIn();
		$('#familia3').fadeIn();
		$('#familia4').fadeIn();
		$('#familia5').fadeOut();

    }
    if(tcfamiliar.value==5){
    	$('#familia1').fadeIn();
		$('#familia2').fadeIn();
		$('#familia3').fadeIn();
		$('#familia4').fadeIn();		
		$('#familia5').fadeIn();
    }
});
     	/*$('#tdepartamento').multiselect();
     	$('#testado').multiselect();
     	$('#testadocivil').multiselect();
     	$('#tcondicion').multiselect();
     	$('#tcfamiliar').multiselect();*/



	function validarDatosFamilia(num){
		var salida=false;
		if(num==1||num==2||num==3||num==4||num==5){
			 if(nombreapellido1.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 1</b>!</div>";        
		          return false;		
		    }
		    else if(!cedula1.value.match(/^[0-9]+$/)||cedula1.value.length>8)
		    {  
		    	console.log("tamano de la cedula 1 "+cedula1.value.length);
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 1 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(fecha1.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 1</b> !</div>";
		        return false;
		    }
		     else if(parentesco1.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 1</b> !</div>";
		        return false;
		    }
		     else if(!edad1.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 1</b> !</div>";
		        return false;
		    }
		    else {salida=true;}
		}
		if(num==2||num==3||num==4||num==5){
			 if(nombreapellido2.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 2</b>!</div>";        
		          return false;		
		    }
		    else if(!cedula2.value.match(/^[0-9]+$/)||cedula2.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 2 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(fecha2.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 2</b> !</div>";
		        return false;
		    }
		     else if(parentesco2.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 2</b> !</div>";
		        return false;
		    }
		     else if(!edad2.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 2</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==3||num==4||num==5){
			 if(nombreapellido3.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 3</b>!</div>";        
		          return false;		
		    }
		    else if(!cedula3.value.match(/^[0-9]+$/)||cedula3.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 3 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(fecha3.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 3</b> !</div>";
		        return false;
		    }
		     else if(parentesco3.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 3</b> !</div>";
		        return false;
		    }
		     else if(!edad3.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 3</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==4||num==5){
			 if(nombreapellido4.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 4</b>!</div>";        
		          return false;		
		    }
		    else if(!cedula4.value.match(/^[0-9]+$/)||cedula4.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 4 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(fecha4.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 4</b> !</div>";
		        return false;
		    }
		     else if(parentesco4.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 4</b> !</div>";
		        return false;
		    }
		     else if(!edad4.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 4</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==5){
			 if(nombreapellido5.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 5</b>!</div>";        
		          return false;		
		    }
		    else if(!cedula5.value.match(/^[0-9]+$/)||cedula5.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula validad del familiar 5 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(fecha5.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 5</b> !</div>";
		        return false;
		    }
		     else if(parentesco5.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 5</b> !</div>";
		        return false;
		    }
		     else if(!edad5.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 5</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}

		return salida;


	}/*-----fin funcion ---*/


function validarDatosFamilia2(num){
		var salida=false;
		if(num==1||num==2||num==3||num==4||num==5){
			 if(bnombreapellido1.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 1</b>!</div>";        
		          return false;		
		    }
		    else if(!bcedula1.value.match(/^[0-9]+$/)||bcedula1.value.length>8)
		    {  
		    	console.log("tamano de la cedula 1 "+bcedula1.value.length);
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 1 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(bfecha1.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 1</b> !</div>";
		        return false;
		    }
		     else if(bparentesco1.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 1</b> !</div>";
		        return false;
		    }
		     else if(!bedad1.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 1</b> !</div>";
		        return false;
		    }
		    else {salida=true;}
		}
		if(num==2||num==3||num==4||num==5){
			 if(bnombreapellido2.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 2</b>!</div>";        
		          return false;		
		    }
		    else if(!bcedula2.value.match(/^[0-9]+$/)||bcedula2.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 2 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(bfecha2.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 2</b> !</div>";
		        return false;
		    }
		     else if(bparentesco2.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 2</b> !</div>";
		        return false;
		    }
		     else if(!bedad2.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 2</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==3||num==4||num==5){
			 if(bnombreapellido3.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 3</b>!</div>";        
		          return false;		
		    }
		    else if(!bcedula3.value.match(/^[0-9]+$/)||bcedula3.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 3 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(bfecha3.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 3</b> !</div>";
		        return false;
		    }
		     else if(bparentesco3.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 3</b> !</div>";
		        return false;
		    }
		     else if(!bedad3.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 3</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==4||num==5){
			 if(bnombreapellido4.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 4</b>!</div>";        
		          return false;		
		    }
		    else if(!bcedula4.value.match(/^[0-9]+$/)||bcedula4.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula valida del familiar 4 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(bfecha4.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 4</b> !</div>";
		        return false;
		    }
		     else if(bparentesco4.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 4</b> !</div>";
		        return false;
		    }
		     else if(!bedad4.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 4</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}
		if(num==5){
			 if(bnombreapellido5.value==""){
		            alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Nombre y Apellido del familiar 5</b>!</div>";        
		          return false;		
		    }
		    else if(!bcedula5.value.match(/^[0-9]+$/)||bcedula5.value.length>8)
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba una <b>Cédula validad del familiar 5 de logintud 8 digitos maximo</b> !</div>";
		        return false;
		    }
		     else if(bfecha5.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor seleccione una <b>fecha valida del familiar 5</b> !</div>";
		        return false;
		    }
		     else if(bparentesco5.value=="")
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba el <b>Parentesco del familiar 5</b> !</div>";
		        return false;
		    }
		     else if(!bedad5.value.match(/^[0-9]+$/))
		    {  
		        alerta2.innerHTML="<div class='alert alert-warning'>¡Por favor escriba la <b>Edad del familiar 5</b> !</div>";
		        return false;
		    }
		     else {salida=true;}
		}

		return salida;


	}/*-----fin funcion ---*/

});
</script>
