 <center>
 	 <br>
  	<div id="alertas"> <div class="alert alert-info"><h4><b>Agregar Aporte</b></h4></div></div>
		<hr>
  </center> 

	<form id="formBuscarSocio" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 
		<div class="row">
		    <div class="col-md-8 col-xs-8 col-sm-8">
					    		<div class="form-group">
					                 <label class="col-sm-5 control-label">Buscar Socio por Cedula</label>
					                  <div class="col-sm-5">
								              <input  id="tBuscarcedula" type="text" class="form-control input-sm" name="Buscarcedula" />
					                  </div>
					              </div>
					    </div>
					    <div class="col-md-2 col-xs-2 col-sm-2"></div>
					    <div class="col-md-2 col-xs-2 col-sm-2">
					    	<button type="submit" class="btn btn-sm btn-info" id="buscar" disabled="true"><span class="glyphicon glyphicon-search"></span>Buscar</button>
					    </div>
		</div>
		<section id="resultado"></section>
	 </form>

    <section id="resultado2"></section>
<section id="panelDatos" style="display:none;">
<hr>

<form id="formregistrarAporte" method="post" class="form-signin form-horizontal"  onsubmit="return false;"> 
	<h6>Datos del Socio</h6>
 		<div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Cedula</label>
                          <div class="col-sm-8">
					              <input  id="tcedula" type="text" class="form-control input-sm" name="cedula" disabled="true" />
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Nombres</label>
                          <div class="col-sm-8">
					              <input  id="tnombre" type="text" class="form-control input-sm" name="nombre"  disabled="true"/>
                          </div>
                      </div>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6">  
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Apellido</label>
                          <div class="col-sm-8">
					              <input  id="tapellido" type="text" class="form-control input-sm" name="apellido" disabled="true" />
                          </div>
                      </div>
		             <div class="form-group">
									<label class="col-sm-4 control-label">Gremio:</label>
						          <div class="col-sm-8">
						              <select  class="form-control input-sm" name="gremio" id="tgremio"  disabled="true" >
								              		
								            </select>     
						          </div>
                          </div>
                </div>
         </div>

         <hr>
          <h6>Fecha de registro del Aporte</h6>
          <div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                  <div class="form-group">
                         <label class="col-sm-4 control-label">Fecha</label>
                          <div class="col-sm-8">
                        <input  id="tfecha" type="month" class="form-control input-sm" name="fechames"   />
                          </div>
                      </div>
                </div>
          </div>

         <h6>RETENCION</h6>
      		<div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Primera Quincena</label>
                          <div class="col-sm-8">
					              <input  id="tprimeraquincena1" type="text" class="form-control input-sm" name="primeraquincena1" />
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Segunda Quincena</label>
                          <div class="col-sm-8">
					              <input  id="tsegundaquincena1" type="text" class="form-control input-sm" name="segundaquincena1" />
                          </div>
                      </div>
                     <!--  <div class="form-group">
                         <label class="col-sm-4 control-label">Segunda Quincena</label>
                          <div class="col-sm-8">
					              <input  id="tsegundaquincena1" type="text" class="form-control input-sm" name="segundaquincena1" />
                          </div>
                      </div> -->
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Incre Sal. Mes</label>
                          <div class="col-sm-8">
					              <input  id="tincrsalmes" type="text" class="form-control input-sm" name="incrsalmes" />
                          </div>
                      </div>
                </div>
                <div class="col-md-6 col-xs-6 col-sm-6">  
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Nivel Alto</label>
                          <div class="col-sm-8">
					              <input  id="tnivelalto" type="text" class="form-control input-sm" name="nivelalto" />
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Div. Año Anterior</label>
                          <div class="col-sm-8">
					              <input  id="tdivanoanterior" type="text" class="form-control input-sm" name="divanoanterior" />
                          </div>
                      </div>
                     <!--  <div class="form-group">
                         <label class="col-sm-4 control-label">Suma de Retencion</label>
                          <div class="col-sm-8">
					              <input  id="tgremio" type="text" class="form-control input-sm" name="gremio" />
                          </div>
                      </div> -->
                </div>
         </div>
        <hr>
        <h6>APORTE PATRONAL</h6>
    		<div class="row">
                <div class="col-md-6 col-xs-6 col-sm-6">
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Primera Quincena</label>
                          <div class="col-sm-8">
					              <input  id="tprimeraquincena2" type="text" class="form-control input-sm" name="primeraquincena2" />
                          </div>
                      </div>
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Segunda Quincena</label>
                          <div class="col-sm-8">
					              <input  id="tsegundaquincena2" type="text" class="form-control input-sm" name="segundaquincena2" />
                          </div>
                      </div>
                  </div>
                   <div class="col-md-6 col-xs-6 col-sm-6">  
                      <div class="form-group">
                         <label class="col-sm-4 control-label">Incre Sal. Mes</label>
                          <div class="col-sm-8">
					              <input  id="tincresalmes2" type="text" class="form-control input-sm" name="incresalmes2" />
                          </div>
                      </div>
                
               
                	<div class="form-group">
                         <label class="col-sm-4 control-label">Nivel Alto</label>
                          <div class="col-sm-8">
					              <input  id="tnivelalto2" type="text" class="form-control input-sm" name="nivelalto2" />
                          </div>
                   </div>
                     
                </div>
         </div>
	<br>
         <div class="row">
			<div class="col-md-6 col-xs-6 col-sm-6">
			</div>
			<div class="col-md-9 col-xs-9 col-sm-9">
			</div>
			<div class="col-md-3 col-xs-3 col-sm-3">
              	<button type="submit" class="btn btn-sm btn-default" id="procesar" disabled="true"><span class="glyphicon glyphicon-cog"></span>Procesar</button>				

			</div>
         </div>
	</form>
  </section>
<script type="text/javascript" src="js/formValidation.js"></script>
<script type="text/javascript" src="js/framework/bootstrap.js"></script>
<script type="text/javascript">
 $(document).ready(function() { 
 	$('#formBuscarSocio').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {                
                Buscarcedula:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             digits: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                }
            }
              
          })
            .on('success.field.fv', function(e, data) {              
            	if (!data.fv.isValid()) {    
                     data.fv.disableSubmitButtons(true);                
                    
                  }
            });	

 	$('#formregistrarAporte').formValidation({
              framework: 'bootstrap',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                fechames:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             }  
                      }
                },                
                primeraquincena1:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                segundaquincena1:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                incrsalmes:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                nivelalto:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                primeraquincena2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                segundaquincena2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                incresalmes2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                nivelalto2:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                },
                divanoanterior:{
                    validators: {
                          notEmpty: {
                                    message: 'Se requiere este campo'
                             },
                             numeric: {
                                   message: 'Por favor introduce sólo dígitos'
                              }  
                      }
                }
            }
              
          })
            .on('success.field.fv', function(e, data) {              
            	if (!data.fv.isValid()) {    
                     data.fv.disableSubmitButtons(true);                
                    
                  }
            });	
 	$('#procesar').on('click',  function(){
			/*console.log("fecha: "+tfecha.value);*/
      $.ajax
          ({
            type: "POST",
            url: "controlador/controladorAportes.php",
            data: {id:1, primeraquincena1:tprimeraquincena1.value, segundaquincena1:tsegundaquincena1.value, nivelalto1:tnivelalto.value, incresalmes1:tincrsalmes.value, primeraquincena2:tprimeraquincena2.value, segundaquincena2:tsegundaquincena2.value, incresalmes2:tincresalmes2.value, nivelalto2:tnivelalto2.value, divanoanterior:tdivanoanterior.value, fecha:tfecha.value },
            async: false,  
            dataType: "json",  
            success:
            function (msg) 
            { 
                  console.log("salida: "+msg);
              
              if(msg=="true"){
                
                  resultado2.innerHTML='<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Aporte Agregado correctamente!</div>';

                  tprimeraquincena1.value="";
                  tsegundaquincena1.value=""; 
                  tnivelalto.value=""; 
                  tincrsalmes.value=""; 
                  tprimeraquincena2.value=""; 
                  tsegundaquincena2.value=""; 
                  tincresalmes2.value=""; 
                  tnivelalto2.value=""; 
                  tdivanoanterior.value="";
                  tcedula.value="";
                  tnombre.value="";
                  tapellido.value="";
                  $('#tgremio').val(""); 
                  tfecha.value=""; 
                  $('#panelDatos').fadeOut();              
                  tBuscarcedula.value="";
                  $('.form-group').removeClass('has-success');
                  $('.form-group').removeClass('has-error');
                  $('.form-group').removeClass('has-error has-feedback');
                  $('.form-group').find('small.help-block').hide();
                  $('.form-group').find('i.form-control input-sm-feedback').hide();

              }
              if(msg=="R"){
                  resultado2.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>¡Ya existe un aporte para la fecha '+tfecha.value+ ' elija otra!</div>';
                  tprimeraquincena1.value="";
                  tsegundaquincena1.value=""; 
                  tnivelalto.value=""; 
                  tincrsalmes.value=""; 
                  tprimeraquincena2.value=""; 
                  tsegundaquincena2.value=""; 
                  tincresalmes2.value=""; 
                  tnivelalto2.value=""; 
                  tdivanoanterior.value="";
                  tcedula.value="";
                  tnombre.value="";
                  tapellido.value="";
                  $('#tgremio').val(""); 
                  tfecha.value=""; 
                  tBuscarcedula.value="";
                  $('#panelDatos').fadeOut();              

                  $('.form-group').removeClass('has-success');
                  $('.form-group').removeClass('has-error');
                  $('.form-group').removeClass('has-error has-feedback');
                  $('.form-group').find('small.help-block').hide();
                  $('.form-group').find('i.form-control input-sm-feedback').hide();

              }
                   
            },
             error: function(jqXHR, textStatus, errorThrown) {
                      console.log("Problema con esto: " +textStatus, errorThrown);
              }
          });
 	
	});
 cargarCargos();
 function cargarCargos(){
  $.ajax
        ({
          type: "POST",
          url: "controlador/controladorCargo.php",
          data: {id:1},
          dataType: "json",  
          async: false,  
          success:
          function (msg) 
          { 
              /*console.log(msg);*/
              $('#tgremio').empty();           
              $('#tgremio').append(' <option value="0" disabled selected> --Seleccione una opcion -- </option>');
              for (var i =0 ; i<msg[0].m; i++) { 
                 $('#tgremio').append('<option  value="'+msg[i].idcargo+'" >'+msg[i].nombre+'</option>');
              }
          },
          error: function(jqXHR, textStatus, errorThrown) {
              console.log("Problema con esto: " +textStatus, errorThrown);
      }
        });
  }
	$('#buscar').on('click',  function(){
			  $.ajax
          ({
            type: "POST",
            url: "controlador/controladorSocio.php",
            data: {id:5, cedula:tBuscarcedula.value},
            async: false,  
            dataType: "json",  
            success:
            function (msg) 
            { 
             /* console.log(msg);*/
              resultado2.innerHTML="";
            	if(msg[0].m>0){
            		resultado.innerHTML='';
                 	console.log(msg); 
                  resultado2.innerHTML='';
                 	$('#panelDatos').fadeIn();   
                 	tcedula.value=msg[0].cedula;
                 	tnombre.value=msg[0].nombres;
                 	tapellido.value=msg[0].apellidos;
                 	$('#tgremio').val(msg[0].gremiocargo);           
            	}
            	else{
                 	$('#panelDatos').fadeOut();              
                  resultado2.innerHTML='';
                    resultado.innerHTML='<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><center>¡No se ha encontrado el socio!</center></div>';

            	}
             
                   
            },
            error:
            function (msg) {console.log( msg +" No se pudo realizar la conexion");}
          });
 	
	});
 });
</script>

