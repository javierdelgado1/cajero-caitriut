<div role="tabpanel">

  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">

	    <li role="presentation" class="active" id="agregar2"><a href="#agregar" aria-controls="agregar" role="tab" data-toggle="tab">Registrar</a></li>
	    <li role="presentation" id="gestionar2"><a href="#gestionar" aria-controls="gestionar" role="tab" data-toggle="tab">Gestionar</a></li>
	    <li role="presentation" id="registro2" style="display:none;"><a href="#registro" aria-controls="registro" role="tab" data-toggle="tab">Registro de cuota</a></li>
	    
	   
	  </ul>

	  <div class="tab-content">
	   <div role="tabpanel" class="tab-pane active" id="agregar"></div>
	    <div role="tabpanel" class="tab-pane " id="gestionar"></div>
	    <div role="tabpanel" class="tab-pane " id="registro"></div>

	    
 		</div>

</div>




 <script type="text/javascript">
 $(document).ready(function() {

 	$("#agregar").hide().load('partials/secretaria/gestionaraportemanual/agregar.php', function(){}).fadeIn(1500);

 	$('#agregar2').on('click',  function(e){
 		e.preventDefault();
       	 $("#wait").css("display", "block");
			$("#agregar").hide().load('partials/secretaria/gestionaraportemanual/agregar.php', function(){
		 		$("#wait").css("display", "none");

			}).fadeIn(1500);
	});

	$('#gestionar2').on('click',  function(e){
		e.preventDefault();
       	 $("#wait").css("display", "block");
			$("#agregar").hide().load('partials/secretaria/gestionaraportemanual/gestionar.html', function(){
		 		$("#wait").css("display", "none");

			}).fadeIn(1500);
	});

	$('#registro2').on('click',  function(e){
		e.preventDefault();
       	 $("#wait").css("display", "block");
			$("#agregar").hide().load('partials/secretaria/gestionaraportemanual/registro.php', function(){	
		 		$("#wait").css("display", "none");

			}).fadeIn(1500);
	});
 });
</script>