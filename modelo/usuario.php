<?php
	require_once "db.class.php";

	class Usuario extends BasedeDatos {

		public function registrarUsuario(){
			$this->conectar();	
			$this->cedula=$_REQUEST['cedula'];
			$this->nombre=$_REQUEST['nombre'];	 
			$this->apellido=$_REQUEST['apellido'];
			$this->usuario=$_REQUEST['usuario'];
			$this->tipo=$_REQUEST['tipo'];	 
			$this->pass=$_REQUEST['pass'];		 
			$this->salida="true";
			$this->tupla = "INSERT INTO usuario3 (cedula, nombre, apellido, usuario, tipo, password ) VALUES  ('$this->cedula', '$this->nombre', '$this->apellido', '$this->usuario', '$this->tipo', '$this->pass')";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;
			if($this->salida!="true"&&$this->salida[0]=="D"&&$this->salida[1]=="u"&&$this->salida[2]=="p"&&$this->salida[3]=="l"&&$this->salida[4]=="i"&&$this->salida[5]=="c"&&$this->salida[6]=="a"&&$this->salida[7]=="t"&&$this->salida[8]=="e"){
				$this->salida="repetido";
			}
			else{
				session_start();
				$usuario=$_SESSION['usuario'];
				$fecha=date("Y-m-d");
				$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Registro un socio como usuario', '$fecha')";
				$this->resultado = $this->consulta($tupla2);
			}
			$this->desconectar();
			echo json_encode($this->salida);
		}

		public function ObtenerTodos(){
			$this->conectar();	
			$this->tupla = "SELECT * FROM usuario3 ORDER BY idusuario DESC ";
			$this->resultado = $this->consulta($this->tupla);
			$objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
					$objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
					$objeto[$this->i]['apellido']=$this->db_resultado['apellido'];
					$objeto[$this->i]['usuario']=$this->db_resultado['usuario'];
					$objeto[$this->i]['tipo']=$this->db_resultado['tipo'];
					$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];	
					$objeto[$this->i]['idusuario']=$this->db_resultado['idusuario'];
					$objeto[$this->i]['password']=$this->db_resultado['password'];


					$this->i++;

			}
			$this->desconectar();			
			echo json_encode($objeto);
		}

		public function obtenerUsuario(){
			$this->conectar();	
			$this->idU=$_REQUEST['idU'];			
			$this->tupla = "SELECT * FROM usuario3 WHERE idusuario='$this->idU'";
			$this->resultado = $this->consulta($this->tupla);
			$objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
					$objeto[$this->i]['nombre']=$this->db_resultado['nombre'];
					$objeto[$this->i]['apellido']=$this->db_resultado['apellido'];
					$objeto[$this->i]['usuario']=$this->db_resultado['usuario'];
					$objeto[$this->i]['tipo']=$this->db_resultado['tipo'];
					$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];	
					$objeto[$this->i]['idusuario']=$this->db_resultado['idusuario'];
									
					$objeto[$this->i]['password']=$this->db_resultado['password'];
									


			}
			$this->desconectar();			
			echo json_encode($objeto);

		}

		public function Eliminar(){
			$this->conectar();	
			$this->idU=$_REQUEST['idU'];

			$this->tupla = "DELETE FROM  usuario3 WHERE  idusuario='$this->idU'";
			$this->resultado = $this->consulta($this->tupla);
			$this->salida="true";

			session_start();
			$usuario=$_SESSION['usuario'];
			$fecha=date("Y-m-d");
			$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Elimino un usuario', '$fecha')";
			$this->resultado = $this->consulta($tupla2);



			$this->desconectar();			
			echo json_encode($this->salida);
		}

		public function modificar(){
			$this->conectar();	
			$this->idU=$_REQUEST['idU'];

			$this->cedula=$_REQUEST['cedula'];
			$this->nombre=$_REQUEST['nombre'];	 
			$this->apellido=$_REQUEST['apellido'];
			$this->usuario=$_REQUEST['usuario'];
			$this->tipo=$_REQUEST['tipo'];	 
			$this->pass=$_REQUEST['pass'];		 
			$this->salida="true";
			$this->tupla = "UPDATE usuario3 SET cedula='$this->cedula', nombre='$this->nombre', apellido='$this->apellido', usuario='$this->usuario', tipo='$this->tipo', password='$this->pass'  WHERE  idusuario='$this->idU'";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;
		
			session_start();
			$usuario=$_SESSION['usuario'];
			$fecha=date("Y-m-d");
			$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Modifico un usuario', '$fecha')";
			$this->resultado = $this->consulta($tupla2);


			$this->desconectar();
			echo json_encode($this->salida);
		}

		


	}

?>