<?php
	require_once "db.class.php";

	
	class SolicitudRetiro extends BasedeDatos {
		public function generaridderetiro(){
				$this->conectar();				
				$this->tupla="SELECT AUTO_INCREMENT as total FROM information_schema.TABLES where TABLE_SCHEMA='cautriut' and TABLE_NAME='solicitudretiro'";
				$this->resultado = $this->consulta($this->tupla) ;	
				$this->total="";
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					
					$this->total=$this->db_resultado['total'];		
				
				}
				$this->desconectar();				
				echo json_encode($this->total);
		}
		public function ComprobarsitieneDeudasoPrestamos(){
				$this->conectar();
				session_start();
				$this->socio=$_SESSION['sociosolicitud'];
				$this->tupla="SELECT SUM(cheque) as cheque, SUM(totalhaberes) as totalhaberes FROM solicitudprestamo WHERE estado='1' and idsocio='$this->socio'"; 
				$this->resultado = $this->consulta($this->tupla) ;				
				$objeto[0]['m']=$this->resultado->num_rows;
				$objeto[0]['deuda']=0;

				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['cheque']=$this->db_resultado['cheque'];
					$objeto[0]['totalhaberes']=$this->db_resultado['totalhaberes'];	
					$objeto[0]['deuda']=$objeto[0]['cheque']+$objeto[0]['totalhaberes'];
					$this->tupla="SELECT SUM(quincenauno1) AS quincenauno1, SUM(quincenados1) AS quincenados1, SUM(incresalmes1) AS incresalmes1, SUM(divanoanterior) AS divanoanterior, SUM(nivelalto1) AS nivelalto1, SUM(quincenauno2) AS quincenauno2, SUM(quincenados2) AS quincenados2, SUM(incresalmes2) AS incresalmes2, SUM(nivelalto2) AS nivelalto2 FROM aportes WHERE  idsocio='$this->socio'";
				
				$this->resultado2 = $this->consulta($this->tupla);
					/*$objeto[0]['m']=$this->resultado->num_rows;*/
				$this->i=0;
				$this->total="";
				$objeto[0]['haberes']=0;


				if($this->db_resultado = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
					{
						
						$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
						$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
						$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
						$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
						$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];
						$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);
					

						$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
						$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
						$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
						$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
						$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
						
					
						$objeto[0]['haberes']=$objeto[0]['haberes']+($objeto[$this->i]['sumaretencion'])+($objeto[$this->i]['sumaaporte']);
						//$this->i++;
						$objeto[0]['haberes']=$objeto[0]['haberes']-$objeto[0]['cheque'];
					}

				}


				

				$this->desconectar();
				
				echo json_encode($objeto);

		}
		public function ActualizarEstado(){
				$this->conectar();
				$this->estado=$_REQUEST['estadosolicittud'];
				$this->salida="true";
				session_start();
				$prestamo=$_SESSION['idsolicitudretiro'];
				$this->tupla="UPDATE  solicitudretiro SET estado='$this->estado' WHERE  idretiro='$prestamo'"; 
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				
				$usuario=$_SESSION['usuario'];
				$fecha=date("Y-m-d");
				$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Cambio el estado del Retiro', '$fecha')";
				$this->resultado = $this->consulta($tupla2);



				$this->desconectar();
				echo json_encode($this->salida);
		}
		public function detalledeRetiro(){
			$this->conectar();
				$this->idsolicitud=$_REQUEST['idsolicitud'];
				$this->tupla="SELECT solicitudretiro.idretiro,  solicitudretiro.fecha, solicitudretiro.tipoderetiro as tipoderetiro, solicitudretiro.monto, solicitudretiro.justificacion, solicitudretiro.fecha, s.fechadeafiliacion, s.nombres, s.apellidos, s.cedula,  solicitudretiro.estado FROM `solicitudretiro` 
							INNER JOIN  socio as s  ON s.idsocio=solicitudretiro.idsocio 
							INNER JOIN estadosolicitud e on e.idestadosolicitud=solicitudretiro.estado
							INNER JOIN  tipoderetiro as t on t.idtipo= solicitudretiro.tipoderetiro WHERE solicitudretiro.idretiro='$this->idsolicitud'";
				$this->resultado = $this->consulta($this->tupla) ;
				$this->i=0;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{	

					session_start();
					$_SESSION['idsolicitudretiro']=$this->db_resultado['idretiro'];
					$objeto[$this->i]['idretiro']=$this->db_resultado['idretiro'];
					$objeto[$this->i]['tipoderetiro']=$this->db_resultado['tipoderetiro'];
					$objeto[$this->i]['monto']=$this->db_resultado['monto'];
					$objeto[$this->i]['estado']=$this->db_resultado['estado'];
			
					$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
					$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
					$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
					$objeto[$this->i]['justificacion']=utf8_encode($this->db_resultado['justificacion']);
					$objeto[$this->i]['fecha']=$this->db_resultado['fecha'];
					$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
					
					$date = new DateTime($objeto[$this->i]['fecha']);
					$objeto[$this->i]['fecha']=$date->format('d-m-Y');

					$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
					$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');

					
				}

				$this->desconectar();	
				//print_r($objeto);		
				echo json_encode($objeto);
		}
		public function ObtenerSolicitudes(){
			$this->conectar();
			$this->tupla="";
			$this->tipo=$_REQUEST['tipo'];
			if($this->tipo==1)
			$this->tupla="SELECT solicitudretiro.idretiro, t.nombre as tipoderetiro, solicitudretiro.monto, solicitudretiro.justificacion, solicitudretiro.fecha,  s.nombres, s.apellidos, s.cedula,  e.estado as Eestado FROM `solicitudretiro` 
							INNER JOIN  socio as s  ON s.idsocio=solicitudretiro.idsocio 
							INNER JOIN estadosolicitud e on e.idestadosolicitud=solicitudretiro.estado
							INNER JOIN  tipoderetiro as t on t.idtipo= solicitudretiro.tipoderetiro WHERE e.estado='Aprobado'  ORDER BY  solicitudretiro.idretiro  DESC";
			if($this->tipo==2)
				$this->tupla="SELECT solicitudretiro.idretiro, t.nombre as tipoderetiro, solicitudretiro.monto, solicitudretiro.justificacion, solicitudretiro.fecha,  s.nombres, s.apellidos, s.cedula,  e.estado as Eestado FROM `solicitudretiro` 
							INNER JOIN  socio as s  ON s.idsocio=solicitudretiro.idsocio 
							INNER JOIN estadosolicitud e on e.idestadosolicitud=solicitudretiro.estado
							INNER JOIN  tipoderetiro as t on t.idtipo= solicitudretiro.tipoderetiro WHERE e.estado='No Aprobado'  ORDER BY  solicitudretiro.idretiro  DESC";
			if($this->tipo==3)
				$this->tupla="SELECT solicitudretiro.idretiro, t.nombre as tipoderetiro, solicitudretiro.monto, solicitudretiro.justificacion, solicitudretiro.fecha,  s.nombres, s.apellidos, s.cedula,  e.estado as Eestado FROM `solicitudretiro` 
							INNER JOIN  socio as s  ON s.idsocio=solicitudretiro.idsocio 
							INNER JOIN estadosolicitud e on e.idestadosolicitud=solicitudretiro.estado
							INNER JOIN  tipoderetiro as t on t.idtipo= solicitudretiro.tipoderetiro WHERE e.estado='En espera'  ORDER BY  solicitudretiro.idretiro  DESC";
			$this->resultado = $this->consulta($this->tupla) ;
			$objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$objeto[$this->i]['idretiro']=$this->db_resultado['idretiro'];
				$objeto[$this->i]['tipoderetiro']=$this->db_resultado['tipoderetiro'];
				$objeto[$this->i]['monto']=$this->db_resultado['monto'];
				$objeto[$this->i]['Eestado']=$this->db_resultado['Eestado'];
		
				$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
				$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
				$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
				$objeto[$this->i]['justificacion']=utf8_encode($this->db_resultado['justificacion']);

				$this->i++;
			}

			$this->desconectar();
			
			echo json_encode($objeto);

		}

		public function solicitud(){
			$this->conectar();
			session_start();
			$this->socio=$_SESSION['sociosolicitud'];
			$this->tipo=$_REQUEST['tiporetiro'];
		
			$this->monto=$_REQUEST['monto'];
			$this->haberes=$_REQUEST['haberes'];
			$this->justificacion=$_REQUEST['justificacion'];

			/*$this->=$_REQUEST[''];*/
			$this->salida="true";
			$this->fecha=date("Y-m-d");
			$this->tupla="INSERT INTO  solicitudretiro  (idsocio, totalhaberes, tipoderetiro, monto, justificacion, estado, fecha)
			 VALUES ('$this->socio', '$this->haberes', '$this->tipo', '$this->monto', '$this->justificacion', '3', '$this->fecha')";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;

			$usuario=$_SESSION['usuario'];
			$fecha=date("Y-m-d");
			$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Solicitud de Retiro', '$fecha')";
			$this->resultado = $this->consulta($tupla2);


			$this->desconectar();
			echo json_encode($this->salida);

		}
	}
	/*$s=new SolicitudRetiro();
	$s->ComprobarsitieneDeudasoPrestamos();*/
?>