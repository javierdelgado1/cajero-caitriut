<?php
	require_once "db.class.php";

	class Cargo extends BasedeDatos {
			public function ObtenerCargo(){
				$this->conectar();
				$this->tupla="SELECT idcargo, nombre FROM  cargo    ORDER BY idcargo DESC";
				$this->resultado =  $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				$this->i=0;
				while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[$this->i]['idcargo']=$this->db_resultado['idcargo'];
					$objeto[$this->i]['nombre']=utf8_encode($this->db_resultado['nombre']);
					
					$this->i++;

				}
				$this->desconectar();
				echo json_encode($objeto);

		}
	}

?>