<?php 
	require_once "db.class.php";

	class Aporte extends BasedeDatos {
		
			public function AgregarAporte(){
				session_start();				
				$this->conectar();
				$this->primeraquincena1=$_REQUEST['primeraquincena1'];
				$this->segundaquincena1=$_REQUEST['segundaquincena1'];
				$this->nivelalto1=$_REQUEST['nivelalto1'];
				$this->incresalmes1=$_REQUEST['incresalmes1'];
				$this->divanoanterior=$_REQUEST['divanoanterior'];
				$this->primeraquincena2=$_REQUEST['primeraquincena2'];
				$this->segundaquincena2=$_REQUEST['segundaquincena2'];
				$this->incresalmes2=$_REQUEST['incresalmes2'];
				$this->nivelalto2=$_REQUEST['nivelalto2'];
				$this->idsocio=$_SESSION['idsocio2'];
				$this->salida="true";
				$this->fecha=$_REQUEST['fecha']."-01";
				$this->ano=$this->fecha[0]."".$this->fecha[1]."".$this->fecha[2]."".$this->fecha[3];
				$this->mes=$this->fecha[5]."".$this->fecha[6];


			//$
				$this->tupla2="SELECT * FROM aportes WHERE  idsocio='$this->idsocio' AND year(fecha)='$this->ano' AND  month(fecha)='$this->mes'";
				$this->resultado2 =  $this->consulta($this->tupla2);
				if($this->db_resultado = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
				{
						$this->salida="R";
				}

				else {
					$this->tupla="INSERT  into  aportes (idsocio, quincenauno1, quincenados1, incresalmes1, nivelalto1, divanoanterior,
						quincenauno2, quincenados2, incresalmes2, nivelalto2, fecha) VALUES 
						('$this->idsocio', '$this->primeraquincena1','$this->segundaquincena1','$this->incresalmes1', '$this->nivelalto1', '$this->divanoanterior','$this->primeraquincena2',
							'$this->segundaquincena2','$this->incresalmes2','$this->nivelalto2', '$this->fecha')";
					$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;

					$usuario=$_SESSION['usuario'];
					$fecha=date("Y-m-d");
					$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Registro un Aporte', '$fecha')";
					$this->resultado = $this->consulta($tupla2);

				}
				$this->desconectar();
				echo json_encode($this->salida);				
			}
			
			public function BuscarAporte(){
				$this->conectar();
				$this->fechadesde=$_REQUEST['desde'];
				$this->fechahasta=$_REQUEST['hasta'];
				$this->cedula=$_REQUEST['cedula'];

				/*echo $this->fechadesde;
				echo $this->fechahasta;
				echo $this->cedula;*/
				$this->tupla="SELECT aportes.*, s.cedula, s.nombres,  s.apellidos, cargo.nombre AS cargo FROM aportes 
							  INNER JOIN  socio  AS s ON  s.idsocio=aportes.idsocio 
							  INNER JOIN  cargo ON cargo.idcargo=s.gremiocargo
							  WHERE  fecha BETWEEN '$this->fechadesde' AND  '$this->fechahasta' AND   s.cedula='$this->cedula'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				$this->i=0;
				while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
					$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
					$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
					$objeto[$this->i]['cargo']=$this->db_resultado['cargo'];
					$fecha=$this->db_resultado['fecha'];
					$objeto[$this->i]['ano']=$fecha[0]."".$fecha[1]."".$fecha[2]."".$fecha[3];
					$objeto[$this->i]['mes']=$fecha[5]."".$fecha[6];


					$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
					$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
					$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
					$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
					$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];

					$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);


					$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
					$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
					$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
					$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
					$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
					$this->i++;

				}
				$this->desconectar();
				echo json_encode($objeto);				


			}


	}

?>