<?php 
	require_once "db.class.php";

	class SolicitudPrestamo extends BasedeDatos {
		public function BuscarListadodePrestamos(){
				$this->conectar();
				$this->cedula=$_REQUEST['cedula'];
				$this->tupla="SELECT solicitudprestamo.idsolicitudprestamo,  solicitudprestamo.cheque, prestamo.descripcion, solicitudprestamo.cantidadprestamo, 
							solicitudprestamo.interes, solicitudprestamo.fecha, estadosolicitud.estado FROM `solicitudprestamo` 
								inner join socio as s on s.idsocio=solicitudprestamo.idsocio 
								inner join prestamo on prestamo.idprestamo=solicitudprestamo.idtipoprestamo
								INNER JOIN  estadosolicitud on estadosolicitud.idestadosolicitud=solicitudprestamo.estado WHERE  s.cedula='$this->cedula'";

				$this->resultado = $this->consulta($this->tupla) ;
				$objeto[0]['m']=$this->resultado->num_rows;
				$this->i=0;
				while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[$this->i]['idsolicitudprestamo']=$this->db_resultado['idsolicitudprestamo'];
					$objeto[$this->i]['descripcion']=utf8_encode($this->db_resultado['descripcion']);
					$objeto[$this->i]['cantidadprestamo']=$this->db_resultado['cantidadprestamo'];
					$objeto[$this->i]['interes']=$this->db_resultado['interes'];
					$objeto[$this->i]['cheque']=$this->db_resultado['cheque'];

					$objeto[$this->i]['fecha']=$this->db_resultado['fecha'];

					$date = new DateTime($objeto[$this->i]['fecha']);
					$objeto[$this->i]['fecha']=$date->format('d-m-Y');


					$objeto[$this->i]['estado']=$this->db_resultado['estado'];
					$this->i++;

				}

				$this->desconectar();				
				echo json_encode($objeto);

		}
		public function actualizarEstadodeSolicitud(){
				$this->conectar();
				$this->estado=$_REQUEST['estadosolicittud'];
				$this->salida="true";
				session_start();
				$prestamo=$_SESSION['idsolicitudprestamo'];
				$this->fecha=date("Y-m-d");

				$this->tupla="UPDATE  solicitudprestamo SET estado='$this->estado', fechadeaprobacion='$this->fecha' WHERE  idsolicitudprestamo='$prestamo'"; 
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				
				$usuario=$_SESSION['usuario'];
				$fecha=date("Y-m-d");
				$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Cambio el estado del Prestamo', '$fecha')";
				$this->resultado = $this->consulta($tupla2);



				$this->desconectar();
				echo json_encode($this->salida);

		}
		public function obtenerDetalledeSolicitud(){
				$this->conectar();
				$this->idsolicitud=$_REQUEST['idsolicitud'];
				$this->tupla="SELECT estadosolicitud.idestadosolicitud as estadosolicittud, solicitudprestamo.idsolicitudprestamo, 
							solicitudprestamo.cheque, solicitudprestamo.interes, solicitudprestamo.fecha, solicitudprestamo.cantidadprestamo, 
							solicitudprestamo.cuotaquincenal, socio.nombres, socio.apellidos, socio.cedula, socio.fechadeafiliacion, 
							prestamo.descripcion FROM solicitudprestamo 
								INNER JOIN  socio on socio.idsocio=solicitudprestamo.idsocio
								INNER JOIN prestamo on prestamo.idprestamo=solicitudprestamo.idtipoprestamo 
								INNER JOIN  estadosolicitud on  estadosolicitud.idestadosolicitud=solicitudprestamo.estado 
								WHERE  solicitudprestamo.idsolicitudprestamo='$this->idsolicitud'";
				$this->resultado = $this->consulta($this->tupla) ;
				$this->i=0;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{	

					session_start();
					$_SESSION['idsolicitudprestamo']=$this->db_resultado['idsolicitudprestamo'];
					$objeto[$this->i]['idsolicitudprestamo']=$this->db_resultado['idsolicitudprestamo'];
					$objeto[$this->i]['estadosolicittud']=$this->db_resultado['estadosolicittud'];
					$objeto[$this->i]['cheque']=$this->db_resultado['cheque'];
					$objeto[$this->i]['interes']=$this->db_resultado['interes'];
					$objeto[$this->i]['cuotaquincenal']=$this->db_resultado['cuotaquincenal'];
					$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
					$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
					$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
					$objeto[$this->i]['fecha']=$this->db_resultado['fecha'];
					$date = new DateTime($objeto[$this->i]['fecha']);
					$objeto[$this->i]['fecha']=$date->format('d-m-Y');

					$objeto[$this->i]['cantidadprestamo']=$this->db_resultado['cantidadprestamo'];
					$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];

					$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
					$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');


					$objeto[$this->i]['descripcion']=utf8_encode($this->db_resultado['descripcion']);
					$this->i++;
				}

				$this->desconectar();	
				//print_r($objeto);		
				echo json_encode($objeto);
		}

		public function solicitud(){
			$this->conectar();
			session_start();
			$this->socio=$_SESSION['sociosolicitud'];
			$this->tipo=$_REQUEST['tipo'];
			$this->total=$_REQUEST['total'];
			$this->totalinteres=$_REQUEST['totalinteres'];
			$this->montocuotaquincenal=$_REQUEST['montocuotaquincenal'];
			$this->cuotas=$_REQUEST['cuotas'];
			$this->totalhaberes=$_REQUEST['totalhaberes'];
			/*$this->=$_REQUEST[''];*/
			$this->salida="true";
			$this->fecha=date("Y-m-d");
			$this->tupla="INSERT INTO  solicitudprestamo  (idsocio, totalhaberes, idtipoprestamo, cuotas, cheque, interes, cuotaquincenal, estado, fecha)
			 VALUES ('$this->socio', '$this->totalhaberes', '$this->tipo', '$this->cuotas', '$this->total','$this->totalinteres','$this->montocuotaquincenal', '3', '$this->fecha')";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;

			$usuario=$_SESSION['usuario'];
			$fecha=date("Y-m-d");
			$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Solicitud de prestamo para un socio', '$fecha')";
			$this->resultado = $this->consulta($tupla2);


			$this->desconectar();
			echo json_encode($this->salida);

		}

		public function obtenerSolicitudes(){
			$this->conectar();
			$this->tupla="";
			$this->tipo=$_REQUEST['tipo'];
			if($this->tipo==1)
			$this->tupla="SELECT estadosolicitud.estado as estadosolicittud, solicitudprestamo.idsolicitudprestamo, solicitudprestamo.cheque, solicitudprestamo.interes, solicitudprestamo.cuotaquincenal, socio.nombres, socio.apellidos, socio.cedula, prestamo.descripcion FROM solicitudprestamo 
							INNER JOIN  socio on socio.idsocio=solicitudprestamo.idsocio
							INNER JOIN prestamo on prestamo.idprestamo=solicitudprestamo.idtipoprestamo 
							INNER JOIN  estadosolicitud on  estadosolicitud.idestadosolicitud=solicitudprestamo.estado WHERE  estadosolicitud.estado='Aprobado'  
							ORDER BY  idsolicitudprestamo  DESC";
			if($this->tipo==2)
				$this->tupla="SELECT estadosolicitud.estado as estadosolicittud, solicitudprestamo.idsolicitudprestamo, solicitudprestamo.cheque, solicitudprestamo.interes, solicitudprestamo.cuotaquincenal, socio.nombres, socio.apellidos, socio.cedula, prestamo.descripcion FROM solicitudprestamo 
							INNER JOIN  socio on socio.idsocio=solicitudprestamo.idsocio
							INNER JOIN prestamo on prestamo.idprestamo=solicitudprestamo.idtipoprestamo 
							INNER JOIN  estadosolicitud on  estadosolicitud.idestadosolicitud=solicitudprestamo.estado WHERE  estadosolicitud.estado='No Aprobado'  
							ORDER BY  idsolicitudprestamo  DESC";
			if($this->tipo==3)
				$this->tupla="SELECT estadosolicitud.estado as estadosolicittud,  solicitudprestamo.idsolicitudprestamo, solicitudprestamo.cheque, solicitudprestamo.interes, solicitudprestamo.cuotaquincenal, socio.nombres, socio.apellidos, socio.cedula, prestamo.descripcion FROM solicitudprestamo 
							INNER JOIN  socio on socio.idsocio=solicitudprestamo.idsocio
							INNER JOIN prestamo on prestamo.idprestamo=solicitudprestamo.idtipoprestamo 
							INNER JOIN  estadosolicitud on  estadosolicitud.idestadosolicitud=solicitudprestamo.estado WHERE  estadosolicitud.estado='En espera'  
							ORDER BY  idsolicitudprestamo  DESC";
			$this->resultado = $this->consulta($this->tupla) ;
			$objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$objeto[$this->i]['idsolicitudprestamo']=$this->db_resultado['idsolicitudprestamo'];
				$objeto[$this->i]['estadosolicittud']=$this->db_resultado['estadosolicittud'];
				$objeto[$this->i]['cheque']=$this->db_resultado['cheque'];
				$objeto[$this->i]['interes']=$this->db_resultado['interes'];
				$objeto[$this->i]['cuotaquincenal']=$this->db_resultado['cuotaquincenal'];
				$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
				$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
				$objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
				$objeto[$this->i]['descripcion']=utf8_encode($this->db_resultado['descripcion']);



				$this->i++;

			}

			$this->desconectar();
			
			echo json_encode($objeto);

		}

	}


?>