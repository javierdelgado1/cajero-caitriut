<?php
	require_once "db.class.php";

	class PlandeInversion extends BasedeDatos { 
		public function ObtenerPlanes(){
			$this->conectar();
			$this->tupla = "SELECT * FROM  plandeinversion ";
			$this->resultado = $this->consulta($this->tupla) ;
			$objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;			
			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				$objeto[$this->i]['objetivo']=utf8_encode($this->db_resultado['objetivo']);
				$objeto[$this->i]['concepto']=utf8_encode($this->db_resultado['concepto']);
				$objeto[$this->i]['inversionanual']=utf8_encode($this->db_resultado['inversion']);
				$objeto[$this->i]['ejecucion']=utf8_encode($this->db_resultado['ejecucion']);				
				$this->i++;
			}	
			$this->desconectar();
			echo json_encode($objeto);
		}
	}
?>