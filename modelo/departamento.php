<?php 
	require_once "db.class.php";

	class Departamento extends BasedeDatos {
		

		public function ObtenerDepartamentos(){
				$this->conectar();
				$this->tupla="SELECT iddepartamento, nombre FROM  departamento    ORDER BY iddepartamento DESC";
				$this->resultado =  $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				$this->i=0;
				while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[$this->i]['iddepartamento']=$this->db_resultado['iddepartamento'];
					$objeto[$this->i]['nombre']=utf8_encode($this->db_resultado['nombre']);
					
					$this->i++;

				}
				$this->desconectar();
				echo json_encode($objeto);

		}
	}
?>