<?php
	require_once "db.class.php";

	class Reporte extends BasedeDatos {

		public function GenerarTabladeAmortizacion(){
			session_start();		
			$_SESSION['tabla']=$_REQUEST['datahtml'];
			echo  $_SESSION['tabla'];
		}

		public function ObtenerHistorialdeOperaciones(){
				$this->conectar();
				$this->tupla="SELECT *  FROM historialdeoperaciones ORDER BY idhistorial DESC";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				$this->i=0;
				while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[$this->i]['usuarioquerealizaaccion']=$this->db_resultado['usuarioquerealizaaccion'];
					$objeto[$this->i]['accion']=$this->db_resultado['accion'];
					$objeto[$this->i]['usuario']=$this->db_resultado['usuario'];
					$objeto[$this->i]['fecha']=$this->db_resultado['fecha'];

					$date = new DateTime($objeto[$this->i]['fecha']);
					$objeto[$this->i]['fecha']=$date->format('d-m-Y');
					

					$this->i++;

				}
				$this->desconectar();
				echo json_encode($objeto);		

		}
		public function obtenerEstadisticasdeSocioregistradosMensualmente(){
			$this->conectar();
			$a[0]="";
			$this->tupla="";
			$this->i=0;
			$this->ano=$_REQUEST['ano'];
			while ($this->i<12){
					$mes=($this->i+1);
					$this->tupla="SELECT count(*) as socioasregistrados FROM socio WHERE  year(fechadeingreso)='$this->ano' AND  month(fechadeingreso)='$mes'";
					$this->resultado = $this->consulta($this->tupla);
					if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
						{			
							$objeto[0]['socioasregistrados']=$this->db_resultado['socioasregistrados'];
							$a[$this->i]=array(($this->i+1),(int) ($objeto[0]['socioasregistrados']));
						}
						else{
							$a[$this->i]=array(($this->i+1),0);
						}
				$this->i++;
			}
			$this->desconectar();

			echo json_encode($a);			
		}

		public function obtenerEstadisticasdeSolicitudesdePrestamos(){
			$this->conectar();
			$a[0]="";
			$this->tupla="";
	
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudprestamo` WHERE estado='1'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[0]=array("Aprobado",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[0]=array("Aprobado",0);
				}
				
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudprestamo` WHERE estado='2'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[1]=array("No Aprobado",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[1]=array("No Aprobado",0);
				}
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudprestamo` WHERE estado='3'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[2]=array("En Espera",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[2]=array("En Espera",0);
				}
				
			
			$this->desconectar();
			$b[0]=$a;
			echo json_encode($b);		
		}

		public function obtenerEstadisticasdeSolicitudesdeRetiros(){
			$this->conectar();
			$a[0]="";
			$this->tupla="";
	
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudretiro` WHERE estado='1'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[0]=array("Aprobado",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[0]=array("Aprobado",0);
				}
				
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudretiro` WHERE estado='2'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[1]=array("No Aprobado",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[1]=array("No Aprobado",0);
				}
			$this->tupla="SELECT count(*) as prestamos FROM `solicitudretiro` WHERE estado='3'";
			$this->resultado = $this->consulta($this->tupla);
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{			
					$objeto[0]['prestamos']=$this->db_resultado['prestamos'];
					$a[2]=array("En Espera",(int) ($objeto[0]['prestamos']));
				}
				else{
					$a[2]=array("En Espera",0);
				}
				
			
			$this->desconectar();
			$b[0]=$a;
			echo json_encode($b);	
		}
	}

/*	$r=new Reporte();
	$r->obtenerEstadisticasdeSolicitudesdeRetiros();*/

?>