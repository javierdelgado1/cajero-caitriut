<?php 
	require_once "db.class.php";

	class Socio extends BasedeDatos {
		public function ObtenerSueldo(){
			$this->conectar();			
			$this->cedula=$_REQUEST['cedula'];
			$this->tupla="SELECT salario  FROM  socio  WHERE  cedula='$this->cedula'";
			$this->resultado = $this->consulta($this->tupla);			
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{

				$this->objeto[0]['salario']=$this->db_resultado['salario'];
			}

			$this->desconectar();
			echo json_encode($this->objeto);

		}
		public function calcularTiempoAportandoalacajadeAhorros(){
			session_start();			
			$this->conectar();
			$this->socio=$_SESSION['sociosolicitud'];
			$this->tupla="SELECT count(*) as total FROM aportes  WHERE idsocio='$this->socio'";
			$this->resultado = $this->consulta($this->tupla) ;
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			$this->total="";

			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{

				$this->objeto[0]['meses']=$this->db_resultado['total'];
			}
			$this->tupla="SELECT tipoderetiro FROM `solicitudretiro` WHERE  idsocio='$this->socio' and estado='1' and tipoderetiro='1'";
			$this->resultado = $this->consulta($this->tupla) ;
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{

				$this->objeto[0]['hizoretiro']=$this->db_resultado['tipoderetiro'];
			}



			$this->desconectar();
			echo json_encode($this->objeto);
		}

		public function actualizarSocio(){
			session_start();
			$this->conectar();
			$this->cedula=$_REQUEST['cedula'];
			$this->nombre=$_REQUEST['nombre'];	 
			$this->email=$_REQUEST['email'];	 
			$this->lugardenac=$_REQUEST['lugardenac'];	 
			$this->direccion=$_REQUEST['direccion'];	 
			$this->celular=$_REQUEST['celular'];	 
			$this->departamento=$_REQUEST['departamento'];	 
			/*$this->estado=$_REQUEST['estado'];*/	 
			$this->cantidadfamiliares=$_REQUEST['cantidadfamiliares'];	 
			$this->apellido=$_REQUEST['apellido'];	 
			$this->fechadenac=$_REQUEST['fechadenac'];	 
			$this->estadocivil=$_REQUEST['estadocivil'];	 
			$this->telefonohabitacion=$_REQUEST['telefonohabitacion'];	 
			$this->gremiocargo=$_REQUEST['gremiocargo'];	 
			$this->condicion=$_REQUEST['condicion'];
			$this->salario=$_REQUEST['salario'];		 
			$this->fechadeingreso=$_REQUEST['fechadeingreso'];	 
			$this->familiares=serialize($_REQUEST['familiares']);	 
		 	$this->idsocio=$_SESSION['idsocio'];
			$this->salida="true";
			$this->tupla="UPDATE socio SET salario='$this->salario', cedula= '$this->cedula', nombres='$this->nombre', correo= '$this->email', lugardenacimiento= '$this->lugardenac',
			 direccion='$this->direccion', celular = '$this->celular', departamento= '$this->departamento',
			  apellidos= '$this->apellido', fechadenacimiento= '$this->fechadenac', estadocivil= '$this->estadocivil', telfonohab = '$this->telefonohabitacion',
			   gremiocargo='$this->gremiocargo', condicion='$this->condicion', fechadeingreso='$this->fechadeingreso', familiares='$this->familiares' WHERE idsocio='$this->idsocio'";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;

			$usuario=$_SESSION['usuario'];
			$fecha=date("Y-m-d");
			$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Modifico un socio', '$fecha')";
			$this->resultado = $this->consulta($tupla2);


			$this->desconectar();
			echo json_encode($this->salida);

		}

		public function afiliarSocio(){
			$this->conectar();
			$this->cedula=$_REQUEST['cedula'];
			$this->nombre=$_REQUEST['nombre'];	 
			$this->email=$_REQUEST['email'];	 
			$this->lugardenac=$_REQUEST['lugardenac'];	 
			$this->direccion=$_REQUEST['direccion'];	 
			$this->celular=$_REQUEST['celular'];	 
			$this->departamento=$_REQUEST['departamento'];	 
			/*$this->estado=$_REQUEST['estado'];*/	 
			$this->cantidadfamiliares=$_REQUEST['cantidadfamiliares'];	 
			$this->apellido=$_REQUEST['apellido'];	 
			$this->fechadenac=$_REQUEST['fechadenac'];	 
			$this->estadocivil=$_REQUEST['estadocivil'];	 
			$this->telefonohabitacion=$_REQUEST['telefonohabitacion'];	 
			$this->gremiocargo=$_REQUEST['gremiocargo'];	 
			$this->condicion=$_REQUEST['condicion'];	
			$this->salario=$_REQUEST['salario'];		 

			$this->fechadeingreso=$_REQUEST['fechadeingreso'];	 
			$this->familiares=serialize($_REQUEST['familiares']);	 
		 
			$this->salida="true";
			$this->tupla = "INSERT INTO socio (`salario`, `cedula`, `nombres`, `correo`, `lugardenacimiento`, `direccion`, `celular`,`departamento`, `cantidaddefamiliares`, `apellidos`, `fechadenacimiento`,  `estadocivil`, `telfonohab`, `gremiocargo`, `condicion`, `fechadeingreso`,  `fechadeafiliacion`, `familiares`) 
			VALUES  ('$this->salario', '$this->cedula', '$this->nombre', '$this->email', '$this->lugardenac', '$this->direccion', '$this->celular', '$this->departamento', '$this->cantidadfamiliares', '$this->apellido', '$this->fechadenac', '$this->estadocivil', '$this->telefonohabitacion',  '$this->gremiocargo', '$this->condicion', '$this->fechadeingreso', '$this->fechadeingreso', '$this->familiares')";
			$this->resultado = $this->consulta($this->tupla) or $this->salida=$this->conexion()->error;
			if($this->salida!="true"&&$this->salida[0]=="D"&&$this->salida[1]=="u"&&$this->salida[2]=="p"&&$this->salida[3]=="l"&&$this->salida[4]=="i"&&$this->salida[5]=="c"&&$this->salida[6]=="a"&&$this->salida[7]=="t"&&$this->salida[8]=="e"){
				$this->salida="repetido";
			}
			else{
				session_start();
				$usuario=$_SESSION['usuario'];
				$fecha=date("Y-m-d");
				$tupla2="INSERT INTO historialdeoperaciones (usuarioquerealizaaccion, accion, fecha) VALUES ('$usuario','Registro un socio', '$fecha')";
				$this->resultado = $this->consulta($tupla2);
			}
			$this->desconectar();
			echo json_encode($this->salida);
		}

		public 	function buscarSocio(){
			$this->conectar();
			$this->cedula=$_REQUEST['cedula'];
			$this->socio=$_REQUEST['socio'];
			$this->tupla="";
			if($this->cedula==""&&$this->socio!="")
				$this->tupla="SELECT socio.*, cargo.idcargo as cargo FROM `socio` INNER JOIN cargo ON cargo.idcargo=socio.gremiocargo WHERE   idsocio like '%$this->socio%'";
			if($this->cedula!=""&&$this->socio=="")
				$this->tupla="SELECT socio.*, cargo.idcargo as cargo FROM `socio`  INNER JOIN cargo ON cargo.idcargo=socio.gremiocargo WHERE  cedula like '%$this->cedula%'";
					
			if($this->cedula!=""&&$this->socio!="")
				$this->tupla="SELECT socio.*, cargo.idcargo as cargo FROM `socio` INNER JOIN cargo ON cargo.idcargo=socio.gremiocargo  WHERE  cedula like '%$this->cedula%' or idsocio like '%$this->socio%'";
				
			$this->resultado =  $this->consulta($this->tupla);
			$this->objeto[0]['m']=$this->resultado->num_rows;
			$this->i=0;
			$this->total="";

			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				session_start();
				$_SESSION['idsocio'] =$this->db_resultado['idsocio'];
				$this->objeto[$this->i]['idsocio']=$this->db_resultado['idsocio'];
				$this->objeto[$this->i]['salario']=$this->db_resultado['salario'];

				$this->objeto[$this->i]['cargo']=$this->db_resultado['cargo'];
				$this->objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
				$this->objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
				$this->objeto[$this->i]['correo']=$this->db_resultado['correo'];
				$this->objeto[$this->i]['lugardenacimiento']=$this->db_resultado['lugardenacimiento'];
				$this->objeto[$this->i]['direccion']=$this->db_resultado['direccion'];
				$this->objeto[$this->i]['celular']=$this->db_resultado['celular'];
				$this->objeto[$this->i]['departamento']=$this->db_resultado['departamento'];
				/*$this->objeto[$this->i]['estado']=$this->db_resultado['estado'];*/
				$this->objeto[$this->i]['cantidaddefamiliares']=$this->db_resultado['cantidaddefamiliares'];
				$this->objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
				$this->objeto[$this->i]['fechadenacimiento']=$this->db_resultado['fechadenacimiento'];
				$this->objeto[$this->i]['estadocivil']=$this->db_resultado['estadocivil'];
				$this->objeto[$this->i]['telfonohab']=$this->db_resultado['telfonohab'];
				$this->objeto[$this->i]['gremiocargo']=$this->db_resultado['gremiocargo'];
				$this->objeto[$this->i]['condicion']=$this->db_resultado['condicion'];
				$this->objeto[$this->i]['fechadeingreso']=$this->db_resultado['fechadeingreso'];
				$this->objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
				if($this->objeto[$this->i]['cantidaddefamiliares']>0)
				$this->objeto[$this->i]['familiares']=unserialize($this->db_resultado['familiares']);
				$this->total="fino (y) :D";


			}
			$this->desconectar();
			echo json_encode($this->objeto);

		}
		public function Obtenernumerodesocio(){
			$this->conectar();
			$this->tupla="SELECT idsocio as total FROM `socio` ORDER BY `socio`.`idsocio` DESC limit 0,1";
			$this->resultado = $this->consulta($this->tupla);
			$this->total="";
			if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				
				$this->total=$this->db_resultado['total']+1;		
				while (strlen($this->total)<7) {
					$this->total="0".$this->total;
				}
			}
			$this->desconectar();
			echo json_encode($this->total);
		}

		public function buscarSocio2(){
					$this->conectar();
					$this->cedula=$_REQUEST['cedula'];
					$this->tupla="";
					$this->tupla="SELECT * FROM `socio` WHERE  cedula like '%$this->cedula%'";	
					$this->resultado = $this->consulta($this->tupla) ;
					$this->objeto[0]['m']=$this->resultado->num_rows;
					$this->i=0;
					$this->total="";

					if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
					{
						session_start();
						$_SESSION['idsocio2'] =$this->db_resultado['idsocio'];
						$this->objeto[$this->i]['idsocio']=$this->db_resultado['idsocio'];
						$this->objeto[$this->i]['cedula']=$this->db_resultado['cedula'];
						$this->objeto[$this->i]['nombres']=$this->db_resultado['nombres'];				
						$this->objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];				
						$this->objeto[$this->i]['gremiocargo']=$this->db_resultado['gremiocargo'];

					}
					$this->desconectar();
					echo json_encode($this->objeto);
		}

		public function calcularChequeTotal(){
			session_start();
			$this->conectar();			
			$idsocio=$_SESSION['sociosolicitud'];
			$this->tupla="SELECT socio.nombres, socio.apellidos, socio.fechadeafiliacion, socio.idsocio, aportes.* FROM socio 
			INNER JOIN  aportes ON socio.idsocio=aportes.idsocio WHERE aportes.idsocio='$idsocio'";	
			$this->resultado = $this->consulta($this->tupla) ;
			$objeto[0]['m']=$this->resultado->num_rows;

			$this->total="";			
			$objeto[0]['TOTAL']=0;
			$objeto[0]['TOTAL2']=0;
			$objeto[0]['cheque']=0;
			$objeto[0]['cincuentaporciento']=0;
			$objeto[0]['controlparahacersolicitud']=0;
			$objeto[0]['totalparapedirprestamo']=0;
			$objeto[0]['totalhaberes']=0;
			$objeto[0]['deuda']=0;
			$this->i=0;

			while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
			{
				
						$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
						$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
						$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
						$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
						$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];
						$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);
						$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
						$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
						$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');

						$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
						$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
						$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
						$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
						$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
						

						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']+($objeto[$this->i]['sumaretencion'])+($objeto[$this->i]['sumaaporte']);
						$this->i++;

			}
			$this->buscarretiro="SELECT SUM(monto) as monto, SUM(totalhaberes) as haberes FROM `solicitudretiro` WHERE idsocio='$idsocio'";
				$this->retiro = $this->consulta($this->buscarretiro) ;
				if($this->db_resultado = mysqli_fetch_array($this->retiro, MYSQLI_ASSOC))
				{
					$objeto[0]['monto']=$this->db_resultado['monto'];
					$objeto[0]['haberes']=$this->db_resultado['haberes'];
					$objeto[0]['TOTAL']=$objeto[0]['TOTAL']-$objeto[0]['monto'];
					
				}

			$tupla="SELECT solicitudprestamo.cheque, totalhaberes FROM  solicitudprestamo WHERE idsocio='$idsocio' and estado='1'";
			$this->resultado2 = $this->consulta($tupla) ;
			
				while($this->db_resultado2 = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
				{
					$objeto[0]['cheque']=($objeto[0]['cheque'])+($this->db_resultado2['cheque']);	
					$objeto[0]['cincuentaporciento']=$objeto[0]['cincuentaporciento']+($objeto[0]['cheque']/2);			
					$objeto[0]['totalhaberes']=$objeto[0]['totalhaberes']+($this->db_resultado2['totalhaberes']);
				}		

			/*if(mysqli_fetch_array($this->resultado, MYSQLI_ASSOC)){
				$objeto[0]['TOTAL2']=$objeto[0]['TOTAL2']-$objeto[0]['cheque'];
			}*/
			

			$objeto[0]['total']=$objeto[0]['TOTAL']-$objeto[0]['cheque'];
			$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento'];
			$objeto[0]['deuda']=$objeto[0]['cheque']+$objeto[0]['totalhaberes'];

			if(($objeto[0]['totalhaberes']+$objeto[0]['cheque'])>$objeto[0]['total'])
				$objeto[0]['deuda']=($objeto[0]['totalhaberes']+$objeto[0]['cheque'])-$objeto[0]['total'];
					

			$this->desconectar();			
			echo json_encode($objeto);
		}
		public function BuscarHaberesdelSocio(){
					$this->conectar();
					$this->cedula=$_REQUEST['cedula'];
					$this->tupla="SELECT socio.nombres, socio.apellidos, socio.fechadeafiliacion, socio.idsocio, aportes.* FROM socio 
					INNER JOIN  aportes ON socio.idsocio=aportes.idsocio WHERE  socio.cedula='$this->cedula'";	
					$this->resultado = $this->consulta($this->tupla) ;
					$objeto[0]['m']=$this->resultado->num_rows;
					$this->i=0;
					$this->total="";
					$objeto[0]['TOTAL']=0;
					$objeto[0]['TOTAL2']=0;
					$objeto[0]['cheque']=0;
					$objeto[0]['cincuentaporciento']=0;
					$objeto[0]['controlparahacersolicitud']=0;
					$objeto[0]['totalparapedirprestamo']=0;
					$objeto[0]['totalhaberes']=0;
					$objeto[0]['cincuentaporciento2']=0;
					$objeto[0]['deuda']=0;
					$idsocio=-1;
					session_start();
					
					
					while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
					{
						$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
						$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
						$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
						$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
						$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
						$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
						$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];
						$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);
						$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
						$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
						$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');

						$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
						$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
						$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
						$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
						$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
						
						$_SESSION['sociosolicitud'] =$this->db_resultado['idsocio'];
						$idsocio=$_SESSION['sociosolicitud']; 


						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']+($objeto[$this->i]['sumaretencion'])+($objeto[$this->i]['sumaaporte']);
						$this->i++;

					}
					$this->buscarretiro="SELECT SUM(monto) as monto, SUM(totalhaberes) as haberes FROM `solicitudretiro` WHERE idsocio='$idsocio'";
					$this->retiro = $this->consulta($this->buscarretiro) ;
					if($this->db_resultado = mysqli_fetch_array($this->retiro, MYSQLI_ASSOC))
					{
						$objeto[0]['monto']=$this->db_resultado['monto'];
						$objeto[0]['haberes']=$this->db_resultado['haberes'];
						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']-$objeto[0]['monto'];
						
					}

					$this->tupla="SELECT  SUM(cheque) as cheque, SUM(totalhaberes) as totalhaberes FROM solicitudprestamo WHERE estado='1' and idsocio='$idsocio'";	
					
					$this->resultado3 = $this->consulta($this->tupla) ;


					$objeto[0]['idsocio']=$idsocio;
					/*$tupla="SELECT * FROM  solicitudprestamo WHERE idsocio='$idsocio' ORDER BY idsolicitudprestamo  DESC limit 1";*/
					$tupla="SELECT   count(*) as tablas, SUM(solicitudprestamo.cheque) as cheque , SUM(totalhaberes) as totalhaberes FROM  solicitudprestamo WHERE idsocio='$idsocio' and estado='1'";
					$this->resultado2 = $this->consulta($tupla) ;
					
					if($this->db_resultado2 = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
					{
						/*$objeto[0]['cheque']=number_format($this->db_resultado2['cheque'],2,".",",");	
						$objeto[0]['cincuentaporciento']=$objeto[0]['cheque']/2;
						$objeto[0]['cincuentaporciento']=number_format($objeto[0]['cincuentaporciento'],2,".",",");			
						$objeto[0]['totalhaberes']=number_format($this->db_resultado2['totalhaberes'],2,".",",");*/

						$objeto[0]['cheque']=$this->db_resultado2['cheque'];	
						$objeto[0]['cincuentaporciento']=$objeto[0]['cheque']/2;
						$objeto[0]['cincuentaporciento']=$objeto[0]['cincuentaporciento'];			
						$objeto[0]['totalhaberes']=$this->db_resultado2['totalhaberes'];
						$objeto[0]['m2']=$this->db_resultado2['tablas'];
						
					}

					$objeto[0]['total']=($objeto[0]['TOTAL'])-($objeto[0]['cheque']);
					if($objeto[0]['m2']==1){
						if($objeto[0]['totalhaberes']<0)
							$objeto[0]['totalhaberes']*=-1;
						if($objeto[0]['cincuentaporciento']<0)
							$objeto[0]['cincuentaporciento']*=-1;

						$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento'];

					}
					else if($objeto[0]['m2']>1){
						/*$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento'];*/
						$tupla="SELECT * FROM  solicitudprestamo WHERE idsocio='$idsocio' and estado='1' ORDER BY idsolicitudprestamo  DESC limit 1";
						$this->resultado2 = $this->consulta($tupla) ;
						
						
						if($this->db_resultado2 = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
						{
							$objeto[0]['cheque']=number_format($this->db_resultado2['cheque'],2,".",",");	
							$objeto[0]['cincuentaporciento2']=$objeto[0]['cheque']/2;	
							$objeto[0]['totalhaberes']=number_format($this->db_resultado2['totalhaberes'],2,".",",");						
						}
						$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento2'];
						
					}
					

					/*$objeto[0]['deuda']=$objeto[0]['cheque']+$objeto[0]['totalhaberes'];*/

					/*if(($objeto[0]['totalhaberes']+$objeto[0]['cheque'])>$objeto[0]['total'])*/
						$objeto[0]['deuda']=($objeto[0]['TOTAL']-$objeto[0]['cheque']);
					

					$this->desconectar();
					
					echo json_encode($objeto);
		}

		public function BuscarHaberesdelSociologueado(){
				/*$this->conectar();
					session_start();

					$this->cedula=$_SESSION['cedula'];
					$this->tupla="SELECT socio.nombres, socio.apellidos, socio.fechadeafiliacion, aportes.* FROM socio 
					INNER JOIN  aportes ON socio.idsocio=aportes.idsocio WHERE  socio.cedula='$this->cedula'";	
					$this->resultado = $this->consulta($this->tupla) ;
					$objeto[0]['m']=$this->resultado->num_rows;
					$this->i=0;
					$this->total="";
					$objeto[0]['TOTAL']=0;
					if(mysqli_fetch_array($this->resultado, MYSQLI_ASSOC)){
						$_SESSION['sociosolicitud'] =$this->cedula;

					}
					
					while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
					{
						$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
						$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
						$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
						$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
						$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
						$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
						$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];
						$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);
						$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
						$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
						$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');

						$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
						$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
						$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
						$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
						$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
						

						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']+($objeto[$this->i]['sumaretencion'])+($objeto[$this->i]['sumaaporte']);
						$this->i++;

					}
					$objeto[0]['total']=$objeto[0]['TOTAL'];
					$this->desconectar();
					
					echo json_encode($objeto);*/


					session_start();
					$this->conectar();
					$this->cedula=$_SESSION['cedula'];
					$this->tupla="SELECT s.nombres, s.apellidos, s.fechadeafiliacion, s.idsocio, usuario3.*, aportes.*  FROM usuario3 
									inner join socio as s on s.cedula=usuario3.cedula
									inner join aportes on aportes.idsocio=s.idsocio
									WHERE  s.cedula='$this->cedula'";	
					$this->resultado = $this->consulta($this->tupla) ;
					$objeto[0]['m']=$this->resultado->num_rows;
					$this->i=0;
					$this->total="";
					$objeto[0]['TOTAL']=0;
					$objeto[0]['TOTAL2']=0;
					$objeto[0]['cheque']=0;
					$objeto[0]['cincuentaporciento']=0;
					$objeto[0]['controlparahacersolicitud']=0;
					$objeto[0]['totalparapedirprestamo']=0;
					$objeto[0]['totalhaberes']=0;
					$objeto[0]['cincuentaporciento2']=0;
					$objeto[0]['deuda']=0;
					$idsocio=-1;
					
					
					while($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
					{
						$objeto[$this->i]['nombres']=$this->db_resultado['nombres'];
						$objeto[$this->i]['apellidos']=$this->db_resultado['apellidos'];
						$objeto[$this->i]['quincenauno1']=$this->db_resultado['quincenauno1'];
						$objeto[$this->i]['quincenados1']=$this->db_resultado['quincenados1'];
						$objeto[$this->i]['incresalmes1']=$this->db_resultado['incresalmes1'];
						$objeto[$this->i]['nivelalto1']=$this->db_resultado['nivelalto1'];
						$objeto[$this->i]['divanoanterior']=$this->db_resultado['divanoanterior'];
						$objeto[$this->i]['sumaretencion']=($objeto[$this->i]['quincenauno1'])+($objeto[$this->i]['quincenados1'])+($objeto[$this->i]['incresalmes1'])+($objeto[$this->i]['divanoanterior'])+($this->db_resultado['nivelalto1']);
						$objeto[$this->i]['fechadeafiliacion']=$this->db_resultado['fechadeafiliacion'];
						$date = new DateTime($objeto[$this->i]['fechadeafiliacion']);
						$objeto[$this->i]['fechadeafiliacion']=$date->format('d-m-Y');

						$objeto[$this->i]['quincenauno2']=$this->db_resultado['quincenauno2'];
						$objeto[$this->i]['quincenados2']=$this->db_resultado['quincenados2'];
						$objeto[$this->i]['incresalmes2']=$this->db_resultado['incresalmes2'];
						$objeto[$this->i]['nivelalto2']=$this->db_resultado['nivelalto2'];
						$objeto[$this->i]['sumaaporte']=($objeto[$this->i]['quincenauno2'])+($objeto[$this->i]['quincenados2'])+($objeto[$this->i]['incresalmes2'])+($objeto[$this->i]['nivelalto2']);
						
						$_SESSION['sociosolicitud'] =$this->db_resultado['idsocio'];
						$idsocio=$_SESSION['sociosolicitud']; 


						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']+($objeto[$this->i]['sumaretencion'])+($objeto[$this->i]['sumaaporte']);
						$this->i++;

					}
					$this->buscarretiro="SELECT SUM(monto) as monto, SUM(totalhaberes) as haberes FROM `solicitudretiro` WHERE idsocio='$idsocio'";
					$this->retiro = $this->consulta($this->buscarretiro) ;
					if($this->db_resultado = mysqli_fetch_array($this->retiro, MYSQLI_ASSOC))
					{
						$objeto[0]['monto']=$this->db_resultado['monto'];
						$objeto[0]['haberes']=$this->db_resultado['haberes'];
						$objeto[0]['TOTAL']=$objeto[0]['TOTAL']-$objeto[0]['monto'];
						
					}

					$this->tupla="SELECT  SUM(cheque) as cheque, SUM(totalhaberes) as totalhaberes FROM solicitudprestamo WHERE estado='1' and idsocio='$idsocio'";	
					
					$this->resultado3 = $this->consulta($this->tupla) ;


					$objeto[0]['idsocio']=$idsocio;
					/*$tupla="SELECT * FROM  solicitudprestamo WHERE idsocio='$idsocio' ORDER BY idsolicitudprestamo  DESC limit 1";*/
					$tupla="SELECT   count(*) as tablas, SUM(solicitudprestamo.cheque) as cheque , SUM(totalhaberes) as totalhaberes FROM  solicitudprestamo WHERE idsocio='$idsocio' and estado='1'";
					$this->resultado2 = $this->consulta($tupla) ;
					
					if($this->db_resultado2 = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
					{
						/*$objeto[0]['cheque']=number_format($this->db_resultado2['cheque'],2,".",",");	
						$objeto[0]['cincuentaporciento']=$objeto[0]['cheque']/2;
						$objeto[0]['cincuentaporciento']=number_format($objeto[0]['cincuentaporciento'],2,".",",");			
						$objeto[0]['totalhaberes']=number_format($this->db_resultado2['totalhaberes'],2,".",",");*/

						$objeto[0]['cheque']=$this->db_resultado2['cheque'];	
						$objeto[0]['cincuentaporciento']=$objeto[0]['cheque']/2;
						$objeto[0]['cincuentaporciento']=$objeto[0]['cincuentaporciento'];			
						$objeto[0]['totalhaberes']=$this->db_resultado2['totalhaberes'];
						$objeto[0]['m2']=$this->db_resultado2['tablas'];
						
					}

					$objeto[0]['total']=($objeto[0]['TOTAL'])-($objeto[0]['cheque']);
					if($objeto[0]['m2']==1){
						if($objeto[0]['totalhaberes']<0)
							$objeto[0]['totalhaberes']*=-1;
						if($objeto[0]['cincuentaporciento']<0)
							$objeto[0]['cincuentaporciento']*=-1;

						$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento'];

					}
					else if($objeto[0]['m2']>1){
						/*$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento'];*/
						$tupla="SELECT * FROM  solicitudprestamo WHERE idsocio='$idsocio' and estado='1' ORDER BY idsolicitudprestamo  DESC limit 1";
						$this->resultado2 = $this->consulta($tupla) ;
						
						
						if($this->db_resultado2 = mysqli_fetch_array($this->resultado2, MYSQLI_ASSOC))
						{
							$objeto[0]['cheque']=number_format($this->db_resultado2['cheque'],2,".",",");	
							$objeto[0]['cincuentaporciento2']=$objeto[0]['cheque']/2;	
							$objeto[0]['totalhaberes']=number_format($this->db_resultado2['totalhaberes'],2,".",",");						
						}
						$objeto[0]['totalparapedirprestamo']=$objeto[0]['totalhaberes']+$objeto[0]['cincuentaporciento2'];
						
					}
					

					$objeto[0]['deuda']=($objeto[0]['TOTAL']-$objeto[0]['cheque']);
					

					$this->desconectar();
					
					echo json_encode($objeto);
		}

	}

?>