<?php
	require_once "db.class.php";

	class Indicadorgeneral extends BasedeDatos {

		public function EPAGuardar()
		{
				$this->conectar();
				$this->saldo=$_REQUEST['saldo'];
				$this->haberes=$_REQUEST['haberes'];
				$this->salida="true";
				$this->fecha=date("Y-m-d");
				$this->tupla="UPDATE endeudamientopatronal SET  porcobrar='$this->saldo', patrimonios='$this->haberes' WHERE  id='1'";
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				$this->desconectar();
				echo json_encode($this->salida);	
		}
		public function obtenerEPA(){
				$this->conectar();
				$this->tupla="SELECT * FROM  endeudamientopatronal WHERE id='1'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['porcobrar']=$this->db_resultado['porcobrar'];			
					$objeto[0]['patrimonios']=$this->db_resultado['patrimonios'];
			    }
			   $this->desconectar();
			   echo json_encode($objeto);		

		}


		public function EPGuardar()
		{
				$this->conectar();
				$this->saldo=$_REQUEST['saldo'];
				$this->haberes=$_REQUEST['haberes'];
				$this->salida="true";
				$this->fecha=date("Y-m-d");
				$this->tupla="UPDATE endeudamientoprestamos SET  saldo='$this->saldo', haberes='$this->haberes' WHERE  id='1'";
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				$this->desconectar();
				echo json_encode($this->salida);	
		}
		public function obtenerEP(){
				$this->conectar();
				$this->tupla="SELECT * FROM  endeudamientoprestamos WHERE id='1'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['saldo']=$this->db_resultado['saldo'];			
					$objeto[0]['haberes']=$this->db_resultado['haberes'];
			    }
			   $this->desconectar();
			   echo json_encode($objeto);		

		}




		public function DisponibilidadGuardar()
		{
				$this->conectar();
				$this->efectivo=$_REQUEST['efectivo'];
				$this->haberes=$_REQUEST['haberes'];
				$this->salida="true";
				$this->fecha=date("Y-m-d");
				$this->tupla="UPDATE disponibilidad SET  efectivo='$this->efectivo', haberes='$this->haberes', fecha='$this->fecha' WHERE  iddisponibilidad='1'";
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				$this->desconectar();
				echo json_encode($this->salida);	
		}
		public function obtenerDisponibilidad(){
				$this->conectar();
				$this->tupla="SELECT * FROM  disponibilidad WHERE iddisponibilidad='1'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['efectivo']=$this->db_resultado['efectivo'];			
					$objeto[0]['haberes']=$this->db_resultado['haberes'];
			    }
			   $this->desconectar();
			   echo json_encode($objeto);		

		}



		public function LiquidezGuardar()
		{
				$this->conectar();
				$this->efectivo=$_REQUEST['efectivo'];
				$this->pasivo=$_REQUEST['pasivo'];
				$this->salida="true";
				$this->fecha=date("Y-m-d");
				$this->tupla="UPDATE liquidez SET  efectivo='$this->efectivo', pasivo='$this->pasivo', fecha='$this->fecha' WHERE  idliquidez='1'";
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				$this->desconectar();
				echo json_encode($this->salida);	
		}
		public function obtenerLiquidez(){
				$this->conectar();
				$this->tupla="SELECT * FROM  liquidez WHERE idliquidez='1'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['efectivo']=$this->db_resultado['efectivo'];
					$objeto[0]['pasivo']=$this->db_resultado['pasivo'];

					
			    }
			   $this->desconectar();
			   echo json_encode($objeto);		

		}

		public function SolvenciaGuardar()
		{
				$this->conectar();
				$this->activo=$_REQUEST['activo'];
				$this->pasivo=$_REQUEST['pasivo'];
				$this->salida="true";
				$this->fecha=date("Y-m-d");				
				$this->tupla="UPDATE solvencia SET  activo='$this->activo', pasivo='$this->pasivo', fecha='$this->fecha' WHERE  idsolvencia='1'";
				$this->resultado = $this->consulta($this->tupla)  or $this->salida=$this->conexion()->error;
				$this->desconectar();
				echo json_encode($this->salida);					
		}

		public function obtenerSolvencia(){
				$this->conectar();
				$this->tupla="SELECT * FROM  solvencia WHERE idsolvencia='1'";
				$this->resultado = $this->consulta($this->tupla);
				$objeto[0]['m']=$this->resultado->num_rows;
				if($this->db_resultado = mysqli_fetch_array($this->resultado, MYSQLI_ASSOC))
				{
					$objeto[0]['activo']=$this->db_resultado['activo'];
					$objeto[0]['pasivo']=$this->db_resultado['pasivo'];

					
			    }
			   $this->desconectar();
			   echo json_encode($objeto);		

		}

	}

?>